# Website des AkArchWiss e. V.

Dieses README enthält zusammengefasst die wichtigsten Informationen zu diesem Repositorium.
Das Repositorium dient uns zum gemeinsamen Arbeiten an der Website des AkArchWiss (www.akarchwiss.de) und protokolliert alle Änderungen.
Die einzelnen Branches geben dir einen Einblick in den aktuellen Entwicklungsstand der Website.
Für die Generierung der eigentlichen HTML-Seite setzen wir einen sog. Static site generator (SSG) ein – in unserem Fall das [Hugo Framework](https://gohugo.io/).

Ausführliche Anleitungen und Tipps zum Umgang mit diesem Repositorium (z. B. Wie erstelle ich einen Artikel?) findest du in unserem [Wiki](https://gitlab.com/akarchwiss/akarchwiss.de/-/wikis/home).

## Static site generator (SSG)

Der Vorteil in einem SSG liegt in den leicht zu programmierenden Modulen (wie z. B. den sog. Shortcodes bei Hugo).
Dadurch können wir das Verhalten der Website gut an die Anforderungen unseres Vereins anpassen;
der Shortcode [epigraph](https://gitlab.com/akarchwiss/hugo-fjord-akarchwiss/-/blob/master/layouts/shortcodes/epigraph.html) gestattet uns etwa, ein global einheitliches Layout für Inschriften zu generieren, ohne dabei die Formatierungsrichtlinien einsehen zu müssen. Das reduziert Fehler und spart Zeit!

Ein weiterer Vorteil eines SSG liegt im Verzicht auf eine Datenbank, wie sie von vielen [CMS](https://de.wikipedia.org/wiki/Content-Management-System) eingesetzt werden (es gibt aber auch sog. [Flat-File-CMS](https://de.wikipedia.org/wiki/Flat-File-Content-Management-System)).
Das reduziert die Komplexität des Systems (im Prinzip benötigen wir nicht mehr als einfache Textdateien) und lässt die Website deutlich schneller laden,
da die HTML-Seiten bereits vorliegen und nicht erst beim Aufruf einer Seite vom Server kompiliert werden muss
(natürlich verfügen viele CMS über einen Cache, um nicht für jeden Aufruf die Seite erneut zu berechnen).

Und zu guter Letzt bieten Websites, die mit einem SSG erstellt wurden, deutlich weniger Angriffsflächen für Eindringle.

Unser verwendetes Theme findest du übrigens hier: [hugo-fjord-akarchwiss](https://gitlab.com/akarchwiss/hugo-fjord-akarchwiss)

## Lizenz der Beiträge auf der Website

Die Autorenschaft legt die Lizenz für ihren Beitrag selbst fest.
In den meisten Fällen empfehlen wir hierfür die [Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/).
Beachte außerdem diese [Ausführungen zu unserem verwendeten Text- und Bildmaterial](LICENSE).

## Du möchtest mitwirken?

Sieh dir unseren [Contributing Guideline](CONTRIBUTING) an.

## Mitarbeitende

* Hannes Faußner
* David Berthel
* Roxana Dörr

Redaktionelle Fragen kannst du via Cloud/Talk an Hannes Faußner richten, technische Fragen an David Berthel.
Natürlich kannst du uns auch gerne eine E-Mail schreiben:  
[webmaster@akarchwiss.de](mailto:webmaster@akarchwiss.de?subject=Website)

-----------------
www.akarchwiss.de
