---
title: "Virtueller Stammtisch"
slug: {{ .File.BaseFileName }} # Benutzerdefinierter Pfad nach Domainname. Füge Datum hinzu!

startDate: {{ ((.Date | time).AddDate 0 00 14).Format "2006-01-02T00:00:00+01:00" }}
endDate: {{ ((.Date | time).AddDate 0 00 14).Format "2006-01-02T00:00:00+01:00" }}
startDate_hhmm: true # Wenn false (auch wenn Parameter fehlt), dann nur Datum in html.
archive: # Manuell archivieren, wenn Event vor seinem Enddatum als abgesagt unter Vergangene Veranstaltungen erscheinen soll. 

location: 
  - cloud.akarchwiss.de

publishDate: {{ .Date }}
lastmod: {{ now.Format "2006-01-02T00:00:00+01:00" }}
draft: true
author:
  - akarchwiss

summary: 

layout: event
---

Der AkArchWiss lädt all seine Mitglieder herzlich zu seinem Virtuellen Stammtisch am {{< param startDate >}} ein. 

<!--more-->

Fragen zum Stammtisch bitte im Mitglieder-Chat der Cloud stellen oder an {{< mail recipient="info@akarchwiss.de" subject="Virtueller Stammtisch [DATUM]" text="info@akarchwiss.de" >}} richten.
