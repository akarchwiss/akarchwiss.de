---
title: "{{ replace .Name "-" " " | title }}" # Nicht mehr als 70 Zeichen – Google kürzt!
subtitle: # Optional.

license: # Optional.

publishDate: {{ ((.Date | time).AddDate 0 00 7).Format "2006-01-02T00:00:00+01:00" }}
lastmod: {{ now.Format "2006-01-02T00:00:00+01:00" }}
draft: true
author: # Liste möglich. 
pdf_version: # Du kannst einen vom Ordnernamen abweichenden PDF-Name hier angeben.

# Vorschaubild für Listenübersicht:
thumb_img: images/
# Bild für Header:
header_img: images/
header_mobile_img: images/

# Ein hervorgehobener Post wird auf der Startseite gelistet:
featured: false
weight: 1
featured_headline: 
featured_img: /images/ # Absolute Pfadangabe (static/).
featured_img_alt: 
featured_summary: 

hide_header: false # Setze true für wiss. Artikel, wenn diese sehr textlastig sind (nüchternes Erscheinugsbild).

toc: false # Noch nicht implementiert.
autoCollapseToc: true # Noch nicht implementiert.
reward: false
mathjax: false

summary:

description: # Für meta-description (SEO): 70–150 Zeichen – Google kürzt!

tags: # Liste möglich. Setze ca. 5 Schlagwörter.

layout: post 
---

<!--more-->

<!-- Fotogalerie 1 -->
{{< gallery >}}

{{<figure link="images/" title="" caption="">}}
{{<figure link="images/" title="" caption="">}}

{{< /gallery >}}
<!-- Fotogalerie 1 Ende -->

<!-- Literaturangaben -->
{{< bibliography title="Lesenswertes" >}}

{{< bib-item author="Nachname, Vorname" item="Literaturtitel">}}
{{< bib-item author="Nachname, Vorname" item="Literaturtitel">}}

{{< /bibliography >}}
<!-- Literaturangaben Ende -->

<!-- Fußnoten -->
