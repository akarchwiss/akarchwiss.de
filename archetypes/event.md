---
title: "{{ replace .Name "-" " " | title }}" # Nicht mehr als 70 Zeichen – Google kürzt!
subtitle: # Optional.
slug: {{ .File.BaseFileName }}-{{ ((.Date | time).AddDate 0 00 14).Format "2006" }} # Benutzerdefinierter Pfad nach Domainname. Jahr, ggf. Monat mit aufnehmen! 

startDate: {{ ((.Date | time).AddDate 0 00 14).Format "2006-01-02T00:00:00+01:00" }}
endDate: {{ ((.Date | time).AddDate 0 00 14).Format "2006-01-02T00:00:00+01:00" }}
startDate_hhmm: true # Wenn false oder Parameter nicht gesetzt, dann nur Datum in html.
endDate_hhmm: true # Wenn false oder Parameter nicht gesetzt, dann nur Datum in html.
archive: # Manuell archivieren, wenn Event vor seinem Enddatum in Sektion "Vergangene Veranstaltungen" erscheinen soll. 

location: # Liste möglich. 

publishDate: {{ .Date }}
lastmod: {{ now.Format "2006-01-02T00:00:00+01:00" }}
draft: true
author:

header_img: images/
header_mobile_img: images/

summary: 

layout: event
---

<!--more-->

<!-- Einführung zur Veranstaltung -->

### Vorläufiger Tagesablauf:

<!-- Tagsablauf vorzugsweise in Tabellenform -->

### Termin & Anmeldung: {#registration}
<!-- registration: true # Setze die header-id "#registration" im Content! Erstellt link unter Artikel-Überschrift. -->

<!-- Ggf. nähere Informationen zum Termin; ansonsten reicht Überschrift "Anmeldung" -->

<!-- Ansprechpartner, E-Mail-Adresse (shortcode!) oder Telefonnummer -->
