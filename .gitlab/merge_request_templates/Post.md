<!-- Todos wie Erstellung einer Fotogalerie bitte als Thread oder Issue starten -->

<!-- Sollte dein MR Fehler aufweisen und Du Unterstützung bei der Korrektur wünschen,
     starte bitte ebenfalls einen Thread oder öffne ein Issue mit Link zu diesem MR. --> 

## Übersicht

Dieser Merge Request umfasst die Onlinefassung des Artikels *(Kurz-)Titel* von <Autorenschaft>.

Er wird unter der Rubrik `Berichte/Lesefunde/Wiss. Artikel` am *YYYY-MM-DD* veröffentlicht.

### Anmerkungen

<!-- Wenn Du von Konventionen oder Formatierungsregeln abweichst, erkläre hier die Hintergründe.
     Zur Verdeutlichung oder für eine Diskussion kannst Du hier auch gerne Screenshots einbetten. -->


## Review

<!-- Die folgenden Punkte müssen von den Reviewern geprüft werden müssen.
     Nicht zutreffende Punkte können durchgestrichten oder gelöscht werden.
     Siehe Diskussion unter: https://gitlab.com/akarchwiss/akarchwiss.de/-/issues/10 -->

### Allgemein

- [ ] Veröffentlichungstermin <!-- ggf. in Absprache mit FB-Team -->
- [ ] kein Entwurfsstatus
- [ ] kein Rechtschreib-/Grammatik-/Satzbaufehler
- [ ] Hugo-Syntax
- [ ] Headerbild in mobiler Ansicht
- [ ] Headerbild in Deskop-Ansicht 
- [ ] Alle Fußnoten werden aufgelöst
- [ ] Autorenschaft (keine Autorenschaft: Hugo setzt automatisch "AkArchWiss")
- [ ] Korrekte Lizenz (ohne Angabe einer Lizenz: Hugo setzt automatisch "CC BY-NC 4.0")
- [ ] PDF: korrekter Dateipfad

### Fotos

- [ ] Bilder wurden komprimiert (Größe: 200–300 KB) (Helper-Skript: `resize-image`)
- [ ] Thumbnails wurden erstellt (Helper-Skript: `create-thumb`)
- [ ] Alle Bilder laden
- [ ] Fotogalerien lockern Text an den richtigen Stellen auf (nicht zu viele Bilder in einer Sektion!)
- [ ] Captions erzählen kleine Geschichte oder rekapitulieren den Text
- [ ] Titles zu Begin eines neuen Themas/Orts gesetzt

### SEO & Usability

- [ ] Meta Description
- [ ] Schlagwörter (3–5)
- [ ] Alle internen Hyperlinks sind korrekt
- [ ] Alle externen Hyperlinks sind korrekt
- [ ] Externe Hyperlinks besitzen einen Titel
- [ ] Hierarchie der Überschriften (im Content-Bereich ab h2)
