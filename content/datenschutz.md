---
title: "Datenschutz"
aliases:
  - /nutzerinformationen/13-datenschutzerklärung

publishDate: 2021-01-25T23:37:38
lastMod: 2021-01-25T23:37:38

header_img: /images/temple-1.jpg

description: >-
  Hier findest du Informationen zum Datenschutz beim Arbeitskreis Archäologische Wissenschaften e. V.

layout: page
---

### Speicherung personenbezogener Daten

Wir sammeln keine personenbezogenen Daten, wenn solche nicht ausdrücklich in irgendeiner Form an uns gesendet werden (bspw. E-Mail-Verkehr, Mitgliedsantrag, Nutzung unserer Cloud).

### Cookies

Wir verwenden auf unserer Website keine Cookies.

### Content Delivery Network & Google Fonts API

Für die Bereitstellung der Fotogalerie greift unsere Website ggf. auf im Browser gecachte Skripte zurück,
um Traffic zu sparen.
Diese Skripte können beim Besuch anderer Websiten von Cloudflare, Inc. geladen worden sein.
Sollten diese Skripte noch nicht im Cache des Browsers vorhanden sein, werden sie vom Server des AkArchWiss e. V. nachgeladen.

Die auf unserer Website verwendete Schrift wird über Google-Server geladen,
wenn die Schriften nicht bereits im Browser-Cache vorhanden sind.
Hierbei übermittelt der Browser Daten an Google, darunter die vollständige IP-Adresse:
* [Google Fonts und Datenschutz der User](https://developers.google.com/fonts/faq#what_does_using_the_google_fonts_api_mean_for_the_privacy_of_my_users)
* [Google Privacy Terms](https://policies.google.com/privacy)
* [Google APIs Terms of Service](https://developers.google.com/terms/)


### Webserver-Protokolle

Zur Eingrenzung technischer Probleme können auf dem von uns verwendeten Webserver Informationen über den verwendeten User-Agent (Webbrowser-Software) sowie die IP-Adresse in anonymisierter Form gespeichert werden.
Die IP-Adresse wird dabei maskiert, sodass lediglich die ersten 16 Bits bei IPv4-Adressen und die ersten 32 Bits bei IPv6-Adressen protokolliert werden;
am Beispiel unserer Server-IP-Adresse: *5.181.50.249* → *5.181.0.0* und *2a03:4000:3f:522::* → *2a03:4000::.*
Nach spätestens sieben Tagen wird eine Protokolldatei gelöscht.

### Tracking- & Analysetools

Auf unserer Website verwenden wir keine Tracking- und Analysedienste.

### Soziale Netzwerke

Auf unserer Website verwenden wir keinerlei Plugins sozialer Netzwerke.
Lediglich Hyperlinks führen zu unseren Auftritten in sozialen Netzwerken.

### URL-Shortener

Beim Aufrufen eines Links über unseren URL-Shortener [go.akarchwiss.de](https://go.akarchwiss.de/ "URL-Shortener des AkArchWiss e. V.")
wird die IP-Adresse pseudonymisiert (das letzte Segment einer IPv6- oder IPv4-Adresse wird durch 0 ersetzt).
Der übrige Teil der IP-Adresse gibt Aufschluss über die nationale Herkunft des Internetdienstanbieters.

### Hyperlinks

Unsere Website verfügt über Hyperlinks zu anderen Websites, für die die jeweiligen Datenschutzerklärungen der jeweiligen Betreibenden zu beachten ist.
Der Arbeitskreis Archäologische Wissenschaften e. V. ist für die Einhaltung von Datenschutzbestimmungen auf diesen Seiten nicht verantwortlich.

Diese Website unterstützt ausschließlich verschlüsselte Verbindungen (https).

AkArchWiss e. V., 2021-03-29T212320Z.
