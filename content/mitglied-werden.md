---
title: "Mitglied werden"
subtitle: >-
  Du interessierst dich für die ehrenamtliche Mitgliedschaft in einem Verein? Dann bist du bei uns genau richtig!

publishDate: 2021-01-24
lastMod: 2021-01-24

header_img: /images/nofretete.jpg

description: >-
  Du interessierst dich für die ehrenamtliche Mitgliedschaft in einem Verein? Dann bist du beim Arbeitskreis Archäologische Wissenschaften genau richtig!


modal: true # Lädt JavaScript für das Anzeigen der Lebensläufe.
cv_logo: /images/akarchwiss-logo-stylized.svg

layout: page
---

{{< cta-button text="Ich möchte beitreten!" url="#beitritt" title="Beitrittserklärung des AkArchWiss e. V." >}}

### Wer sind wir?

Hinter dem Arbeitskreis Archäologische Wissenschaften e. V. (AkArchWiss) stehen Alumni und Studierende verschiedener kulturhistorischer Fachrichtungen. Gemein ist uns das Interesse an der (Menschheits-)Geschichte und die Freude, Wissen darüber einer breiten Öffentlichkeit zugänglich zu machen.

### Unsere aktuelle Vorstandschaft

{{< team "vorstand" >}}

### Was machen wir?

Wir organisieren Veranstaltungen wie Exkursionen, Fachvorträge und Tagungen. Unsere Homepage bietet eine Plattform zur Veröffentlichung von Texten zu kulturgeschichtlichen Themen und steht dabei sowohl Vereinsmitgliedern als auch vereinsexternen Autorenschaft offen.  
Ausgehend vom Feld der archäologisch-historischen Studien wollen wir über den Tellerrand hinausschauen und fächerübergreifende Projekte angehen. Überschneidungspunkte existieren etwa mit der Anthropologie, der Geographie, der Geologie, der Soziologie, den Religionswissenschaften, anderen Naturwissenschaften und vielen weiteren Disziplinen. Wir möchten zukünftig ein interaktives Angebot an experimenteller Archäologie, Prospektionen und Informationskursen anbieten. Darunter etwa Seminare zu Themen wie: Grabungssicherheit, private Sondagengänge, Erste Hilfe, die Zusammenstellung eines Grabungskoffers und Berufsberatung für Personen, die in der Archäologie beschäftigt sind/sein möchten.  
Obwohl der AkArchWiss bundesweit aufgestellt und aktiv ist, stehen bestimmte Regionen im Zentrum unserer bisherigen Tätigkeit. Das sind Orte, an denen wir bereits Projekte umgesetzt haben, oder Städte, in denen Unterstützerinnen und Unterstützer ihren Lebensmittelpunkt haben. Hierzu zählen: Fulda, Marburg und Würzburg.  

### Wie kannst du aktiv mitgestalten?

Du kannst (gern auch als Nicht-Mitglied) an unseren Vereinsveranstaltungen teilnehmen oder selbst aktiv werden, indem du:

* Besichtigungen von Museen, Ausstellungen, Denkmälern, Städtereisen o.ä. vorschlägst.
* Projektpläne einreichst für eine Exkursion, einen Fachvortrag, eine interaktive Veranstaltung o.ä.  
  Als aktives Mitglied erhältst du die Möglichkeit, an vielfältigen und spannenden Projekten mitzuwirken und kannst dir dabei obendrein neue Fähigkeiten aneignen. Darunter etwa:
  + Lektorieren und Layouten von Texten.
  + Gestalten unserer Internetpräsenz.

### Welche festen Teams gibt es im Verein?

Wir arbeiten sowohl in permanenten als auch in projektbezogenen temporären Teams:
*Website,*
*Social Media,*
*Layout,*
*Lektorat,*
*Podcast (im Aufbau).*

### Warum solltest du beitreten?

* weil du dich für kulturhistorische Themen interessierst.
* weil du dich aktiv im Vereinsleben einbringen möchtest.
* weil du gerne Exkursionen zu historischen Denkmälern oder Museen machst.
* weil du Lust auf kulturhistorische Veranstaltungen hast und dich auch selbst bei solchen einbringen möchtest.
* weil du dich mit anderen kulturwissenschaftlich begeisterten Menschen austauschen möchtest.
* weil du archäologische Experimente toll findest.
* weil du gerne Berichte über kulturhistorische Themen liest.

Bei weiteren Fragen stehen dir unsere
{{< mail recipient="neumitglied@akarchwiss.de" text="Neumitgliedbetreuerinnen" >}}
Maria Dobler und Synje Diekmann
zur Seite.
Außerdem veranstalten wir regelmäßig (Online-)Stammtische, an denen du Vereinsmitglieder kennenlernen und befragen kannst.

{{< anchor anchor-id="beitritt" highlight=true >}}
Du hast dich über unseren Verein informiert und mit unserem Ansprechpartner gesprochen?
Du möchtest bei uns mitgestalten?
**Hier kannst du unsere [Satzung](/docs/satzung-akarchwiss.pdf "Satzung des AkArchWiss e. V.") und [Beitrittserklärung](/docs/beitrittserklaerung-akarchwiss.pdf "Beitrittserklärung des AkArchWiss e. V.") einsehen!**
Bitte reiche mit deiner Beitrittserklärung auch ein kurzes Motivationsschreiben (ca. ½ DIN-A4-Seite) sowie die unterschriebene [Datenschutzerklärung](/docs/datenschutzerklaerung-akarchwiss.pdf "Datenschutzerklärung des AkArchWiss e. V.") ein.
{{< /anchor >}}
