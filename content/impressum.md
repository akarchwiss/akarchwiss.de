---
title: "Impressum"
aliases:
  - /nutzerinformationen/30-impressum

header_img: /images/temple-1.jpg

description: >-
  Hier findest du das Impressum des Arbeitskreis Archäologische Wissenschaften e. V.

layout: page
---

**Arbeitskreis Archäologische Wissenschaften e. V.**  
Röntgenstraße 12  
86415 Mering  
GERMANY

E-Mail: {{< mail recipient="info@akarchwiss.de" text="info@akarchwiss.de" >}}

Registernummer: VR 5221  
Registergericht: Amtsgericht Marburg  

### Einzelvertretungsberechtigung
 
* Stefan Kleinert, *Vorsitzender*
| {{< mail recipient="info@akarchwiss.de" text="E-Mail" >}}
* Philipp Kuhn, *Stellvertretender Vorsitzender für Organisation*
| {{< mail recipient="info@akarchwiss.de" subject="Organisation" text="E-Mail" >}}
* Hannes Faußner, *Stellvertretender Vorsitzender für Kommunikation*
| {{< mail recipient="info@akarchwiss.de" subject="Kommunikation" text="E-Mail" >}}
* Jamal Hudson, *Stellvertretender Vorsitzender für Finanzen*
| {{< mail recipient="info@akarchwiss.de" subject="Finanzen" text="E-Mail" >}}

### Verantwortlich für redaktionelle Inhalte

Hannes Faußner, *Stellvertretender Vorsitzender für Kommunikation*
| {{< mail recipient="info@akarchwiss.de" subject="Redaktion" text="E-Mail" >}}
