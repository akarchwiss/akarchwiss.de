---
title: "Buchbesprechung – Phillip Hoose: Sabotage nach Schulschluss"
aliases:
  - /texte/lesefunde/44-buchbesprechung-sabotage-nach-schulschluss

publishDate: 2021-01-22T08:00:00+03:00
lastmod: 2021-03-18T00:00:00+03:00

author:
  - mscheck

summary: Das hier vorgestellte Werk von Phillip Hoose erzählt die Geschichte zweier dänischer Jugendgruppen, die sich ab 1940 gegen die nationalsozialistischen Besatzer in Dänemark wehrten.

description: >-
  Zweiter Weltkrieg: Dänischer Widerstand gegen die Nationalsozialisten.
  Lesen Sie hier die Buchbesprechung zu "Sabotage nach Schulschluss" von Phillip Hose.

tags:
  - "Zweiter Weltkrieg"
  - "Nationalsozialismus"
  - "Widerstand NS"
  - "Skandinavien"
  - "Buchbesprechung"

layout: post
---

**Phillip Hoose: Sabotage nach Schulschluss. Wie wir Hitlers Pläne durchkreuzten, München 2018. ISBN: 978-3-423-71777-9. Preis: 9,95 €**

Das hier vorgestellte Werk von Phillip Hoose erzählt die Geschichte zweier dänischer Jugendgruppen, die sich ab 1940 gegen die nationalsozialistischen Besatzer in Dänemark wehrten. Hierfür sprach er mit Knud Pedersen. Dieser hatte als 14-jähriger mit seinem Bruder, seinem Cousin und Freunden den *RAF-Club* in Odense und, nach dem Umzug der Familie Pedersen nach Aalborg, mit seinem Bruder und neuen Freunden den *Churchill-Club* ins Leben gerufen:

Hoose beginnt seinen Bericht über die Geschichte der beiden Widerstandsgruppen mit dem 9. April 1940, als deutsche Flugzeuge Propagandaflugblätter über Dänemark abwarfen, die verkündeten, dass die Deutschen gekommen wären, »um die Dänen vor den finsteren Franzosen und Engländern zu ›beschützten‹«. Dänemark wurde zum Protektorat erklärt.[^1] Am gleichen Tag marschierte die Wehrmacht auch in Norwegen ein. Während sich das dänische Heer ob der feindlichen Übermacht ergab, kämpften die Norweger zwei Monate mit großer Opferbereitschaft um ihre Freiheit, auch weil sie auf die Hilfe der Engländer hofften. Diese traf verspätet und nur in unzureichendem Umfang ein, sodass die Norweger letztlich keine Chance hatten. Angestachelt vom Widerstandsgeist der Norweger und beschämt durch die Passivität ihrer Landsleute, beschlossen Knud und Jens Pedersen, ihr Cousin Hans Jørgen Andersen, Harald Holm und Knud Hedelund eine Widerstandsgruppe zu gründen, um zu zeigen, dass sich nicht alle Dänen den Nationalsozialisten fügten. Sie fürchteten, dass die Engländer und Franzosen die Dänen nie als Verbündete betrachten würden, wenn diese es den Deutschen so gemütlich machten.

Die Erzählung setzt sich mit der Schilderung der ersten Sabotage-Akte des *RAF-Clubs,* benannt nach der britischen Royal Air Force, fort. Die Jungen verübten sie auf Fahrrädern, die es ihnen ermöglichten, schnell zuzuschlagen und genauso schnell wieder davonzukommen. Als Edvard Pedersen, der Vater von Knud und Jens, 1941 an die protestantische Pfarrei in Aalborg berufen wurde und seine Familie ihn dorthin begleitete, konnten seine Söhne ihre Aktivitäten im *RAF-Club* nicht fortsetzen. Die Brüder gründeten mit neuen Freunden in Aalborg den *Churchill-Club* und legten es darauf an, wie sie den Mitgliedern des *RAF-Clubs* vor dem Umzug gelobten, eine noch stärkere Sabotagegruppe aufzubauen. In Aalborg waren Widerstandsaktionen ein deutlich riskanteres Unterfangen als in Odense, da dort weitaus mehr deutsche Soldaten stationiert waren. Dennoch gelangen den Jugendlichen einige Sabotageakte. Auch der *RAF-Club* blieb bestehen und setzte den Widerstand fort. Ihren Eltern verrieten die Jugendlichen nichts von ihren Widerstandsaktivitäten. 

Hoose gelingt mit seinem Buch eine spannende und fesselnde Schilderung über die Geschichte mehrerer Jugendlicher, die nicht tatenlos zusehen wollen, wie sich ihr Land den nationalsozialistischen Besatzern unterwirft und in dieses Schicksal fügt, sondern stattdessen für die Unabhängigkeit ihres Landes und damit auch für ihre Zukunft kämpfen wollen. Bisweilen entsteht bei der Lektüre der Eindruck, einen Abenteuerroman für eine jugendliche Zielgruppe vor sich liegen zu haben. Der Abenteuercharakter und die Leichtigkeit schwinden, als sich das Leben der Jugendlichen zum Schlechten wendet. Hoose vermeidet es nicht, auch die negativen Aspekte und Gefahren des Widerstands darzustellen. Es ist und bleibt ein gefährliches Spiel, das sie betreiben. Von den Nationalsozialisten drohen brutale Strafen. Auch unter den Dänen sind nicht alle begeistert, da sie Vergeltungsmaßnahmen durch die deutschen Soldaten fürchten, offen angedroht und schließlich auch zu spüren bekommen. Zudem profitierten einige Dänen von der deutschen Besatzung. Auch in diesem Abschnitt reißt der bestehende Erzählfluss nicht ab.

Hoose konnte bei der Arbeit für sein Buch auf einen reichen Quellenfundus zugreifen: Er sprach zwischen dem 7. und 14. Oktober 2012 täglich mit Knud Pedersen. Die Aufnahmen, die er von diesen Gesprächen machte, umfassen 25 Stunden Material. Im weiteren Entstehungsprozess seines Buchs tauschten die beiden beinahe 1000 E-Mails miteinander aus. Eine weitere wichtige Quelle Hooses war Knud Pedersens Veröffentlichung über die Geschichte des *Churchill-Clubs,* die dieser 1945 verfasste.[^2] Bei der Erstellung seines persönlichen Berichtes konnte er eine Fülle wichtiger Dokumente verwenden: Er nutzte Polizeiprotokolle, militärische Dokumente, Ministerschreiben zwischen deutschen und dänischen Behörden, Gefängnisunterlagen, Zeitungsartikel und journalistische Berichte. Auch viele Fotos konnte er auftreiben. Mit diesem Material gelang es Knud, die nachhaltige Wirkung, die von ihrer Gruppe ausgegangen war, zu belegen. Knuds Buch wurde über die Jahre mehrfach durch verschiedene Verlage neu aufgelegt. Darüber hinaus standen Hoose auch Eigil Astrup-Frederiksens Erinnerungen an seine Zeit im *Churchill-Club* aus dem Jahr 1987[^3] und ein einstündiges Telefoninterview mit der engen Freundin Knuds Patricia Bibby Heath zur Verfügung. Aufgrund des größeren Quellenmaterials, das Knud Pedersen lieferte, steht dieser unweigerlich im Zentrum der Schilderung. Die anderen Clubmitglieder kommen dennoch nicht zu kurz. Hoose gelingt ein tiefer und umfassender Blick auf die Geschichte aller Mitglieder des Widerstands. Er erzählt ihre Geschichte in leicht lesbarer Sprache, immer wieder ergänzt durch Zitate Knud Pedersens. Des Weiteren werden historische Details oder erläuternde Hintergrundinformationen, in vom Fließtext abgegrenzten Blöcken, eingeschoben.

Das Buch ist für all jene gut geeignet, die eine kurze Übersicht über die Verhältnisse in Dänemark während der Besatzung durch NS-Deutschland und die Entstehung des dänischen Widerstands erfahren möchten. Mit seinem Fokus und seinem Umfang von 237 Seiten ist es dem Buch verständlicherweise nicht möglich, auf alle historischen Aspekte während des betrachteten Zeitraums (umfassend) einzugehen. Es schildert knapp die wichtigsten Ereignisse und Hintergründe die notwendig sind, um das Handeln der Jugendlichen verständlich zu machen. Dass dadurch logischerweise Aspekte bezüglich des dänischen Widerstandes und der deutschen Gegenmaßnahmen (etwa Aktionen und Gruppen des sog. Gegenterrors) zu kurz kommen, ist nachvollziehbar, dem Autor jedoch nicht zum Vorwurf zu machen, da dies nicht sein Anspruch ist. Ein gutes Einstiegs- und Übersichtswerk stellt sein Werk dennoch dar. Durch seine gute Lesbarkeit, seine Protagonisten, den Umfang an vermitteltem Wissen und die relativ geringe Seitenzahl, bietet sich das Buch für Leser ab der Mittelstufe, als auch für den Schulunterricht dieser Jahrgangsstufen an. Mit seiner Hilfe lassen sich etwa ethisch-moralische Fragen zum Widerstand gegen eine Besatzungsmacht erörtern.

<!-- Fußnoten -->
[^1]: Hoose, Phillip: Sabotage nach Schulschluss. Wie wir Hitlers Pläne durchkreuzten, München 2018, S. 15.
[^2]: Pedersen, Knud: Churchillklubben, o. O. 1946.
[^3]: Churchill-Klubben som Eigil Foxberg oplevede, Hellasverlag 1987. (Der Churchill-Klub, wie ihn Eigil Foxberg erlebte. Frederiksen änderte seinen Namen zu Foxberg um.)
