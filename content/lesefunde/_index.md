---
title: "Lesefunde"
aliases:
  - /texte/lesefunde
  # Die Rubrik "Texte" umfasste neben "Lesefunde" auch "Wissenschaftliche Artikel".
  # Da sich unter "Lesefunde" z. Zt. mehr Inhalt befindet, wird von /texte hierher weitergeleitet. 
  - /texte
has_more_link: true
header_img: /images/artifacts-1.jpg

description: >-
  Hier findest du informative, aber auch locker und humorvoll gestaltete Texte zur Archäologie und Geschichte sowie zu verwandten Themenbereichen.

rss: true

layout: category
---
