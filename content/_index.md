---
title: "Arbeitskreis Archäologische Wissenschaften e. V."

description: >-
  Du interessierst dich für Archäologie, Geschichte und ihre Nachbardisziplinen? Dann wirf einen Blick auf unsere vielfältigen Projekte und Artikel!

layout: home
---

Du interessierst dich für Archäologie, Geschichte und ihre Nachbardisziplinen?
Dann bist du beim **Arbeitskreis Archäologische Wissenschaften e. V.** genau richtig!

Durchforste unsere Website und wirf einen Blick auf unsere Projekte und Artikel zu den unterschiedlichsten Themen.
Wenn du mehr über den Verein erfahren möchtest,
dann informieren wir dich [hier](/mitglied-werden)!

## Lesenswertes

Unsere Redaktion hat dir an dieser Stelle unsere schönsten Artikel zusammengestellt.  
[Veröffentliche deinen eigenen Beitrag auf unserer Website!](/leitfaeden "Publizieren beim AkArchWiss | Leitfäden & Richtlinien")

{{< featured_articles >}}
