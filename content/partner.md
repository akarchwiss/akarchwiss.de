---
title: "Partner des Vereins"
subtitle: >-
  Unsere Freunde und Partner in alphabetischer Auflistung

publishDate: 
lastMod: 

header_img: /images/alma-tadema-phidias-parthenon.crop.jpg

description: >-
  Gemeinsam besser ans Ziel. Hier findest Du Freunde und Partner des Arbeitskreises Archäologische Wissenschaften e. V.

layout: page
---

### Arbeitskreis zur Erforschung der Tonpfeifen

*Der Arbeitskreis Tonpfeifen widmet sich sowohl dem Objekt Tonpfeife wie auch den Menschen, die mit ihm in Verbindung standen, also den Produzenten (Pfeifenmacher), den Pfeifenhändlern und den Rauchern.
[…]
Seit Beginn der intensiveren Erforschung der Geschichte der Tonpfeifenmacherei in Deutschland durch die Mitglieder des Arbeitskreises zur Erforschung der Tonpfeifen sind eine Vielzahl von Orten entdeckt worden, in denen in den vergangenen 300 Jahren Tonpfeifen produziert wurden. Zur Zeit sind ca. 250 Produktionsorte bekannt, über die aber häufig noch keine genaueren Informationen vorliegen. Der Arbeitskreis bemüht sich um eine weitere Intensivierung solcher Forschungen und unterstützt jedes Engagement von wissenschaftlicher, öffentlicher und privater Seite. Dazu zählt in besonderem Maße die Sensibilisierung der verantwortlichen Institutionen (archäologische Bodendenkmalpflege, kulturhistorische Museen etc.), aber auch von Privatpersonen, aufgefundene Tonpfeifen stärker zu beachten und wissenschaftlich zu bearbeiten.
Eine wichtige Aufgabe ist die Entwicklung und Weiterführung vorhandener Methoden zur einheitlichen, wissenschaftlich exakten Ansprache und Darstellung der Tonpfeifen. Hierzu zählen eine einheitliche Katalogisierung der Funde, die Vereinbarung über die zeichnerische Darstellung und die Aufstellung einer Systematik der Dekore. Bei diesen Arbeiten steht der Arbeitskreis wie auch bei alle anderen Forschungen und Aktivitäten in engem Kontakt mit Forschern in Europa und in Übersee sowie der britischen »Society for Clay Pipe Research« und dem »Pijpelogischen Kring Nederland«.*  
— [Erfahre mehr über den Arbeitskreis zur Erforschung der Tonpfeifen](http://helene-bonn.info/AK/aufg.htm)

* Internetauftritt: [Website](http://helene-bonn.info/AK/index.htm)
* Ansprechpartner: Dr. Gerald Volker Grimm |
{{< mail recipient="gerald.volker.grimm@gmx.de" text="E-Mail" >}}
* Publikationen: Auflistung auf der [Website](http://helene-bonn.info/AK/publi.htm) des Arbeitskreises

### Archäologisches Spessartprojekt e. V. (ASP)

*Das Archäologische Spessartprojekt (ASP) befasst sich mit der Kulturlandschaft Spessart in all ihren unterschiedlichen Aspekten: Geschichte, Sprache, Kultur, Landschaftsentwicklung, natürliche Voraussetzungen wie Geographie, Topographie, Geologie oder Biologie. In enger Zusammenarbeit mit Universitäten und Forschungsinstituten werden Umwelt- und Klimaveränderungen, die Spuren der Waldnutzung durch die Jahrtausende und die gegenseitige Beeinflussung von Mensch und Natur erforscht. Mit Hilfe geophysikalischer Messungen, Pollenanalyse, der Dendrochronologie, archäologischer Prospektionen und Grabungen, sowie mit Fernaufklärung per Satellit, Luftbildern, der Auswertung von Archivalien, und der Kartierung von Zeigerpflanzen oder Bewuchsmerkmalen wird die Geschichte einer lange vernachlässigten Kulturlandschaft rekonstruiert.
Alle Daten werden in einem Geographischen Informationssystem (GIS) zusammengeführt, wodurch erstmals ein grenzübergreifendes (bayerisch-hessisches) und umfassendes Bild des Spessart entsteht.
Im Jahre 1997 beteiligte sich das ASP an European Cultural Paths, das vom RAPHAEL Programm der EU gefördert wurde. Im Anschluss daran entwickelte das ASP seit 2001 das Projekt Pathways to Cultural Landscapes, das mit 12 Partnern in 10 europäischen Ländern eine Förderung von drei Jahren im Rahmen des Programms CULTURE 2000 der EU erhielt
Derzeit ist das ASP in verschiedenen europäischen Projekten aktiv und als beratende Nichtregierungsorganisation beim Europarat in die Umsetzung der Europäischen Landschaftskonvention eingebunden.
Neben der Forschung bemüht sich das Projekt vor allem um die Vermittlung der Kulturlandschaft an Bewohner und Touristen, besonders durch die Einrichtung von Kulturwegen, Produktion populärer Publikationen, Ausstellungen, Vorträgen, Seminaren, die Ausbildung von Landschaftsführern, Projektarbeit mit Kindern und eine intensive Pressearbeit.*  
— [Erfahre mehr über das Archäologische Spessartprojekt e. V.](https://www.spessartprojekt.de/spessartprojekt/)


* Internetauftritt:[Website](https://www.spessartprojekt.de/), [Facebook](https://www.facebook.com/Spessartprojekt/)
* Ansprechpartner: Dr. Harald Rosmanitz |
{{< mail recipient="rosmanitz@spessartprojekt.de" text="E-Mail" >}}
* [An-Institut an der Julius-Maximilians-Universität Würzburg](https://www.spessartprojekt.de/spessartprojekt/das-an-institut-an-der-universitaet-wuerzburg/)
* Publikationen: Auflistung auf der [Website](https://www.spessartprojekt.de/spessartprojekt/spessartprojekt-publikationen/) des Vereins

### Vonderau Museum Fulda

*Gegründet wurde das Vonderau Museum im Jahr 1875 mit der Schenkung einer privaten Sammlung. Benannt wurde es nach Heimatforscher Joseph Vonderau, der bei Ausgrabungen zahlreiche Objekte aus der Steinzeit, Bronzezeit und Eisenzeit entdeckte. Als langjähriger ehrenamtlicher Leiter entwickelte er die Sammlung und Präsentation des Museums weiter.
Heute beherbergt das Museum drei Dauerausstellungen zur Kulturgeschichte, Naturkunde, Malerei und Skulptur mit regionaler Ausrichtung sowie ein Klein-Planetarium.*  
— [Erfahre mehr über das Vonderau Museum Fulda](https://www.fulda.de/kultur-freizeit/vonderau-museum/das-museum)

* Website: [Website](https://www.fulda.de/kultur-freizeit/vonderau-museum)
* Ansprechpartner: 
  * Museumsleitung: Dr. Frank Verse |
  {{< mail recipient="frank.verse@fulda.de" text="E-Mail" >}}
  * Stadt- und Kreisarchäologie: Milena Wingenfeld M. A. |
  {{< mail recipient="milena.wingenfeld@fulda.de" text="E-Mail" >}}
* Förderverein des Museums: [Freunde des Museums Fulda e. V.](https://www.freundedesmuseums.de/)

### Interesse an einer Kooperation mit uns?

Möchtet auch ihr Teil des Freundeskreies des Arbeitskreis Archäologische Wissenschaften e. V. werden
und mit uns gemeinsame Projekte durchführen?
Dann steht euch
{{< mail recipient="info@akarchwiss.de" subject="Anfrage: Partner des Vereins" text="Stefan Kleinert" >}}
als Ansprechpartner zur Seite.
