---
title: "Exkursion: Darmstadt November 2015"
aliases:
  - /blog/exkursionsberichte/20-exkursion-darmstadt-landesmuseum-hessen-sonderausstellung-homo

publishDate: 2015-11-11

header_img: images/hessisches-landesmuseum-darmstadt-herbst.jpg

description: >-
  Zur Sonderausstellung »Homo – Expanding Worlds« im Hessischen Landesmuseum Darmstadt (2015):
  Lies hier den Exkursionsbericht des AkArchWiss.
tags:
  - Evolution
  - Hessen
  - Landesarchäologie
  - Exkursion

layout: post 
---

Die Exkursion des AkArchWiss führte im November 2015 nach Darmstadt.

<!--more-->

Dort wurde die Sonderausstellung des [Hessischen Landesmuseums Darmstadt](https://www.hlmd.de/ "Hessisches Landesmuseum Darmstadt | hlmd") *»Homo – Expanding Worlds«* besucht.
Diese widmete sich der Entstehung des Menschen. Im Anschluss daran wurde der Vortrag »Ursprünge, Umbrüche, Umwege – 6 Millionen Jahre Mensch« von Prof. Dr. Friedmann Schrenk besucht.
