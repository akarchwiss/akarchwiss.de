---
title: "Exkursion: Fulda Oktober 2015"
aliases:
  - /blog/exkursionsberichte/21-exkursion-fulda-vonderau-museum-stadtführung-2015

publishDate: 2015-11-02

header_img: images/fulda-dom-st-salvator.jpg

description: >-
  Zu einer Ausstellung über die Landschaft Rhön im Vonderau Museum in Fulda:
  Lies hier die Zusammenfassung über den Exkursionstag des AkArchWiss.
tags:
  - Rhön
  - Vonderau Museum
  - Exkursion

layout: post 
---

Die erste Exkursion des AkArchWiss führte im Oktober 2015 nach Fulda.

<!--more-->

Dort wurde die Sonderausstellung des Vonderau Museums: »Die Rhön – Geschichte einer Landschaft« besucht. Diese stellte die Entstehung der Natur- und Kulturlandschaft Rhön, die ersten Besiedlungen dieser Landschaft bishin zur jüngsten Vergangenheit dar. Im Anschluss daran folgte die Teilnahme an einer Stadtführung durch die Fuldaer Altstadt.

<!-- Fotogalerie 1 -->
{{< gallery >}}

{{<figure link="images/fulda-stadtschloss-eingang.jpg" title="Stadtschloss Fulda" caption="Hier begann die Führung durch die Stadt Fulda.">}}
{{<figure link="images/fulda-fachwerkhaus.jpg" title="Fachwerkhaus" caption="Ein Beispiel für die Fachwerkarchitektur in der Stadt Fulda.">}}
{{<figure link="images/fulda-dom-st-salvator.jpg" title="Dom St. Salvator" caption="Dom der Stadt Fulda, errichtet zwischen 1704 und 1712.">}}

{{< /gallery >}}
<!-- Fotogalerie 1 Ende -->
