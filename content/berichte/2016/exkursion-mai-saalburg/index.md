---
title: "Mai-Exkursion: Römerkastell Saalburg 2016"
aliases:
  - /blog/exkursionsberichte/22-exkursion-bad-homburg-saalburgmuseum-römerkastell-2016

publishDate: 2016-05-01

header_img: images/saalburg-tor.jpg

description: >-
  In das UNESCO-Welterbe Römerkastell Saalburg führte die Exkursion am 30.4.2016 des AkArchWiss.
  Lies hier die Zusammenfassung über den Exkursionstag.
tags:
  - Antike
  - Römer
  - Exkursion

layout: post 
---

Am 30. April 2016 fand eine Exkursion zum [Römerkastell Saalburg](https://www.saalburgmuseum.de/ "Römerkastell Saalburg | Saalburgmuseum") (Teil des UNESCO-Welterbes *Obergermanisch-Raetischer Limes)* im Archäologischen Park bei Bad Homburg statt.

<!--more-->

Thematisch ging es an diesem Tag um römische Soldatenlager, speziell um die 4. Vindelikerkohorte und der Cohors Secunda Raetorum.


<!-- Fotogalerie 1 -->
{{< gallery >}}

{{<figure link="images/saalburg-tor.jpg" title="Saalburg" caption="Rekonstruierte Toranlage der Saalburg." >}}
{{<figure link="images/vierte-vindelikerkohorte-saalburg-2016.jpg" title="Auf dem Exerzierplatz" caption="Die 4. Vindelikerkohorte.">}}
{{<figure link="images/roemische-schildkroetenformation-testudo-saalburg-2016-1.jpg" caption="In Testudo …">}}
{{<figure link="images/roemische-schildkroetenformation-testudo-saalburg-2016-2.jpg" caption="… auch Schildkrötenformation genannt.">}}

{{< /gallery >}}
<!-- Fotogalerie 1 Ende -->
