---
title: "Exkursion: hessenARCHÄOLOGIE-Tag 2016"
aliases:
    - /blog/exkursionsberichte/23-xkursion-vortrag-hessenarchäologie-tag-2016-hünfeld

publishDate: 2016-11-06

header_img: images/keltenhof-mackenzell-haeuser.jpg

description: >-
  Der AkArchWiss beim hessenARCHÄOLOGIE-Tag 2016 in Hünfeld:
  Lies hier die Zusammenfassung über einen Tag mit Fachvorträgen und Kurzexkursionen.
tags:
  - Hessen
  - Kelten
  - Landesarchäologie
  - Tagung
  - Exkursion

layout: post 
---

Am 05. November 2016 unternahm der AkArchWiss eine Exkursion zum [hessenARCHÄOLOGIE-Tag 2016](https://lfd.hessen.de/service/hessenarchäologie-tag "hessenARCHÄOLOGIE-Tag | lfd hessen") in der Stadthalle in Hünfeld.

<!--more-->

Dort wurden über den Tag verteilt mehrere archäologische Fachvorträge gehalten. Thematisch bewegten sich diese Vorträge, allerdings nicht strikt chronologisch, von der Bronzezeit bis zu den Napoleonischen Kriegen.
Während der Mittagspause wurden des Weiteren Kurzexkursionen durch die Stadt Hünfeld oder aber zum [Keltenhof bei Mackenzell](http://www.zuse-museum-huenfeld.de/aussenstelle-keltenhof-mackenzell-2.html "Keltenhof Mackenzell | Konrad-Zuse-Museum Hünfeld") angeboten.
Die Vereinsmitglieder besichtigten den Keltenhof.


<!-- Fotogalerie 1 -->
{{< gallery >}}

{{<figure link="images/keltenhof-mackenzell-haeuser.jpg" title="Keltenhof bei Mackenzell" caption="Nachbauten keltischer Häuser.">}}
{{<figure link="images/vortragssaal-hessenarchaeologie-2016-huenfeld.jpg" title="Stadthalle Hünfeld" caption="Hier fanden die Vorträge des Vortragstags der hessischen Landesarchäologie »hessenARCHÄOLOGIE-Tag« statt.">}}

{{< /gallery >}}
<!-- Fotogalerie 1 Ende -->
