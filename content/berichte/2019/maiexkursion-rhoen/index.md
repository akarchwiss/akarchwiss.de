---
title: "Mai-Exkursion in die Rhön 2019" # Nicht mehr als 70 Zeichen – Google kürzt!
subtitle: # Optional.
aliases:
  - /blog/exkursionsberichte/31-mai-exkursion-in-die-rhön-2019

license: # Optional.

publishDate: 2019-06-01T09:30:00+03:00
lastmod: 2021-03-24T00:00:00+03:00
author:
  - fjordan
  - skleinert
pdf_version: # Du kannst einen vom Ordnernamen abweichenden PDF-Name hier angeben.

# Vorschaubild für Listenübersicht:
thumb_img: images/milseburg-fernblick-rhoen.jpg
# Bild für Header:
header_img: images/milseburg-fernblick-rhoen.jpg
pdf_version: # Du kannst einen vom Ordnernamen abweichenden PDF-Name hier angeben.

# Ein hervorgehobener Post wird auf der Startseite gelistet:
featured: false
weight: 1
featured_headline:
featured_img: /images/ # Absolute Pfadangabe (static/).
featured_img_alt:
featured_summary:

hide_header: false # Setze true für wiss. Artikel, wenn diese sehr textlastig sind (nüchternes Erscheinugsbild).

toc: false # Noch nicht implementiert.
autoCollapseToc: true # Noch nicht implementiert.
reward: false
mathjax: false

summary: >-
  Sonne, strahlender Himmel und ein Tag in der Rhön, so zumindest hatte es die Planung vorgesehen.
  Der Wetterbericht hingegen meldete kühlere Temperaturen und Regenschauer, was am Ende in einem dichten Schneetreiben auf der Milseburg endete.
  Der Ausflug in den Mai.

description: >-
  Mache mit uns einen Ausflug in die Rhön zur vorgeschichtlichen Höhensiedlung auf der Milseburg und ins Fränkische Freilandmuseum Fladungen!

tags:
  - Exkursion
  - Rhön
  - Vonderau Museum 
  - Franken

layout: post
---
### Samstag 04.05.2019

Sonne, strahlender Himmel und ein Tag in der Rhön, so zumindest hatte es die Planung vorgesehen. Der Wetterbericht hingegen meldete kühlere Temperaturen und Regenschauer, was am Ende in einem dichten Schneetreiben auf der Milseburg endete. Der Ausflug in den Mai.

Am Samstag, den 4. Mai, fanden wir uns zahlreich vor dem [Vonderau Museum Fulda](https://www.fulda.de/kultur-freizeit/vonderau-museum/ "Vonderau Museum | Stadt Fulda") ein. Dort starteten wir unseren Ausflug mit einem Rundgang in der Archäologischen Abteilung unter Führung von Frau S. Rössner, Doktorandin und Stipendiatin des Landkreis Fulda. Anschließend stellte Frau Rössner aktuelle wissenschaftliche Ergebnisse und Neufunde in den Magazinen vor. Für ihre Promotion über [die Höhensiedlung auf der Milseburg](https://www.uni-marburg.de/de/fb06/vfg/forschung/laufend/ezhessen/die-milseburg-bei-danzwiesen-lkr-fulda "Die Milseburg bei Danzwiesen, Lkr. Fulda | Univ. Marburg") wünschen wir vom Verein alles Gute und bedanken uns für die Führung und Fundschau.

Umfassend über die Milseburg informiert brachen wir zum Gang ins Gelände auf. Die Sonne war zwischenzeitlich zurückgekehrt, der Niederschlag hatte geendet – der Schnee war immer noch da. Die Milseburg bei Kleinsassen ist das bedeutendste vorgeschichtlich, archäologische Denkmal der hessischen Rhön. Erste Untersuchungen fanden zunächst im 19. Jahrhundert statt. Hauptaugenmerk galt den noch mächtig erhaltenen Steinwällen. Erst zu Beginn des 20. Jahrhunderts fand die Milseburg weiterführende Betrachtung: Anerkennung als prähistorisches Denkmal, topographische Aufnahmen der Wallverläufe, erste Ausgrabungen. Mit dieser ersten Hochphase der Erforschung sind viele Namen von Heimatforschern und frühen Archäologen, der noch im Entstehen befindlichen Vorgeschichtlichen Archäologie verbunden. Besonders für die archäologische Erforschung machte sich der Heimatforscher J. Vonderau verdient, indem er eine Vielzahl von archäologischen Stätten untersuchte und Artefakte vor der Zerstörung rettete. Nach ihm ist heute das Vonderau Museum in Fulda benannt. Vonderau führte schließlich 1931 alle, bis dato gemachten Erkenntnisse in einer Publikation zusammen, die auf viele Jahrzehnte die Grundlage zur Milseburg und Höhenbefestigungen im Fuldaer Raum bildete. Neben vereinzelten Lesefunden, wurden Untersuchungen erst wieder 2003 und 2004 durch den Stadt- und Kreisarchäologen Herrn Dr. M. Müller aufgenommen. Die örtliche Grabungsleitung hatte Frau Dr. U. Söder inne, die derzeit im Vor- und Frühgeschichtliches Seminar der Philipps-Universität Marburg tätig ist. Die Wallgrabung lieferte Erkenntnisse zur Gestaltung und Umfang der ursprünglichen Mauerdimensionen, die heute mit einer Volumenrekonstruktion sichtbar gemacht sind.

Aufgrund jüngster Ausgrabungen durch Frau Dr. Söder von 2014 bis 2016, lässt sich auf eine deutlich ältere Besiedlung, der in der Vergangenheit als latènezeitliche Befestigung angesprochenen Milseburg schließen. Neben urnenfelderzeitlichen Siedlungsresten, die während der Walluntersuchungen 2003/2004 entdeckt wurden, fanden sich im Bereich der Kernanlage Besiedlungsreste von der älteren Eisenzeit bis in die Mittellatènezeit. Bisher ist nur ein Bruchteil der Siedlungsfläche wissenschaftlich dokumentiert. Die Ergebnisse zeigen aber, dass der Milseburg in ihrem Umfeld eine herausragende Stellung zukam. Die schiere Größe der Anlage mit ihren massiven Mauern und hochwertigen Keramik sowie Metallerzeugnissen lassen den Platz als Zentralort erscheinen. Unsere Geländeexkursion führte uns zunächst vorbei an Wallresten zum »Vonderau-Denkmal«. Nach einer kurzen Verschnaufphase folgte der Aufstieg über den Kälberhutstein, bei dem wir für einen ersten Fernblick innehielten.

Anschließend folgten wir einem Pfad zur Gangolfsquelle – das feste Schuhwerk machte sich bezahlt. Der unter normalen Bedinungen bereits abenteuerliche Weg, war witterungsbedingt sehr rutschig und matschig, was ein Fortkommen erschwerte. Anschließend stiegen wir weiter hinauf und besichtigten die Grabungsstelle und ihr Umfeld. Nach einer kurzen Pause und Erläuterung der Grabungsergebnisse im Gelände setzten wir zum Gipfelsturm an, den wir bei einsetzenden Schneefall auch unbeschadet bestanden. Am Gipfel hissten wir unser Banner. Den Blick über das Umland schweifend wurde erst der Umfang der Anlage in der Größe und als Landmarke erkennbar. Nach langem Verweilen und schweifenden Blicken machten wir uns durchfroren an den Abstieg. Einen gelungenen Abschluss bildete das Abendessen im Fuldaer Haus, in dem wir den Tag ausklingen ließen.

<!-- Fotogalerie Tag 1: Milseburg -->
{{< gallery >}}

{{<figure link="images/milseburg-verschneiter-pfad.jpg" title="Milseburg" caption="Zum Aufstieg zur Milseburg erwarteten uns verschneite Pfade.">}}
{{<figure link="images/milseburg-gedenkstein-bischoff-kopp.jpg" title="" caption="Ein Gedenkstein erinnert an den Bischof Georg von Kopp, einst Bischof von Fulda.">}}
{{<figure link="images/milseburg-gipfel-schneetreiben-1.jpg" title="" caption="Durch das schneereiche Wetter bot uns der Gipfel leider nicht den erhofften Ausblick.">}}
{{<figure link="images/milseburg-gipfel-schneetreiben-2.jpg" title="" caption="Beim Abstieg begann das Wetter jedoch langsam aufzuklaren.">}}
{{<figure link="images/milseburg-rutschiger-pfad.jpg" title="" caption="Und als die rutschigen Pfade erst einmal überwunden waren …">}}
{{<figure link="images/milseburg-fernblick.jpg" title="" caption="… konnten wir doch noch einen schönen Ausblick genießen.">}}

{{< /gallery >}}
<!-- Fotogalerie Tag 1: Milseburg Ende -->

### Sonntag 05.05.2019

Der Sonntag führte den AkArchWiss e. V. schließlich von der hessischen Rhön in den nördlichsten Landkreis Bayerns, Rhön-Grabfeld. Zur Mittagszeit fand sich die Gruppe im Gasthaus Schwarzer Adler zum Genuss der lokalen Fränkischen Küche ein. Dieses Gasthaus ist jedoch kein gewöhnlicher Gasthof, sondern bereits Teil der Ausstellung historischer Fachwerkhäuser, die im [Fränkischen Freilandmuseum Fladungen](https://www.freilandmuseum-fladungen.de/ "Fränkisches Freilandmuseum | Freilandmuseum Flandungen") zu bestaunen und erkunden sind. Bei Sonnenschein folgte eine fast zweistündige Führung durch das Museum, die einen sehr informativen Einblick in die Geschichte einzelner Gebäude aus ganz Unterfranken bot. Die Museumsexponate sind Stein getreue Aufbauten von historischen Gebäuden, die durch den Wiederaufbau und die Restauration in Fladungen vor dem undokumentierten Abriss oder dem völligen Verfall bewahrt worden sind. Die Informationstafeln erzählen aus der Geschichte des jeweiligen Gebäudes und der einstigen Umgebung. Die Informationen reichen dabei von der Struktur eines historischen Dorfes, über Architektur-, Technik-, und Werkstoffgeschichte, bis hin zu dessen Sozialgeschichte. Dabei ist die Historie vieler Häuser an Personen der Alltagsgeschichte der letzten Jahrhunderte geknüpft, die den Exponaten somit einen lebhaften Charakter verleihen. Die meisten Objekte geben in ihrem Inneren Einblick in das Mobiliar der jeweils dargestellten Zeitstellung. Über Analysen der Wandverputze und Tapeten konnten weitere Rückschlüsse auf mehrphasige Nutzung, Farb- und Verputzstoffe und Stil gezogen werden, was sich an mehreren Wänden über Aufschlüsse der konservatorisch aufgearbeiteten Schichtabfolgen betrachten lässt. Trotz eines gut dreistündigen Aufenthalts im Freilichtmuseum, konnte der Arbeitskreis nur einen Teil des, mehrere Hektar umfassenden Geländes erkunden, weshalb ein erneuter Besuch in diesem Kleinod unterfränkischer Geschichte sicherlich einen weiteren Vereinsausflug wert wäre.

Nach den Impressionen des Museums stand der Besuch des [Schwarzen Moores](https://www.rhoen.de/urlaub-kultur-ferien-wellness/wandern-natur/lehrpfade/1729.Naturlehrpfad-Schwarzes-Moor.html "Schwarzes Moor | Rhön GmbH") an, welches sich im bayerischen Teil des sog. Dreiländerecks Hessen, Bayern und Thüringen befindet, wobei die Landschaft des Hochmoores im Rahmen einer anderen Exkursion in die Region bewandert werden soll. Schließlich fand das Exkursionswochenende des *Arbeitskreis Archäologische Wissenschaften e. V.* einen schönen Ausklang in der Fahrt durch die Rhön, gen Fulda, von wo aus jeder wieder rechtzeitig zum Start in die neue Woche aufbrechen konnte.

Wir blicken gemeinsam zurück auf eine Exkursion voller Einblicke in die jüngste archäologische Forschung der Region, das beeindruckende Vonderau Museum Fulda, die wirsche, doch berauschende Natur der Milseburg und des Schwarzen Moores, beste Verköstigung im Fuldaer Haus und im Schwarzen Adler, einer erholsamen Unterkunft in der Jugendherberge Oberbernhards, am Fuße der Milseburg, dem picturesquen Freilandmuseum Fladungen, und schließen, mit bestem Dank an alle Teilnehmer mit denen dieser unvergessene Ausflug so gut gelingen konnte.

<!-- Fotogalerie Tag 2: Fladungen -->
{{< gallery >}}

{{<figure link="images/fladungen-historische-steinbruecke.jpg" title="Fladungen" caption="In Freilandmuseum Fladungen überquerten wir zunächst eine Stein für Stein umgesiedelte Brücke …">}}
{{<figure link="images/fladungen-dorfstrasse.jpg" title="" caption="… um historische ländliche Bauten zu besichtigen. Dazu zählten u. a. das wiedererrichtete Gemeindebrauhaus aus Alsleben …">}}
{{<figure link="images/fladungen-kuratiekirche.jpg" title="Kuratiekirche" caption="… und die aus Leutershausen umgesetzte Kuratiekirche.">}}
{{<figure link="images/fladungen-oelschlagmuehle-wasserradwelle.jpg" title="Ölschlagmühle" caption="Einen Höhepunkt für die Teilnehmer stellte die Ölschlagmühle aus Wiesthal dar. Sich drehende Wasserradwelle.">}}
{{<figure link="images/fladungen-oelschlagmuehle-kammrad-stockrad.jpg" title="" caption="Die ursprünglich im 19. Jh. errichtete Mühle kommt auch heute noch mehrmals im Jahr zum Einsatz, um Öl aus z. B. Raps, Mohn oder Leinsamen zu gewinnen. Kammrad und Stockrad greifen ineinander.">}}
{{<figure link="images/fladungen-oelschlagmuehle-mahlwerk.jpg" title="" caption="Auch wenn das Müllerhandwerk einst als unehrlich galt, so zog doch das Klappern der beeindruckenden technischen Konstruktionen … Tonnenschweres Mahlwerk.">}}
{{<figure link="images/fladungen-oelschlagmuehle.jpg" title="" caption="… und das Rauschen am Bach die Teilnehmer in seinen Bann.">}}

{{< /gallery >}}
<!-- Fotogalerie Tag 2: Fladungen Ende -->
