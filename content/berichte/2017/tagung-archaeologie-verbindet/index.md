---
title: "Tagung: Archäologie verbindet"
subtitle: "Ein Diskurs zwischen Wissenschaft, Beruf und Ehrenamt"
aliases:
  - /blog/vortragsreihen/vortragsreihe-archäologie-verbindet

publishDate: 2018-03-03T16:01:23+01:00

thumb_img: images/fulda-stadtmauer-featured.jpg
header_img: images/fulda-stadtmauer.jpg

featured: true
weight: 5
featured_headline: "Tagung 2017"
featured_img: /berichte/2017/tagung-archaeologie-verbindet/images/fulda-stadtmauer-featured.jpg
featured_img_alt: "Reste der mittelalterlichen Stadtmauer in Fulda (Grabungssituation)."

description: >-
  Die Tagung »Archäologie verbindet – ein Diskurs zwischen Wissenschaft, Beruf und Ehrenamt« fand am 8.10.2017 statt.
  Lies hier die Zusammenfassung.
tags:
  - Tagung
  - Ehrenamt
  - Wissenschaft
  - Beruf
  - Archäologie verbindet
  - Vonderau Museum

summary: Am 8.10.2017 fand im Vonderau Museum (Fulda) die 1. Tagung des AkArchWiss statt.

layout: post
---

Am 8. Oktober 2017 richtete der AkArchWiss, mit freundlicher Unterstützung durch das Vonderau Museum (Fulda), seine erste Tagung in den Räumlichkeiten des Museums aus. Der Titel der Veranstaltung lautete: *»Archäologie verbindet. Ein Diskurs zwischen Wissenschaft, Beruf und Ehrenamt.«* Dazu hielten acht Redner aus verschiedenen Bereichen der Archäologie Vorträge über die Forschungsarbeit, über die Tücken des archäologischen Berufs und wie sich diese überwinden lassen und über den Beitrag ehrenamtlicher Arbeit zur Wissenschaft. Aufgrund des thematischen Rahmens waren die Schwerpunkte der Vorträge breit gesteckt. Eine Vielzahl von Gästen erschien zur Tagung. Für die Mittagspause hatte der AkArchWiss eine historische Führung durch die Fuldaer Innenstadt organisiert. Diese führte unter anderem zu mittelalterlichen Denkmälern, die teilweise – wie etwa Abschnitte der mittelalterlichen Stadtmauer – im Jahr der Tagung wissenschaftlich untersucht wurden. Abschließend sei ein besonderer Dank an Frau Dr. Sabine Fechter für die Bereitstellung der Museumsräumlichkeiten, Herrn Dr. Frank Verse für seine Hilfe bei der Organisation und seine umfangreiche Mitwirkung auf der Tagung, Frau Marita Glaser für die historische Führung durch die Fuldaer Innenstadt, allen Rednern für ihre Unterstützung und den zahlreichen Gästen für ihr Erscheinen ausgesprochen. 

### Vortragsprogramm

| Uhrzeit     | Referent                                                      | Thema/Programmpunkt                                                                                                                                         |
|-------------|---------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
| 10:00 10:30 | Frank Verse, Dr.; Sabine Fechter, Dr.; Stefan Kleinert, B. A. | Eröffnung und Grußworte durch Vertreter der Stadt- und Kreisarchäologie Fulda und des AkArchWiss e. V.                                               |
| 10:30 11:15 | Christian Lotz, B. A.                                         | Archäologie an Schauplätzen des Zweiten Weltkrieges und des Ost-West-Konfliktes im Landkreis Fulda                                                  |
| 11:15 12:00 | Marius Lippert, M. A.                                         | Waffen und Kampfdarstellungen auf den Felsbildern der Nordischen Bronzezeit                                                                         |
| 12:00 12:45 | David Berthel, B. A.                                          | Die bandkeramische Höhlennutzung auf der Schwäbischen und Fränkischen Alb                                                                           |
| 12:45 15:00 | Mittagspause                                                  | Stadtführung durch Altstadt und gemeinsames Mittagessen                                                                                             |
| 15:00 15:45 | Sascha Pifko, M. A.                                           | Archäologie als Beruf: Zwischen Professionalisierung und Ehrenamt                                                                                   |
| 15:45 16:30 | Thomas Reiser, M. A.                                          | Die DEGUWA und ihr Einsatz für das Unterwasserkulturerbe – Ein zivilgesellschaftlicher Spagat zwischen Wissenschaft und Ehrenamt; Taucharchäologie. |
| 16:30 17:00 | Nachmittagspause                                              |                                                                                                                                                     |
| 17:00 17:45 | Jürgen Schneider                                              | Das Ehrenamt als Leiter der archäologischen Abteilung des Hünfelder Konrad-Zuse-Museums und Durchführung von Geländeprospektierungen                |
| 17:45 18:30 | Patrick Albert, B. A.                                         | Konzept und Ausstellung – Glyptothek München                                                                                                        |
| 18:30 19:30 | Abendpause                                                    |                                                                                                                                                     |
| 19:30 21:00 | Frank Verse, Dr.                                              | Aktuelle archäologische Forschung in Osthessen                                                                                                      |

<!-- Fotogalerie 1 -->
{{< gallery >}}
	 {{< figure link="images/litfasssaeule-vonderau-museum-tagungsplakat-2017.jpg" caption="Litfaßsäule mit Tagungsplakat." >}}
	 {{< figure link="images/fulda-stadtmauer.jpg" caption="Reste der mittelalterlichen Stadtmauer in Fulda (Grabungssituation)." >}}
	 {{< figure link="images/fulda-vonderau-museum.jpg" caption="Der Vortragsort: das Vonderau Museum in Fulda." >}}
{{< /gallery >}}
<!-- Fotogalerie 1 Ende -->
