---
title: "Besichtigung der Villa rustica in Hechingen-Stein"
subtitle: # Optional.

license: # Optional.

publishDate: 2021-05-23T12:00:00+05:00
lastmod: 2021-05-05T00:00:00+05:00
author:
  - mscheck

pdf_version: # Du kannst einen vom Ordnernamen abweichenden PDF-Name hier angeben.

# Vorschaubild für Listenübersicht:
thumb_img: images/villa-rustica-hechingen-stein-anlage-hechingen.jpg
# Bild für Header:
header_img: images/villa-rustica-hechingen-stein-eckrisalit.jpg
header_mobile_img: images/villa-rustica-hechingen-stein-anlage-hechingen.jpg

# Ein hervorgehobener Post wird auf der Startseite gelistet:
featured: false
weight: 1
featured_headline:
featured_img: /images/ # Absolute Pfadangabe (static/).
featured_img_alt:
featured_summary:

hide_header: false # Setze true für wiss. Artikel, wenn diese sehr textlastig sind (nüchternes Erscheinugsbild).

toc: false # Noch nicht implementiert.
autoCollapseToc: true # Noch nicht implementiert.
reward: false
mathjax: false

summary: >-
  Da der übliche Mai-Ausflug mit Mitgliedern und Freunden des AkArchWiss aufgrund der Coronapandemie nicht stattfinden konnte, möchten wir stattdessen mit euch einen virtuellen Mai-Ausflug zur Villa rustica in Hechingen-Stein unternehmen.

description: >-
  Besichtigt mit uns virtuell die rekonstruierte Villa rustica in Hechingen-Stein.

tags:
  - Antike
  - Baden-Württemberg
  - Exkursion
  - Römer

layout: post
---

Die bislang teilrekonstruierte römische Villa rustica liegt im Wald nördlich des Dorfes Stein, das wiederum etwa vier Kilometer nordwestlich der Hechinger Kernstadt liegt. Das Gelände der Anlage betritt der Besucher rechts der rekonstruierten Toranlage durch eine Öffnung in der westlichen Ummauerung. Dort führt ein Weg zwischen der rekonstruierten Schmiede und den Grundmauern eines Wohnhauses hindurch. Auf der Karte ist das Wohnhaus als *Nebengebäude* benannt. Der Torbau ist heute verschlossen. Das eigentliche, umzäunte Museumsgelände das derzeit das Badegebäude, Hauptgebäude, die Wirtschaftsbauten sowie die Tempelanlage umfasst betritt der Besucher unweit des Badegebäudes.

{{< gallery >}}

      {{<figure link="images/villa-rustica-hechingen-stein-anlage-hechingen.jpg" title="Rekonstruktionszeichnung der gesamten Anlage" caption="Quelle der Rekonstruktionszeichnung: Schautafel des Museums.">}}
      {{<figure link="images/villa-rustica-hechingen-stein-torbau-aussen.jpg" title="Der rekonstruierte Torbau" caption="">}}

{{< /gallery >}}

### Villae rusticae:

Bei Villae rusticae handelt es sich um römische Gutshöfe, die zur Versorgung der römischen Truppen, aber auch der zivilen Bevölkerung in den Städten und auf dem Land dienten. Sie dienten der landwirtschaftlichen Erschließung eroberter Gebiete. Für Baden-Württemberg konnten bislang über 1500 Villae rusticae entdeckt werden. Die Gutsanlagen wurden stets nach dem selben Schema errichtet: Das Hauptgebäude mit den Wohnräumen für den Besitzer oder Pächter und seine Familie stellte den Mittelpunkt der Anlage dar, um den herum sich Wirtschaftsgebäude, Stallungen, Scheunen und Speicher gruppierten. Weiterhin gehörte ein Badegebäude bzw. ein im Hauptraum eingebautes Bad in der Regel zu jeder Villa rustica. In seltenen Fällen war es im Nebengebäude anzutreffen. Eine Hofmauer umschloss das gesamte Hofareal zum Schutz vor wilden Tieren wie Wölfen oder Bären. Darüber hinaus kann man davon ausgehen, dass die Mauern bis zu einem gewissen Maß auch als Schutz vor menschlichen Räubern dienten, wie mehr als zwei Meter hohe Mauern, mitsamt vier Ecktürmen und Toranlagen an verschiedenen Gutshöfen[^1] und die vermutlich dazu gehörenden Wachen nahelegen. Auch der Zweck der landwirtschaftlichen Erschließung eroberter Gebiete und die damit einhergehende Lage weit im Feindesland belegen die Notwendigkeit sich gegen menschliche Angreifer verteidigen zu können. Teil der Ausstattung der Anlagen war auch ein kleiner Familienfriedhof in unmittelbarer Nähe zum Gutshof. Es lässt sich nicht genau sagen, wie groß die Flächen waren, die eine Villa rustica bewirtschaftete. Mit etwa 50 bis 100 Hektar pro Betriebseinheit kann je nach Bodenqualität gerechnet werden. Trotz einer gewissen Vereinheitlichung bestanden dennoch teils erhebliche Unterschiede zwischen den einzelnen Anlage. Ausschlaggebend hierfür waren etwa die Lage der Villae[^2] der Reichtum der Besitzer und die Art der Nutzung der Anlagen, wie sich anhand der Anlage von Hechingen-Stein erörtern lässt.

{{< gallery >}}

      {{<figure link="images/villa-rustica-hechingen-stein-karte-der-anlage.jpg" title="Karte der Anlage" caption="Um die Anordnung des Guthofareals vor Augen zu haben hier eine Karte der Anlage, wie sie im Freilichtmuseum zu sehen ist.">}}
      {{<figure link="images/villa-rustica-hechingen-stein-modell-garten-badegebaeude.jpg" title="Modell des Hauptgebäudes" caption="Das Modell des vollständig rekonstruierten Haupthauses steht im Keller desselben. Hier der Blick auf den Garten und das Badegebäude.">}}

{{< /gallery >}}

Mit dem Begriff Villa bezeichnete der römische Schriftsteller Cato der Ältere um etwa 150 v. Chr. ein römisches Bauernhaus mit dazugehörigem Acker- und Weideland, einschließlich von Personal und Viehbestand, die zur Bewirtschaftung notwendig waren. Er unterschied den Wohntrakt als Villa urbana gegenüber dem Rest der Anlage als Villa rustica.[^3] Der römische Schriftsteller Columella unterschied mit dem Aufkommen von aufwändigerer, teils geradezu palastartiger Haupthäuser in der ersten Hälfte des ersten Jahrhunderts zwischen dem Haupthaus als Villa urbana und den Wirtschaftsbauten als Villa rustica. Daneben existiert der Begriff der Villa pseudourbana, der wohl vom römischen Architekten Vitruv bereits im ersten Jahrhundert v. Chr. geprägt wurde, mit dem ein prächtiges, stadtähnliches Haus auf dem Lande bezeichnet wird, in dessen Mittelpunkt Hof und Küche stehen. Konkret handelte es sich dabei um eine Luxusvilla, die sich im Bau an städtischen Häusern, den Villae suburbanae, orientierte.[^4] Eine Villa suburbana war wiederum ein aufwändig ausgestattetes Haupthaus mitsamt dazugehörigen Nebengebäuden, das einem Angehörigen der wohlhabenden Gesellschaftsschichten gehörte und somit eher als Sommerwohnsitz zu verstehen ist.

### Die Villa rustica in Hechingen-Stein:

Im Bautyp ist das Haupthaus der Villa rustica in Hechingen-Stein eine so genannte Portikusvilla mit Eckrisaliten. Der Begriff Portikus bezeichnet sowohl einen Säulengang als auch eine Säulenhalle. Das Haupthaus der Villa in Hechingen-Stein weist beide Architekturbestandteile auf. Bei den Eckrisaliten handelt es sich um die zwei aus der Front des Hauptgebäudes hervorragenden Ecktürme. Der hier anzutreffende Bautyp könnte seine Vorbilder in der altorientalisch-syrischen Architektur des achten bis siebten Jahrhunderts v. Chr. gehabt haben. Dieses Motiv fand sich in der persischen Hofarchitektur außerordentlich häufig und tauchte um 550 v. Chr. am zweiten Tyrannenpalast im griechischen Larisa erstmals auf dem europäischen Festland auf. Durch die griechische Kolonialpolitik dürfte sich diese Bauform über den Balkan weiter nach Westen, als auch nach Nordafrika verbreitet haben und gelangte schließlich durch die griechischen Apoikien[^5] bzw. Poleis[^6] in Südfrankreich, wie etwa Massalia oder Nikaia[^7], nach Gallien und von dort in die übrigen späteren römischen Provinzen.

{{< gallery >}}

      {{<figure link="images/villa-rustica-hechingen-stein-modell-gartenzugang.jpg" title="Modellansicht: Gartzugang zur Säulenhalle zwischen den Eckrisaliten" caption="Zugang vom Garten in die Porticus.">}}
      {{<figure link="images/villa-rustica-hechingen-stein-eckrisalit.jpg" title="Rekonstruierter Eckrisalit" caption="">}}
      {{<figure link="images/villa-rustica-hechingen-stein-porticus-eckrisalit.jpg" title="Porticus und anschließender Eckrisalit" caption="Außenansicht der Säulenhalle und des anschließenden Eckrisalit.">}}

{{< /gallery >}}

Die provinzialrömische Anlage in Hechingen-Stein wurde am Ende des ersten Jahrhunderts gegründet. Für die erste Bauphase wird ein hölzerner Bau angenommen, der sich jedoch nicht gesichert nachweisen ließ. Die gewählte Halbhanglage des Hauptgebäudes ist typisch für römerzeitliche Einzelgehöfte.[^8] Vom ersten bis zum dritten Jahrhundert folgten sieben Umbauten an der gesamten Anlage. Zwischen 233 und 260 wurde die Villa rustica aufgegeben, wobei die Gebäude wohl planmäßig geräumt wurden. Es lassen sich keine Brandspuren oder Zerstörungen, durch die in der Mitte des dritten Jahrhunderts in die römischen Provinzen nördlich der Alpen einfallenden alamannischen Personengruppen feststellen. Nördlich des Hauptgebäudes konnte eine Nachnutzung in Form alamannischer Holzbauten, die wahrscheinlich im vierten Jahrhundert errichtet wurden, nachgewiesen werden. An den Steingebäuden wurden ebenfalls nachrömische Veränderungen vorgenommen.

### Entdeckung der Anlage:

Entdeckt wurde die Anlage in den 1970er Jahren. 1971 hatte der Bürgermeister der Gemeinde Stein auf der Suche nach einem mittelalterlichen Weiler im Wald Tufelbach Mauerreste, Ziegelbruchstücke und einige Eisengegenstände gefunden und seine Entdeckungen an das zuständige Landesdenkmalamt in Tübingen gemeldet. Aufgrund der Namen umliegender Fluren wie Azelisgärten und Hofstätten ging man zunächst von einer abgegangenen Hofstelle des Mittelalters aus. Durch weitere Funde zeigte sich jedoch rasch, dass es sich hierbei um eine Villa rustica handelte. Die ungefähren Ausmaße der Villa rustica ließen sich durch Geländebeobachtungen und kleinere Sondagen in Grundzügen erfassen. Die ummauerte Anlage gehört zu den größeren ihrer Art in Baden-Württemberg. Stefan Schmidt-Lawrenz, langjähriger Archäologe und wissenschaftlicher Leiter auf dem Gelände der Villa, stellte in seiner Dissertation die Vermutung auf, dass die Villa rustica »in ihrer Größe, der Architektur, der Ausstattung und des Fundmaterials nur schwerlich als einfaches, landwirtschaftlich genutztes Gut zu deuten ist.«[^9] Stattdessen vermutet er, dass die Anlage aufgrund ihrer besonderen Lage »zumindest nach dem großen Umbau am Ende des zweiten Jahrhunderts, nicht nur bzw. nicht mehr die Funktion einer rein landwirtschaftlichen Anlage erfüllte, sondern vielmehr auch als Straßenstation, Handelsumschlagplatz und Herberge diente.«[^10] Hierfür führt er die Lage und Ausstattung als Beleg an. Die Anlage selbst wurde in der Provinz Germania superior, unweit der Grenze zu Rätien, errichtet. Genauer gesagt, lag sie direkt in der Mitte zwischen den bedeutenden Niederlassungen von Burladingen[^11] in Rätien und Rottenburg[^12] in Germania superior. Unweit von Hechingen-Stein verläuft über das Starzeltal eine Verbindung von der Donau über die Schwäbische Alb bis ins Neckartal bei Rottenburg. Diese trifft nördlich von Rangendingen auf die Straße zwischen Rottweil und Rottenburg. Somit lag die Villa rustica von Hechingen-Stein an einer sehr wichtigen Verkehrsverbindung in unmittelbarer Nähe der Provinzgrenze, weshalb Schmidt-Lawrenz ihr eine wichtige Rolle im Grenzhandel zuschreibt. Die gemachten Funde weisen Beziehungen zu beiden Provinzen auf. Für einen rein landwirtschaftlichen Bau besitzt der Standort der Anlage zudem eine zu schlechte Bodenqualität auf.[^13] Weitere Belege sieht Schmidt-Lawrenz darin, dass der monumentale Ausbau des Hauptgebäudes darauf ausgerichtet war, dieses früh von der südlich davon verlaufenden Straße aus eindrucksvoll sichtbar zu machen. Die zeitgleiche Errichtung und gleichartige Ausstattung der Räume in der Nordflucht des Hauptgebäudes deutet er als Schlafräume für Durchreisende. Die Größe des Tempelbezirks mit zehn Kultbauten legt die Vermutung nahe, dass er nicht nur von den Bewohnern der Villa rustica genutzt wurde, sondern auch Reisenden zur Religionsausübung zur Verfügung gestellt wurde.[^14] Bei einer alleinigen Nutzung durch die Bewohner dürfte eher mit einem Gebetsbereich unmittelbar im Haupthaus oder einem eigenständigen Kultgebäude unweit davon gerechnet werden. Letzteres fand sich etwa bei der Villa rustica bei Bondorf.[^15] Zwar weist das Hauptgebäude in Hechingen-Stein eine außergewöhnliche Größe auf, allerdings besitzt es einen für Villae rusticae typischen Grundriss. Trotz der zuvor genannten Vermutungen hält Schmidt-Lawrence am Begriff Villa rustica fest und hält Bezeichnungen wie Villa urbana, pseudourbana oder suburbana für unpassend.[^16]

### Ausgrabungen:

1977 wurde der Förderverein Römisches Freilichtmuseum Hechingen-Stein e. V. ins Leben gerufen, dessen Mitglieder sich seither um den Erhalt der Anlage bemühen.[^17] Im gleichen Jahr musste der Baumbestand im Bereich des angenommenen Hauptgebäudes aufgrund von Windbruch und Rotfäule reduziert werden. Zu diesem Zeitpunkt hatten bereits Raubgräber Grabungen vorgenommen. Um Zerstörungen und mögliche Fundraube durch solche Raubgrabungen zu verhindern, wurden archäologische Untersuchungen durch das Landesamtdenkmalamt Tübingen durchgeführt. Von 1978 bis 1981 erfolgten vier Grabungskampagnen, durch die das Hauptgebäude und ein Badegebäude der Anlage freigelegt und erforscht werden konnten. Nach einer Unterbrechung[^18], wurden die Grabungen 1992 wieder aufgenommen und werden seither als Dauergrabung fortgeführt. Außerhalb der Ummauerung des Gutshofes wurde ein kleiner heiliger Bezirk aufgedeckt, in dem die Fundamente von zehn kleineren Kultbauten freigelegt wurden. An der westlichen Mauer der Villa fand sich der Haupteingang, ein Mühlengebäude, ein Speicherbau, eine Schmiede, ein weiteres Wohnhaus und – mit großem Abstand zu den übrigen Gebäuden – auch ein Eckturm.

### Rekonstruktion der Villa rustica:

Die Ausgrabungen fanden bereits früh Anklang in der Öffentlichkeit und so entstand auch bald der Wunsch, die Funde in einem Museum zugänglich zu machen. Um dies in die Tat umsetzen zu können, wurde der Verein zur Erforschung und Erhaltung der Kulturdenkmale in Hechingen-Stein ins Leben gerufen. Auf dessen Initiative hin entstand der Plan, die Villa Rustica zu rekonstruieren. Die erste Rekonstruktionsphase erfolgte von 1985 bis 1991. Sie wurde lange Zeit geplant und erfolgte unter maßgeblicher Beteiligung des Landesdenkmalamtes in Tübingen. Als erstes begann die Wiedererrichtung des südöstlichen Eckrisaliten, des Säulengangs und von Teilen der Säulenhalle. Dabei orientierte man sich an den verbliebenen Resten und der bildlichen Überlieferung römischer Gutshöfe in Italien und den römischen Provinzen.[^19] Vorsicht ist dadurch geboten, dass der Villa-Bautyp mit Eckrisaliten in der Villen-Architektur in Italien nicht nachgewiesen werden konnte.[^20] Hingegen gehört er zum häufigsten Grundrisstyp in der Gallia Belgica und den beiden germanischen Provinzen. Die existierenden Kenntnisse reichen allerdings dennoch nicht aus, um eine einwandfreie Rekonstruktion einer solchen Anlage zu ermöglichen, sodass für das Ergebnis keine Restzweifel verbleiben. Über die Ausgestaltung und Funktion der Räume im Hauptgebäude können anhand der Grabungsbefunde und des Fundmaterials nur unzureichende Aussagen getroffen werden. Nachweisen ließen sich etwa einfache Lehmböden, Bretterböden und terrazzoähnliche Kalkmörtelestriche. Lediglich drei Räume wiesen eine Unterbodenheizung auf. Die Wände waren verputzt, weiß getüncht, einfarbig bemalt oder mit mehrfarbiger Streifenmalerei versehen. Durch vereinzelte Putzfragmente lassen sich Rückschlüsse auf figürlichen und pflanzlichen Dekor ziehen. Weiterhin fanden sich Bruchstücke von gegossenen Glasscheiben, die vermutlich von verglasten Holzsprossenfenstern stammen. Vom Mobiliar, wie es in Form von Bänken, Betten, Kasten, Schränken, Sesseln, Tischen und Truhen existiert haben muss, fanden sich nur noch die Metallteile aus Bronze und Eisen (Beschläge, Scharniere, Schlösser und Schlüssel). Die Reste der Grundmauern und eines kleinen Abschnitts des aufgehenden Mauerwerks lassen wiederum nur wenige oder überhaupt keine Aussagen über das exakte Aussehen ganzer Stockwerke, anzunehmendem Schmuck an den Außenwänden und die Dachkonstruktion der Hechinger Villa rustica zu. Die Orientierung an Abbildungen oder Funden anderer Anlagen begünstigt Verfälschungen, da auch Bauten der gleichen Gebäudeart lokale Notwendigkeiten oder persönliche Bedürfnisse und Wünsche der Erbauer widerspiegelten. Dementsprechend kam auch Kritik aus der Fachwelt an diesem Projekt auf. Nichtsdestotrotz ermöglicht eine Rekonstruktion der interessierten Öffentlichkeit ein greifbareres Abbild vom möglichen Aussehen eines solchen Gutshofs.

Wer die Rekonstruktion von Anfang an verfolgen konnte, erfuhr einiges über den Wandel der Forschungsmeinung zum Aussehen der Anlage. So ging man in der ersten Rekonstruktionsphase noch davon aus, dass der Gutshof – wie die aus dem italischen Raum bekannten Vorbilder – mit offenem Hof und nach Innen geneigten Dächern angelegt worden war. Eine solche Bauweise wäre jedoch nicht für die klimatischen Verhältnisse Süddeutschlands mit teils starken Regenfällen geeignet. Ein nach innen geneigtes Dach – das in den trockeneren Regionen das rare Regenwasser gezielt in den Hof leiten soll – hätte hier dafür gesorgt, dass der Hof und wahrscheinlich auch die Kellerräume unter Wasser gesetzt worden wären. Folglich änderten sich Anfang der 2000er Jahre die Ansichten und die Verantwortlichen gingen nun für die weitere Rekonstruktion des Hauptgebäudes von einer geschlossenen Überdachung der Halle mit Hilfe eines Giebeldaches aus. Im Originalbefund zeigte sich, dass die Mauern an jenen Stellen, an denen die größte Last des Daches auflag, verstärkt worden waren. Dies wird als Beleg für die regional veränderte Bauweise und deren entsprechenden Rekonstruktion angesehen.[^21]

{{< gallery >}}

{{<figure link="images/villa-rustica-hechingen-stein-modell-von-oben.jpg" title="Blick auf das Dach des Modell" caption="In dieser Ansicht sieht man gut, welche Dachkonstruktion bei einer Villa rustica im südwestdeutschen Raum am wahrscheinlichsten ist.">}}

{{< /gallery >}}

### Rundgang durch die Anlage:

Die heute sichtbare und in Teilen rekonstruierte Umfassungsmauer weist eine Länge von ca. 240 m in Nord-Süd-Richtung und ca. 140 m in West-Ost-Richtung auf, sodass eine Fläche von etwa 4 Hektar umschlossen wird. Das umschlossene Areal ist ungewöhnlich groß. Die meisten Gutshöfe umfassen eine kleinere Fläche. Es gibt Hinweise darauf, dass diese Hofmauer aus der letzten Bauphase, im dritten Jahrhundert, stammt und das Gelände der Anlage verkleinerte.[^22] Sie besaß eine Höhe von etwa 2,4 m und wurde durch Ziegel abgedeckt. Im Führer des Römischen Freilichtmuseums wird ─ wohl angesichts des Befunds ─ ein repräsentativer Zweck der Mauern genannt.[^23] Dies spiegelt sich auch in der Rekonstruktion der Umfassungsmauern wider. Es ist nicht ausgeschlossen, dass die Besitzer sich einen gewissen Schutz vor menschlichen Plünderern erhofften. Mit ihrer Höhe, dem stabilen Torbau und der Aussicht, die die Türme boten ist die Hofmauer nicht unbedingt leicht und völlig unbemerkt ─ sofern man von Wachposten in den Türmen ausgehen kann ─ zu überwinden. Argumente für den möglichen Wunsch nach einer Absicherung vor Plünderungen stellen die Bauzeit im dritten Jahrhundert ─ in dem die Römer zunehmend mit einheimischen Widerstand in den germanischen Provinzen zu kämpfen hatten ─ sowie die Grenznähe der Anlage dar. Einem ernsthaften Angriff ─ noch dazu mit entsprechenden Geräten ─ hätten sie wohl nicht langen standgehalten.

{{< gallery >}}

{{<figure link="images/villa-rustica-hechingen-stein-westmauer.jpg" title="Blick auf die westliche Umfassungsmauer" caption="">}}
{{<figure link="images/villa-rustica-hechingen-stein-umfassungsmauer.jpg" title="Blick auf die Westmauer vom rekonstruierten Eckturm" caption="">}}
{{<figure link="images/villa-rustica-hechingen-stein-torbau-innen.jpg" title="Torbau der Anlage von innen betrachtet" caption="">}}

{{< /gallery >}}

Auch das Hauptgebäude in Hechingen-Stein ist mit seiner Länge von 47 m außergewöhnlich groß für einen Gutshof. Die Hauptgebäude durchschnittlicher Villae, wie etwa die Villa in Lauffen, weisen in ihrem Wohnbereich eine Länge von 23 m auf. Längen zwischen 20 m und 30 m scheinen für viele Villae Standard zu sein. Während für Lauffen eine Wohnfläche von 414 m² errechnet wurde, verfügte der Wohnbau in Hechingen-Stein wohl über 560 m². Der hohe Wert rührt vom angenommenen überdachten Innenhof her, welcher der Wohnfläche zugerechnet wird.[^24]

Heute sind die westliche Ummauerung, das Eingangsportal, die dahinter befindliche Schmiede, Teile des Säulengangs vom Badegebäude zum Haupthaus, der südöstliche Eckrisalit, die Hälfte der an den südöstlichen Eckrisaliten anschließenden Säulenhalle, Teile der Innenhalle des Hauptgebäudes (mitsamt Unter- und Obergeschoss), der nordwestliche Eckturm der Anlage, einige Mauerabschnitte eines Wohnbaus und Teile des im südwestlichen Bereich der Anlage befindlichen Tempelbezirks mit aufgehender Architektur rekonstruiert. Die Grundmauern der restlichen Anlage wurden bei den Ausgrabungen konserviert und gesichert. Dem Hauptgebäude vorgelagert befindet sich ein Badegebäude mit Latrine. Wer den Bereich des Badegebäudes betritt, kommt auf die Latrine zu. Diese ist aus Holz nachgebildet. Vom Eingangsbereich des Badegebäudes blickt man auf die Unterbodenheizung des Tepidariums[^25] Links davon befindet sich das Caldarium[^26] rechts das Frigidarium[^27].

{{< gallery >}}

{{<figure link="images/villa-rustica-hechingen-stein-latrine.jpg" title="Die Latrine im Badegebäude" caption="">}}
{{<figure link="images/villa-rustica-hechingen-stein-badegebaeude.jpg" title="Blick über die Grundmauern des Badeshauses" caption="">}}

{{< /gallery >}}

Vom Badegebäude kommend, kann der Besucher sich entweder in den Kräutergarten oder durch den teilrekonstruierten Säulengang, der das Badegebäude mit dem Hauptgebäude verbindet, in das Haupthaus begeben. Im Erdgeschoss des südöstlichen Eckrisalits befindet sich bspw. ein Ausstellungsraum mit Originalfunden. Von dort aus kann man die ca. 30 m lange und mit offenen Rundbogenfenstern ausgestattete Säulenhalle durchschreiten. Bislang ist die Außentreppe vom Garten zur Säulenhalle noch nicht abgeschlossenen, weshalb man sie nicht von innen betreten kann. Stattdessen gelangt man von der Säulenhalle aus in die halboffene Innenhalle. Von dort aus kann man sich etwa in das rekonstruierte Triclinium[^28] oder ins Lapidarium[^29] begeben, in dem Teile von Säulen und Skulpturen ausgestellt sind. Die Wandbemalung im Triclinium orientiert sich an den aufgefundenen Resten des bemalten Wandverputzes. Wer vor der Innenhalle stehenbleibt, sieht rechts noch weitere Räumlichkeiten: Sie sind als Präfurnium[^30] Im vorderen Teil sieht man Deicheln[^31], zudem ausgewählte Objekte der Architektur der Villa. Rückwärtig dazu, in einem etwas tiefergelegenen Bereich, befindet sich der eigentliche Brennofen. In einer in den Boden eingelassenen Grube liegt der Feuerofen, der durch einen Heizkanal mit dem Triclinium verbunden ist. So werden die heißen Feuerungsgase unter dem Raum hindurchgeführt und durch weitere Kanäle in den Wänden ins Freie abgeleitet. Mit dieser Technik konnten Wände und Fußböden beheizt werden. Weiterhin befinden sich in der Innenhalle drei Treppen: zwei aus Stein, eine aus Holz. Die Steintreppen führen ins Untergeschoss: Dort können ein vollständiges Modell der Villa rustica und die Lagerräume besichtigt werden. Die Holztreppe führt ins Obergeschoss: Dort sind nachgebaute Wohn-, Schlaf- und Arbeitsräume mit Möbeln und Wandmalereien, deren Möblierung und Gestaltung Grabungsbefunden aus Pompeji und zeitgenössischen Darstellungen nachempfunden wurden, zu sehen.

{{< gallery >}}

{{<figure link="images/villa-rustica-hechingen-stein-garten.jpg" title="Blick durch den Garten auf den Säulengang" caption="Der Säulengang verband das Haupthaus mit dem Bad">}}
{{<figure link="images/villa-rustica-hechingen-stein-porticus-innen.jpg" title="Säulenhalle von innen" caption="">}}
{{<figure link="images/villa-rustica-hechingen-stein-triclinium.jpg" title="Speisezimmer" caption="">}}
{{<figure link="images/villa-rustica-hechingen-stein-praefurnium.jpg" title="Brennofen zur Raumbeheizung" caption="Brennofen mit dem die Fußbodenheizung betrieben wurde.">}}
{{<figure link="images/villa-rustica-hechingen-stein-arbeitsbereich.jpg" title="Arbeitsbereich" caption="Den Schlaf- und Wohnräumlichkeiten vorgelagerter Arbeitsbereich im Obergeschoss des Haupthauses.">}}
{{<figure link="images/villa-rustica-hechingen-stein-schlafraum.jpg" title="Schlafraum" caption="">}}
{{<figure link="images/villa-rustica-hechingen-stein-wohnbereich.jpg" title="Wohnbereich" caption="">}}

{{< /gallery >}}

Der ursprüngliche Eingangsbereich des Hauptgebäudes ist noch nicht rekonstruiert. Links und rechts des Eingangs befanden sich Küchenräume. Wer den rechten Küchenraum betritt, blickt auf einen Torbogen, der ursprünglich den Zugang zu den, unter der Säulenhalle befindlichen, Kellerräumen darstellte. Der Torbogen wurde fast vollständig am Boden liegend aufgefunden. Die erhaltenen Teile sind aus Stubensandstein, die nicht erhalten gebliebenen Teile wurden aus Beton angefertigt, damit der Bogen wiedererrichtet werden konnte. Hinter dem Küchenraum befindet sich ein langrechteckiger Raum, der als Horreum[^32] angesprochen wird. Gegenüber des Horreums liegt ein Wohnraum, dessen Fußbodenheizung in Teilen offengelegt ist.

{{< gallery >}}

{{<figure link="images/villa-rustica-hechingen-stein-rechter-kuechenraum.jpg" title="Küchenraum am Haupteingang des Haupthauses" caption="">}}
{{<figure link="images/villa-rustica-hechingen-stein-torbogen-keller.jpg" title="Torbogen zum Keller" caption="">}}
{{<figure link="images/villa-rustica-hechingen-stein-horreum.jpg" title="Horreum neben der Küche" caption="">}}
{{<figure link="images/villa-rustica-hechingen-stein-wohnraum-gegenueber-horreum.jpg" title="Wohnraum gegenüber dem Horreum" caption="Der Boden ist offen, sodass man die Fußbodenheizung ─ lat. Hypokaustum ─ sehen kann.">}}

{{< /gallery >}}

Im südwestlichen Teil der Anlage wurden die Grundmauern eines Speicherbaus sowie Mühlengebäudes angetroffen. Der Speicherbau war 20 m × 14 m groß. Von ihm haben sich nur noch die Fundamente erhalten, während es keine Anhaltspunkte über das aufgehende Mauerwerk gibt. Die Nähe zum Mühlengebäude sowie fehlende Spuren zur Raumaufteilung legen nahe, dass es sich um einen Getreidespeicher oder eine Scheune handelte. Der Innenraum dürfte mit Holz ausgebaut gewesen sein. Für das Mühlengebäude werden zwei Stockwerke angenommen. Heute sind die Grundmauern teilweise noch bis in eine Höhe von zwei Metern erhalten. Im Untergeschoss fanden sich die Reste von drei Darren[^33], mit denen frisch geerntetes Getreide für die Lagerung und das Mahlen vorbereitet wurden. Auf diesen Trocknungsöfen konnte man aber auch Fisch, Fleisch oder Obst trocknen, um es haltbar zu machen. Die Darren hatten eine Seitenlänge von drei Metern und waren annähernd quadratisch. Sie wurden von Norden her durch einen Kanal beheizt. Neben einer Darre fanden sich zwei Mühlsteine, die Teil einer sog. Göpelmühle waren. Solche Mühlen wurden von einem Esel oder Maultier angetrieben, wodurch sie eine Mahlleistung von rund 20 kg in der Stunde erreichten. Der Fund eines großen Steinbeckens mit Abflussloch nördlich des Mühlengebäudes deutet darauf hin, dass dort auch Cervisia[^34] gebraut wurde. Mit der Schmiede, die unweit des Torbaus liegt, wurde ein Wirtschaftsbau bereits rekonstruiert.

{{< gallery >}}

{{<figure link="images/villa-rustica-hechingen-stein-speicherbau.jpg" title="Grundmauern des Speicherbaus" caption="">}}
{{<figure link="images/villa-rustica-hechingen-stein-muehlengebaeude.jpg" title="Grundmauern des Mühlengebäudes" caption="">}}
{{<figure link="images/villa-rustica-hechingen-stein-schmiede.jpg" title="Schmiede" caption="">}}

{{< /gallery >}}

Noch ein Stück weiter südwestlich dieses Gebäudes befindet sich der Tempelbezirk. Die Rekonstruktion des Tempelbezirks ist noch immer in vollem Gange. Die Besitzer der Villa ließen ihn gegen Ende des zweiten Jahrhunderts zunächst auf einem Areal von 23 m × 30 m errichten. Im dritten Jahrhundert wurde er auf 30 m × 30 m erweitert. Dieses Areal erhielt eine eigene Umfassungsmauer. Im Inneren haben sich die Fundamente von zehn kleinen Aediculae[^35] erhalten, die einen annähernd quadratischen Grundriss aufweisen. Im Tempelbezirk sind heute Nachbildungen von Götterdarstellungen, die an verschiedenen baden-württembergischen Fundorten gefunden wurden, ausgestellt – darunter: Epona[^36], Herecura[^37], Minerva[^38], Genius[^39] und Merkur[^40]. Das Hochrelief Merkurs ist dem Fund aus Sumelocenna nachgebildet.

{{< gallery >}}

{{<figure link="images/villa-rustica-hechingen-stein-modell-tempelbezirk.jpg" title="Modell des rekonstruierten Tempelbezirks" caption="">}}
{{<figure link="images/villa-rustica-hechingen-stein-aediculae.jpg" title="Rekonstruierte Aediculae" caption="Rekonstruierte Aediculae. Aediculae sind kleine Tempel.">}}
{{<figure link="images/villa-rustica-hechingen-stein-grundmauern-aedicula.jpg" title="Grundmauern eines Aedicula" caption="">}}

{{< /gallery >}}

Hinter einem Wohnhaus, das nach Betreten der Anlage zur Linken des neuzeitlichen Besuchers liegt, befindet sich die Rekonstruktion eines der vier vermuteten Ecktürme: der sog. nordwestliche Eckturm. In diesem befand sich die Küche des Wohnbaus. Der Turm ist in seinem Grundriss annähernd quadratisch. Die Erhaltung der Mauern mit bis zu zehn Steinlagen ist sehr gut. Vom Turm aus kann der Besucher einen schönen abschließenden Blick auf die gesamte Anlage sowie die sich im Hintergrund erhebende Burg Hohenzollern werfen.

{{< gallery >}}

{{<figure link="images/villa-rustica-hechingen-stein-grundmauern-wohnhaus-eckturm.jpg" title="Wohnhaus am heutigen Eingang zur Anlage" caption="">}}
{{<figure link="images/villa-rustica-hechingen-stein-eckturm.jpg" title="Westlicher Eckturm" caption="">}}
{{<figure link="images/villa-rustica-hechingen-stein-westturm.jpg" title="Blick vom westlichen Eckturm" caption="Ein abschließender Blick nach Süden vom westlichen Eckturm über die Anlage hinweg. Am Horizont ist die Burg Hohenzollern zu sehen.">}}

{{< /gallery >}}

<!-- Literaturangaben -->
{{< bibliography title="Lesenswertes" >}}

{{< bib-item
  author="Stefan Schmidt-Lawrenz"
  title="Das Haupt- und Badegebäude der Villa rustica von Hechingen-Stein, Zollernalbkreis (Grabungen 1978-1981). Dissertation zur Erlangung des Doktorgrades der Philosophie an der Fakultät für Kulturwissenschaften, Institut für Ur- und Frühgeschichte der Eberhard-Karls-Universität Tübingen. [Online abgerufen](http://www.villa-rustica.de/index.php/de/die-villa/ausgrabungen/1978-1981.html \"Die komplette Dissertation von Herrn Schmidt-Lawrenz zum Download | Römisches Freilichtmuseum Hechingen-Stein e.V.\")">}}

  {{< bib-item
    author="Römisches Freilichtmuseum Hechingen-Stein e.V."
    title="Website des Freilichtmuseums mit Informationen zur Geschichte der Anlage, der Entdeckung, der Ausgrabungen und Rekonstruktion. [Online abgerufen](http://www.villa-rustica.de/index.php/de/ \"Startseite der Website | Römisches Freilichtmuseum Hechingen-Stein e.V.\")">}}

{{< /bibliography >}}
<!-- Literaturangaben Ende -->

<!-- Fußnoten -->
[^1]: Wie in den Gutshöfen Hechingen-Stein oder Bondorf im Kreis Böblingen.
[^2]: Dies umschließt die Lage innerhalb des gesicherten römischen Einflussgebiets oder noch zu erschließenden Gebieten. Dort wiederum ist die Nähe zu Kastellen sicherlich entscheidend. All dies dürfte etwa zu Anpassungen in der Sicherung der Anlage geführt haben.
[^3]: Schmidt-Lawrenz, Stefan: Das Haupt- und Badegebäude der Villa rustica von Hechingen-Stein, Zollernalbkreis (Grabungen 1978-1981). Dissertation zur Erlangung des Doktorgrades der Philosophie an der Fakultät für Kulturwissenschaften, Institut für Ur- und Frühgeschichte der Eberhard-Karls-Universität Tübingen, S. 229.
[^4]: Schmidt-Lawrenz: Das Haupt- und Badegebäude der Villa rustica von Hechingen-Stein, Zollernalbkreis (Grabungen 1978-1981), S. 230.
[^5]: Bezeichnung der von griechischen Siedlern gegründeten Kolonien der Antike.
[^6]: Der städtische Siedlungskern und das dazugehörige Umland antiker griechischer Gründungen.
[^7]: Griechische Namen von Marseille und Nizza.
[^8]: Schmidt-Lawrenz: Das Haupt- und Badegebäude der Villa rustica von Hechingen-Stein, Zollernalbkreis (Grabungen 1978-1981), S. 14.
[^9]: Schmidt-Lawrenz: Das Haupt- und Badegebäude der Villa rustica von Hechingen-Stein, Zollernalbkreis (Grabungen 1978-1981), S. 9.
[^10]: Schmidt-Lawrenz: Das Haupt- und Badegebäude der Villa rustica von Hechingen-Stein, Zollernalbkreis (Grabungen 1978-1981), S. 240.
[^11]: Das Kastell und der Vicus Burladingen lagen an einem der wichtigsten Albübergänge.
[^12]: Der römische Name der Siedlung war Sumelocenna.
[^13]: Römisches Freilichtmuseum Hechingen-Stein e.V., Einführung, [online abgerufen](http://www.villa-rustica.de/index.php/de/die-villa/eintrittspreise.html).
[^14]: Schmidt-Lawrenz: Das Haupt- und Badegebäude der Villa rustica von Hechingen-Stein, Zollernalbkreis (Grabungen 1978-1981), S. 239 f.
[^15]: Planck, Dieter: Die römische Villa rustica bei Bondorf. Aus dem Grabungsbericht des Denkmalamtes Stuttgart, [online abgerufen](https://zeitreise-bb.de/villa-2/).
[^16]: Schmidt-Lawrenz: Das Haupt- und Badegebäude der Villa rustica von Hechingen-Stein, Zollernalbkreis (Grabungen 1978-1981), S. 238.
[^17]: Römisches Freilichtmuseum Hechingen-Stein e.V., 1978 - 1981 Erste Grabungskampagne Hauptgebäude, [online abgerufen](http://www.villa-rustica.de/index.php/de/die-villa/ausgrabungen/1978-1981.html).
[^18]: Die Unterbrechung zwischen den Grabungskampagnen wurde durch die Diskussion über die Realisierung einer Rekonstruktion der Anlage, als auch der ersten Rekonstruktionsphase verlängert.
[^19]: Römisches Freilichtmuseum Hechingen-Stein e.V., Rekonstruktion 1985, [online abgerufen](http://www.villa-rustica.de/index.php/de/die-villa/rekonstruktion-1985.html).
[^20]: Schmidt-Lawrenz: Das Haupt- und Badegebäude der Villa rustica von Hechingen-Stein, Zollernalbkreis (Grabungen 1978-1981), S. 228.
[^21]: Römisches Freilichtmuseum Hechingen-Stein e.V., Rekonstruktion 2005, [online abgerufen](http://www.villa-rustica.de/index.php/de/die-villa/rekonstruktion-2005.html).
[^22]: Römisches Freilichtmuseum Hechingen-Stein e.V., Einführung, [online abgerufen](http://www.villa-rustica.de/index.php/de/die-villa/eintrittspreise.html).
[^23]: Im Führer durch die Anlage nennt das Römische Freilichtmuseum Hechingen-Stein e.V. auf Seite 9 andere Maße für die Ausdehnung der Anlage und Länge der Mauer: Römisches Freilichtmuseum Hechingen-Stein e.V., Führung durch die Anlage, [online abgerufen](http://www.villa-rustica.de/index.php/de/das-museum/fuehrung-durch-die-anlage.html).
[^24]: Schmidt-Lawrenz: Das Haupt- und Badegebäude der Villa rustica von Hechingen-Stein, Zollernalbkreis (Grabungen 1978-1981), S. 232 f.
[^25]: Warmbad.
[^26]: Heißbad.
[^27]: Kaltbad.
[^28]: Speisezimmer. Der Name leitet sich von den Speiseliegen oder -sofas ─ lat. lectus triclinaris ─ ab. Bei den Römern wurden meist drei dieser Speisesofas hufeisenförmig um einen Tisch ─ lat. Mensa ─ angeordnet. Ein Sofa konnte teilweise von drei Personen gleichzeitig genutzt werden.
[^29]: Sammlung von Steinwerken, hier in einem rekonstruierten Raum ausgestellt.
[^30]: Eigentlich Brennofen.
[^31]: Durchbohrte Baumstämme, die als Rohrleitungen genutzt wurden.
[^32]: Lagerraum.
[^33]: Trocknungsöfen.
[^34]: Antike Form des Bieres.
[^35]: Kleine Tempel. Eigentlich ein Diminutiv des Begriffs aedes, womit ein Haus oder Tempel bezeichnet wurde. Aedicula wird vorwiegend für kleine Tempelbauten verwendet.
[^36]: Keltische Pferdegottheit.
[^37]: Keltisch-germanische Furchtbarkeits- bzw. Unterweltsgöttin.
[^38]: Unter anderem Göttin der Weisheit.
[^39]: Persönlicher Schutzgeist. Ihnen wurde geopfert, wofür man sich von ihnen Hilfe für schwierige Lebenslagen erhoffte. Einige Genien waren allerdings auch bestimmten Berufsgruppen zugeordnet und wurden von diesen verehrt.
[^40]: Schutzgott der Händler.
<!-- Fußnoten Ende -->
