---
title: "Archäologischer Radausflug nach Zellingen und Gössenheim"
aliases:
  - /blog/exkursionsberichte/29-archaeologischer-radausflug-franken-2019

publishDate: 2020-02-28

author:
  - pkuhn

featured: true
weight: 4
featured_headline: Radausflug 2019
featured_img: /berichte/2020/archaeologische-radtour-zellingen/images/radtour-franken-2019-homburg-featured.jpg
featured_img_alt: ""

thumb_img: images/radtour-franken-2019-fahrrad-vor-see.jpg
header_img: /images/events/radtour-1.jpg
#header_img: images/radtour-franken-2019-fahrrad-vor-see.jpg

description: >-
  Archäologische Radtour durch Franken: Im Sattel durch Geschichte, Mittelalter und Archäologie Unterfrankens.
  Lies hier den Bericht zur Exkursion.
tags:
  - Franken
  - Vorgeschichte
  - Mittelalter
  - Neuzeit
  - Exkursion

layout: post
---

Am Samstag, den 6. Juli 2019 fand die jährliche von Studierenden der Universität Würzburg organisierte »Archäologische Radltour« statt, welche dieses Jahr unter der Schirmherrschaft des Arbeitskreis Archäologische Wissenschaften durchgeführt wurde.

<!--more-->

Die Ziele waren Zellingen in Unterfranken sowie die Homburg in Gössenheim, welche zu den größten Burgruinen Deutschlands zählt. Zellingen kann mit einem historischen Altort, mehreren Hügelgräbern im Wald zwischen Zellingen und Leinach, einer barocken Kirche, welche einst als Sommerresidenz des Würzburger Fürstbischof Julius Echter fungierte, sowie den Mauerresten eines Schlosses aus dem 16. Jahrhundert aufwarten. Somit war das erste Ziel der Reise besonders aus historischer Perspektive lohnenswert. Nach einer kleinen Ortsführung und einer kurzen Erfrischung wurde die Fahrt in Richtung Karlstadt fortgeführt. Unterwegs sollten im Großraum Zellingen mehrere archäologische Fundstätten besichtigt werden. Leider konnte der vermutete und in mehreren Quellen erwähnte Grabhügel im Bereich der ICE-Trasse im Zellinger Wald nicht lokalisiert werden. Nach einem kurzen Zwischenstopp an der alten Synagoge Laudenbach und unterhalb der Karlsburg in Mühlbach erreichten wir Karlstadt. Hier wurde die Karlstädter Altstadt mit ihren zahlreichen historischen Fachwerkbauten und ihrer mittelalterlichen Stadtmauer inspiziert.

<!-- Fotogalerie Karlsburg -->
{{< gallery >}}
  {{< figure link="images/radtour-franken-2019-fahrrad-vor-see.jpg" title="Rast bei Erlabrunn" caption="Nach dem Start in Würzburg fand am Erlabrunner See eine kleine Rast statt. Im Hintergrund Felshänge des Unteren Muschelkalks." >}}
	{{< figure link="images/radtour-franken-2019-hang-maintal.jpg" title="Zellinger Umgebung" caption="Die Suche nach archäologischen Bodendenkmälern im Zellinger Umland blieb leider erfolglos." >}}
	{{< figure link="images/radtour-franken-2019-synagoge-laudenbach-gedenktafel.jpg" title="Synagoge Laudenbach" caption="Die ehemalige Synagoge Laudenbach war hingegen schnell gefunden. Die Gedenktafel ist der Zerstörung während der Reichspogromnacht gewidmet." >}}
	{{< figure link="images/radtour-franken-2019-burgruine-karlsburg.jpg" title="Burgruine Karlsburg" caption="Vorbeigeradelt wurde auch an der Burgruine Karlsburg, welche 2017 Ziel der »Archäologischen Radltour« war." >}}
	{{< figure link="images/radtour-franken-2019-main-radweg.jpg" title="Main-Radweg" caption="Auf dem Main-Radweg ging es entspannt Richtung Hauptziel des Tages …" >}}
	{{< figure link="images/radtour-franken-2019-wernmuendung.jpg" title="Wern-Radweg" caption="… nach kurzer Rast an der Mündung der Wern in den Main bei Wernfeld …" >}}
{{< /gallery >}}
<!-- Fotogalerie Karlsburg Ende -->

Nach dieser kurzweiligen Rast führte unsere Reise entlang des Main- und des Werntal-Radwegs nach Gössenheim auf die Burgruine Homburg, welche wir gegen frühen Abend erreichten. Die Kernburg ist der älteste Bestandteil der Ruine und datiert zurück bis in das frühe Mittelalter. 1008 wurde sie von dem Geschlecht derer von Hohenberg errichtet. Zwischen 1028 und 1031 wurde die Burg auf ihre heutige Größe ausgebaut. Die Burg war bis 1680 besiedelt, wurde jedoch durch einen Brand im Hauptbau allmählich dem Verfall preisgegeben und in den 1720er Jahren verlassen. 1780 wurde die Burg durch die fürstbischöfliche Hofkammer aufgeteilt. Dabei erhielt die Gemeinde Karsbach den Schlosshof mit den dazugehörigen Gebäuden, während das Hauptgebäude mit den dort befindlichen Wohnräumen der Gemeinde Gössenheim zugeteilt wurde. Da die Ruine von den anliegenden Ortschaften als Steinbruch verwendet wurde, zerfiel sie allmählich. Heute wird die Ruine vom Homburg- und Denkmalschutzverein betreut, welcher sich zum Ziel gesetzt hat, die Homburg als historisches Denkmal zu erhalten. Außerdem setzt er sich für die Renovierung der Burgruine ein.

<!-- Fotogalerie Homburg -->
{{< gallery >}}
  {{< figure link="images/radtour-franken-2019-homburg-vorburg-turm.jpg" title="Burgruine Homburg" caption="… erreichten wir die Homburg bei Gössenheim.  Blick auf den Turm der Vorburg." >}}
  {{< figure link="images/radtour-franken-2019-spornberg-homburg.jpg" caption="Auf der 150 m über ihrem Umland gelegenen Burgruine empfing uns eine willkommene Brise Wind." >}}
  {{< figure link="images/radtour-franken-2019-homburg-baustruktur-1.jpg" caption="Die von einem Halsgraben umwehrte Burgruine zählt zu den größten Deutschlands. Blick auf eine Baustruktur der Hauptburg." >}}
  {{< figure link="images/radtour-franken-2019-homburg-baustruktur-2.jpg" caption="Mit ihrer erkundenswerten Baustruktur stellt sie den Höhepunkt der diesjährigen Radtour dar. Blick auf eine Baustruktur der Hauptburg." >}}
  {{< figure link="images/radtour-franken-2019-homburg-hauptburg-2.jpg" caption="Die Hauptburg ist durch einen Abschnittsgraben von der Vorburg getrennt." >}}
  {{< figure link="images/radtour-franken-2019-homburg-zwingermauer.jpg" caption="Die Homburg blickt auf eine tausendjährige Geschichte zurück. Fenster in der südlichen Zwingermauer." >}}
  {{< figure link="images/radtour-franken-2019-homburg-hauptburg-palas.jpg" caption="Umliegende Ortschaften verwendeten die Bausubstanz als Steinbruch, weshalb sie allmählich zerfiel. Im Palas der Hauptburg." >}}
  {{< figure link="images/radtour-franken-2019-homburg-hauptburg-1.jpg" caption="Heute steht die Homburg bei Gössenheim unter Denkmalschutz. Blick auf die Reste der Hauptburg." >}}
  {{< figure link="images/radtour-franken-2019-homburg-romanischer-keller.jpg" caption="Wohl ein in spätromanischer Zeit errichteter Keller im Osten der Hauptburg." >}}
  {{< figure link="images/radtour-franken-2019-homburg-vorburg-kapelle-1.jpg" caption="Blick auf die Vorburgkapelle." >}}
  {{< figure link="images/radtour-franken-2019-homburg-vorburg-kapelle-2.jpg" caption="Neben den historischen Aufschlüssen lädt die Homburg auch als ein idyllischer Ort zum Verweilen ein." >}}
{{< /gallery >}}
<!-- Fotogalerie Homburg Ende -->

Nach diesem äußerst lehrreichen Exkurs in die Mediävistik machten wir uns auf den Rückweg. Dabei fuhren wir entlang des Werntal-Radwegs über Eußenheim, Karlstadt und Stetten zurück nach Retzbach. Nach einer Besichtigung von Schützengräben auf dem Retzbacher Benediktusberg, welche auf das Ende des Zweiten Weltkriegs datieren, beendeten wir einen äußerst informativen und sportlichen Tag.

<!-- Fotogalerie Zellingen -->
{{< gallery >}}{{< figure link="images/radtour-franken-2019-bach-radfahrer-steg.jpg" title="Überquerung eines kleinen Bachs" caption="Kein Weg blieb für uns unpassierbar." >}}
  {{< figure link="images/radtour-franken-2019-strecke-46-hinweisschild.jpg" title="Strecke 46" caption="Zurück gen Würzburg geht es ein Stück entlang der unvollendeten Autobahntrasse »Strecke 46« …" >}}
  {{< figure link="images/radtour-franken-2019-radweg-felder.jpg" title="Wern-Radweg" caption="… deren Bau Ende der 1930er Jahre eingestellt wurde; einzelne bauliche Überreste stehen heute unter Denkmalschutz." >}}
  {{< figure link="images/radtour-franken-2019-wern-geologischer-aufschluss.jpg" caption="Vorbei an geologischen Aufschlüssen …" >}}
  {{< figure link="images/radtour-franken-2019-wiese-radweg-wald-hang.jpg" caption="… radelten wir weiter auf dem Wern-Radweg Richtung Retzbach." >}}
  {{< figure link="images/radtour-franken-2019-werntal.jpg" caption="Ein Blick zurück auf 60 gefahrene Kilometer. Es folgen noch zehn." >}}
  {{< figure link="images/radtour-franken-2019-bildstock-pestkreuz.jpg" title="Pestkreuz" caption="Ein um 1600 errichtetes Pestkreuz nahe Stetten, an dem auch ein Prozessionsweg entlangführt." >}}
  {{< figure link="images/radtour-franken-2019-maintal.jpg" title="Blick ins Maintal" caption="Blick auf Retzbach und Zellingen. Fern im Hintergrund ist die ICE-Trasse, die Würzburg mit Fulda verbindet, zu sehen." >}}
  {{< figure link="images/radtour-franken-2019-schuetzengraeben-2-wk.jpg" title="Schützengräben" caption="Der letzte Anstieg des Tages: Schützengräben bei Retzbach aus dem Zweiten Weltkrieg." >}}
  {{< figure link="images/radtour-franken-2019-hang-maintal.jpg" title="Blick auf den Main" caption="Blick mainaufwärts vom Benediktusberg bei Retzbach." >}}
  {{< figure link="images/radtour-franken-2019-bruecke-main.jpg" title="Ende der Radtour" caption="Gegen 21.00 Uhr endete die Radtour in Zellingen." >}}
  {{< figure link="images/karte-akarchwiss-radltour-2019.png" title="Streckenkarte 2019" caption="Datengrundlage: DGM Bayern (LDBV 2019), OSM-Daten (GEOFABRIK 2019, bearbeitet), CLC10 (GeoBasis-DE/BKG 2019); Autor: David Berthel (Juni 2019)." >}}
{{< /gallery >}}
<!-- Fotogalerie Zellingen Ende -->

Wir danken den Organisatoren, ohne die der Ausflug nicht möglich gewesen wäre. Besonderer Dank geht an Wolfram und Anette Kuhn für die Bereitstellung von Snacks und Erfrischungen und an Helma Endress für die Bereitstellung der Ortschronik Zellingen.
In naher Zukunft sind weitere archäologische Radtouren geplant, um der faszinierenden Regionalgeschichte des Großraums Würzburg auf die Spur zu kommen.
