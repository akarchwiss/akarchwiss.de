---
title: "Rundfahrt durch Rottenburg am Neckar – Teil Ⅰ" # Nicht mehr als 70 Zeichen – Google kürzt!
subtitle:
aliases:
  - /blog/exkursionsberichte/40-rundfahrt-durch-rottenburg-am-neckar-teil-i

license: # Optional.

publishDate: 2020-06-27T09:30:00+03:00
lastmod: 2021-03-22T00:00:00+03:00
draft: false
author:
  - mscheck

pdf_version: # Du kannst einen vom Ordnernamen abweichenden PDF-Name hier angeben.

# Vorschaubild für Listenübersicht:
thumb_img: images/rottenburg-antike-wasserbecken.jpg
# Bild für Header:
header_img: images/rottenburg-antike-jupiter-giganten-saeule-6.jpg
header_mobile_img: images/rottenburg-antike-header-mobile.jpg

# Ein hervorgehobener Post wird auf der Startseite gelistet:
featured: false
weight: 1
featured_headline:
featured_img: /images/ # Absolute Pfadangabe (static/).
featured_img_alt:
featured_summary:

hide_header: false # Setze true für wiss. Artikel, wenn diese sehr textlastig sind (nüchternes Erscheinugsbild).

toc: false # Noch nicht implementiert.
autoCollapseToc: true # Noch nicht implementiert.
reward: false
mathjax: false

summary:

description: >-
  Reise mit uns in das römische Rottenburg: Vorbei an der antiken Stadtmauer, über Aquädukt, Inschriften und Reliefs bis zur Jupitergigantensäule.

tags:
  - Antike
  - Römer
  - Baden-Württemberg
  - Exkursion

layout: post
---
Angesichts der diesjährigen Umstände, die Exkursionen in Gruppen – noch dazu zu weit entlegenen Orten – erschweren, wurde eine kleine, lokale Rundfahrt zu einer Auswahl der in Rottenburg am Neckar und naher Umgebung auffindbaren römischen und mittelalterlichen Denkmäler unternommen.

<!--more-->

### Teil Ⅰ: Römisches Rottenburg – Sumelocenna:

Beginnen wollen wir unseren Bericht mit einem kurzen geschichtlichen Überblick:
Lange Zeit ging die Forschung davon aus, dass die römische Siedlung Sumelocenna um 85/90 n. Chr. – in der Regierungszeit des Kaisers Domitian (81 bis 96 n. Chr.) – am Ort der mittelalterlichen und heutigen Stadt Rottenburg am Neckar gegründet wurde. Neuere Erkenntnisse verorten die Gründung dagegen um 110/115 n. Chr. und damit auf die heute angenommene Anlegung des Odenwald-Neckar-Alb-Limes. Damit würde die Gründung Sumelocennas unter die Regentschaft Kaiser Trajans (98 bis 117 n. Chr.) fallen. Es ist davon auszugehen, dass die Anlegung Sumelocennas mit der Grenzsicherung zusammenfällt. Der Verlauf der Fundmünzenhäufigkeit in Rottenburg, der weitgehend mit dem Verlauf an den Limeskastellorten, wie etwa Grinario (Köngen) übereinstimmt, stützt diese Annahme. Bisher steht ein eindeutiger Beweis für die Existenz eines Kastells im Raum um Sumelocenna aus, weshalb hier von einer zivilen Gründung ausgegangen wird. Dies schließt die Gründung gemeinsam mit dem Limes nicht unbedingt aus, da die Siedlung günstig an der Fernstraße zwischen dem Limes bei Grinario und Arae Flaviae (Rottweil) lag und vielleicht einen wichtigen Wegpunkt zwischen Limes und dem Legionslager in Argentorate (Straßburg) darstellte. Wie aus einer 1850 im Norden Rottenburgs aufgefundenen Inschrift hervorgeht, war das Gebiet um die Rottenburger Siedlung ein »Saltus«. Damit war Sumelocenna ein Sondergebiet und stand im direkten Besitz des Kaisers. Es scheint, als sei die gebietsrechtliche Kategorie des Areals um Sumelocenna einmalig im Limesgebiet. Diese – vermeintliche – Tatsache könnte allerdings der aktuellen Überlieferungslage geschuldet sein. Zwei weitere Inschriften – die wir später noch zu sehen kriegen – geben nähere Auskunft über die Stellung Sumelocennas: Sie benennen sie als »Civitas Sumeloceanna«. Eine »Civitas« war eine zivile Verwaltungseinheit eines regionalen Gebietes. Der Hauptort einer solchen Verwaltungseinheit war dabei gleichzeitig namensgebend für die Civitas. Wie groß das verwaltete Gebiet war, lässt sich – derzeit – nicht rekonstruieren; klar ist aufgrund von Inschriften, dass das Kastell in Köngen Teil davon war. Ein Forum, das zu den zentralen Bauten eines Hauptortes zählte, konnte noch nicht für Sumelocenna ausfindig gemacht werden.

Im 3. Jh. n. Chr. erhielt die Siedlung Sumelocenna eine Mauer, deren Bauzeitraum sich noch nicht genauer eingrenzen lässt. Der Optimalfall wäre eine jahrgenaue Datierung – was sich als utopisch erweisen könnte. Der Zeitpunkt des Mauerbaus ist für die Interpretation des Charakters der Anlage entscheidend. So zeichnen die alte und die neuere Deutung ein gegensätzliches Bild zu den Ursachen ihres Baus, was auch Auswirkungen auf die Einordnung der allgemeinen Verhältnisse im Ort und der gesamten Civitas zu diesem Zeitpunkt hat. Die ältere Deutung nahm einen Mauerbau um 200 n. Chr. an und sah sie als Beleg für die wirtschaftliche Blüte der Civitas an. Die Auswertung der bei den Grabungen von 1995 bis 1999 »Am Burggraben« im Nordwesten der heutigen Stadt gewonnenen Funde brachte eine neue These auf: So wäre es möglich, dass der Mauerbau im Zuge eines staatlich-militärischen Schutzprogramms gegen Einfälle von Plünderern im Limesgebiet ab 233 n. Chr. durchgeführt wurde. Möglicherweise durch Kaiser Maximinius Thrax ab 235 n. Chr. initiiert.

An den heute noch sichtbaren Resten der römischen Stadtmauer im Gebiet des römischen Tempelbezirks begann die Rundfahrt. Diese Reste liegen heute zwischen dem Gelände der Justizvollzugsanstalt – dem ehemaligen Hohenberger Schloss – und der Wittenberger Straße. Die Fundamentrollierung der Mauer, die im Grabungsschnitt 2,20 m breit und 0,90 m bis 1,00 m tief war, besteht aus unvermörtelten, dunkelgrauen Kalkbruchsteinen und vereinzelten Sandsteinen mit einer Kantenlänge von etwa 0,25 m. Zwischen den Steinen fand sich dunkelbrauner Lehm. Auf die Rollierung folgt eine 2,00 m bis 2,10 m breite Ausgleichsschicht aus vermörtelten Kalkbruchsteinen. Die 1,75 m breite Mauer wurde in Zweischalentechnik gebaut. Das Mauerinnere setzt sich aus einer regellosen Mischung von Kalkbruchsteinen und Kalkmörtel mit Kiesmagerung zusammen.
Unweit dieses Stücks der Stadtmauer befand sich zur Zeit der römischen Besiedlung ein separat ummauerter Tempelbezirk. Dieser Bereich war mindestens 53 m breit und mindestens 166 m lang. Das südöstliche Ende ließ sich bei der Grabung nicht sicher bestimmen. Innerhalb dieses Bezirks wurden bei Ausgrabungen von 1995 bis 1999 mehrere Baustrukturen beobachtet, welche angesichts ihrer typischen Grundrisse als Kult- und Profanbauten anzusprechen sind. Unter diesen Gebäuden waren zwei nahe hintereinander liegende gallo-römische Umgangstempel, die jeweils eine Größe von 18 m × 18 m aufwiesen. Die Tempel besaßen einen Abstand von gerade einmal 2,10 m. Zudem gab es noch ein weiteres Paar Kultbauten mit einer Seitenlänge von jeweils 5,50 m. Alle vier Gebäude besaßen die gleiche Ausrichtung; ihre Nordwestfronten lagen in einer Flucht. Gegenüber ihnen wurde ein rechteckiges, kleinräumig unterteiltes Wohngebäude gefunden, das eine vorgelagerte Portikus – einen Säulengang oder eine Säulenhalle – besaß. Im Inneren fanden sich bichrome Mosaikböden und bemalte Wände. Es wird davon ausgegangen, dass es sich um das Wohngebäude der im Kultbetrieb aktiven Bewohner der Stadt handelte, vielleicht wurden dort auch Zusammenkünfte von Collegien abgehalten. In der Nordecke der Umfassungsmauer lag zudem ein großer, rechteckiger Hallenbau mit Maßen von 31 m × 15 m. In ihm konnten keine Unterteilung und – abgesehen von einer großen Herdstelle – auch keine Einbauten nachgewiesen werden. Wozu die große Herdstelle diente, ließ sich nicht mehr bestimmen. Es fanden sich weder Anzeichen auf eine Metallverarbeitung noch Funde, die auf die Zubereitung oder Veredelung von Lebensmitteln hindeuten könnten. An der nordöstlichen Umfassung befand sich ein mehrräumiges Gebäude, welches allerdings durch Eingriffe im Mittelalter und in der Moderne stark beschädigt wurde. Der Tempelbezirk entstand wohl nicht vor der Wende zum 3. Jh. n. Chr. Ob dieser Bezirk bereits in vorheriger Zeit für kultische Zwecke genutzt wurde, ließ sich weder stichhaltig beweisen noch endgültig ausschließen. Die jüngeren Grabungen ließen keine Aussagen über die dort verehrten Gottheiten zu. Es mangelte an kultisch zu interpretierenden oder sicher mit einer Kulthandlung in Verbindung zu bringenden Funden. Ergraben wurden ein mit einer Rosette verziertes Pulvinusfragment eines Altars und Fragmente von immerhin 13 Räucherkelchen. Allerdings wurden seit dem 16. Jh. auf den südwestlich angrenzenden Grundstücken wiederholt Architekturteile, Weihinschriften und Skulpturen aufgefunden, die mit hoher Wahrscheinlichkeit dem Tempelbezirk entstammten. Diese Altfunde weisen eine kanonische Zusammensetzung der verehrten Gottheiten auf.

Vom Tempelbezirk ging die Fahrt in das Zentrum des römischen Siedlungsgebietes. Im heutigen Stadtkern konnte eine 32 m lange Toilettenanlage ausgegraben werden, die heute im Römischen Stadtmuseum – im Erdgeschoss des Parkhauses neben dem Martinshof – besichtigt werden kann.
Zur rechten Seite des Museumseingangs kann man heute ein überwölbtes Teilstück des römischen Aquädukts begutachten. Das Aquädukt erstreckte sich über 7,2 km Länge vom südwestlich von Rottenburg gelegenen heutigen Obernau nach Sumelocenna. Gespeist wurde es von einer Quelle im Rommelstal, nördlich von Obernau. Dort wurde ein kurzer Teil der ehemaligen Wasserleitung rekonstruiert. Die Wasserleitung besaß ein Gefälle von 0,33 % und konnte somit wohl 74 l pro Sekunde transportieren. Sie war wohl nicht über ihre gesamte Länge überdacht. Die Wangen der Leitung waren in Zweischalentechnik aus Muschelkalk gefertigt und enthielten an ihren Innenseiten Ziegelstrich. Von einem Sammelbehälter, der in der Antike im Bereich der JVA stand, konnte das Trinkwasser über Deichelleitungssysteme in die Gebäude Sumelocennas geleitet werden. Dies belegen zahlreiche im römischen Stadtgebiet gefundene Deichelbüchsen, mit denen hölzerne Rohre verbunden wurden. Allerdings wird diese direkte Wasserzufuhr höchstwahrscheinlich nur den Wohlhabenden zur Verfügung gestanden haben.

Vor dem Eingang des römischen Museums befindet sich ein kreuzförmiges Wasserbecken aus Stubensandstein mit einem Durchmesser von 9,5 m und einer Tiefe von gerade einmal 0,30 m. Das Becken war mit Ziegelstrich ausgegossen, wodurch es abgedichtet war. Angesichts der geringen Beckentiefe ist eine Funktion als Zierbecken in einem Gartenbereich sehr wahrscheinlich. Ursprünglich befand sich dieses Becken im offenen Innenhof eines Peristylgebäudes, das 1988 bis 1990 teilweise ausgegraben wurde und heute unter dem Parkhaus eingebaut ist.

<!-- Fotogalerie Mauer & Aquädukt -->
{{< gallery >}}

{{<figure link="images/rottenburg-antike-roemische-stadtmauer-1.jpg" title="Reste der römischen Stadtmauer" caption="Diese befinden sich in der Nähe des ehemaligen Tempelbezirks im heutigen Bereich der Stadt »Am Burggraben«.">}}
{{<figure link="images/rottenburg-antike-roemische-stadtmauer-2.jpg" title="" caption="Die Fundamentrollierung besteht aus unvermörtelten, dunkelgrauen Kalkbruchsteinen und vereinzelten Sandsteinen. Darauf folgt eine Ausgleichsschicht aus vermörtelten Kalkbruchsteinen. Die 1,75m breite Mauer wurde in Zweischalentechnik gebaut. ">}}
{{<figure link="images/rottenburg-antike-blick-durchs-lapidarium.jpg" title="Lapidarium vor dem Römermuseum" caption="Blick durch das Lapidarium vom Eingang des Römermuseums aus auf die Jupitergigantensäule.">}}
{{<figure link="images/rottenburg-antike-lapidarium-1.jpg" title="" caption="Überwölbtes Teilstück des Aquädukts, das sich von Sumelocenna aus 7,2 km bis hinter den heutigen Ort Obernau erstreckte.">}}
{{<figure link="images/rottenburg-antike-lapidarium-2.jpg" title="" caption="Blick in das Teilstück.">}}
{{<figure link="images/rottenburg-antike-lapidarium-3.jpg" title="" caption="Kreuzförmigs Wasserbecken eines Zierbrunnen. Dahinter wohl Teilstücke einer antiken Wasserleitung.">}}

{{< /gallery >}}
<!-- Fotogalerie Mauer & Aquädukt Ende -->

Vom Brunnen und Aquäduktteilstück aus begeben wir uns – der Nummerierung der Fundstücke nach rückwärts – durch das Lapidarium vor dem Museum. Es folgen mehrere Steine mit Inschriften. Die Transkriptionen und Übersetzungen derselben entstammen den dazugehörigen Infotafeln. Die Kenntlichmachung der Zeilenumbrüche durch Schrägstriche erfolgt durch Verf. Abkürzungen in den Inschriften, die von den Steinmetzen aus Platzgründen vorgenommen wurden, werden durch runde Klammern aufgelöst statt wie auf den Tafeln in Kleinbuchstaben. In eckigen Klammern stehen Buchstaben, die in der originalen Inschrift verloren gingen. Um die Lesbarkeit des Berichts nicht zu schmälern, werden einige Inschriften übergangen und im Anhang aufgeführt.

Als erstes findet sich eine Weihinschrift auf Schilfsandstein für das Götterpaar Merkur Visucius und Sancta Visucia eines Bezirksrates aus Grinario, die dieser während seiner Amtszeit als Ratsmitglied (ordo decurionum) der Civitas Sumalocennensis stiftete. Die beiden genannten Götter werden als einheimische keltische Gottheiten des Handels und der Fruchtbarkeit angesehen. Die Inschrift wurde 1832 in Köngen gefunden.

<!-- Fotogalerie Inschriften -->
{{< gallery >}}

{{<figure link="images/rottenburg-antike-inschrift-1.jpg" title="Weihestein Merkur Visucius und Sancta Visucia" caption=" Dem Gott Mercurius Visucius und der heiligen Visucia hat Publius Quartonius Secundinus, Ratsmitglied der Gebietskörperschaft Sumelocenna, auf Geheiß sein Gelübde eingelöst gerne aufgrund der (göttlichen) Leistung.">}}
{{<figure link="images/rottenburg-antike-inschrift-2.jpg" title="Bauinschrift" caption=" Zur Ehre des Kaiserhauses haben dem besten und größten Jupiter der rechten Straße (vom Kastell aus betrachtet) die Bürger der Civitas Sumelocenennsis aus dem Dorf Grimario die Umfassungsmauer des Heiligtums von ihrem Geld errichtet. ">}}
{{<figure link="images/rottenburg-antike-inschrift-3.jpg" title="Grabstein 1" caption=" Den Totengöttern, der Julia Severina hat Decimus Julius Severus den Grabstein machen lassen.">}}
{{<figure link="images/rottenburg-antike-inschrift-4.jpg" title="Weihestein für Herecura" caption="Der Göttin Erecura hat Quintus nach einem Gelübde sein Gelübde eingelöst, frei und gerne.">}}

{{< /gallery >}}
<!-- Fotogalerie Inschriften Ende -->

{{< epigraph title="Weihinschrift Köngen:" >}}

{{< transcription >}}
**DEO** **MERCVRIO VI** / **SVCIO E** **SA**(N) **CT**(A)**E VISV** / **CI**(A)**E** **P**(UBLIUS) **QUARTIONIVS** / **SECUNDINVS DECV**(RIO) / **[C]IVI**(TATIS) **SUMAL**(OCENNENSIS) **EX IV**(SSU) **V**(OTUM) **S**(OLVIT) **L**(IBENS) **M**(ERITO)
{{< /transcription >}}

{{< translation >}}
Dem Gott Mercurius Visucius und der heiligen Visucia hat Publius Quartonius Secundinus, Ratsmitglied der Gebietskörperschaft Sumelocenna, auf Geheiß sein Gelübde eingelöst gerne aufgrund der [göttlichen] Leistung.
{{< /translation >}}

{{< /epigraph >}}

Darauf folgt eine Bauinschrift auf Stubensandstein, die ebenfalls in Köngen gefunden wurde. Auf ihr ist festgehalten, dass Bürger der Civitas Sumelocenna, die aus Köngen stammten, Geld für die Umfassungsmauer des Jupiterheiligtums in Köngen stifteten gestiftet hatten. Das Heiligtum befand sich während des 2. und 3. Jhs. n. Chr. an der Straße, die von Grinario nach Sumelocenna führte. Innerhalb des Heiligtums wurden eine Jupitergigantensäule, Altäre und Weihesteine mit Statuen anderer Gottheiten errichtet.

{{< epigraph title="Bauschrift Köngen:" >}}

{{< transcription >}}
**I**(N) **H**(ONOREM) **D**(OMUS) **D**(IVINAE) **I**(OVI) **O**(PTIMO) **M**(AXIMO) / **PLATIAE DE**(XTRAE) **C**(IVES) / **[SV] MELOCENE**(N)**S**(ES) / **VICI GRINAR**(IONIS) / **MACERIAM D**(E) **S**(UO) **P**(OSUERENT)
{{< /transcription >}}

{{< translation >}}
Zur Ehre des Kaiserhauses haben dem besten und größten Jupiter der rechten Straße [vom Kastell aus betrachtet] die Bürger der Civitas Sumelocenennsis aus dem Dorf Grinario die Umfassungsmauer des Heiligtums von ihrem Geld errichtet.
{{< /translation >}}

{{< /epigraph >}}

Unweit dieser Inschriften ist ein in Lettenkeuper gehauenes Relief zweier Männer beim Handschlag zu sehen. Dieser Handschlag wird im Text auf der zugehörigen Tafel als Abschluss eines Vertrags zwischen den Männern gedeutet. Beide stehen allem Anschein nach unter einem Baldachin. Sie besitzen gelocktes Haar und einen gelockten Vollbart.  Der linke Mann hält wohl ein Kästchen in der rechten Hand, der rechte Mann vermutlich eine Schriftrolle in der linken Hand. Letzterer streckt seinen Zeigefinger und seinen kleinen Finger aus und hält die Rolle mit den drei übrigen Fingern. Sie tragen Togen und sind barfuß. Unter dem linken Mann sind noch die Buchstaben **IN** zu erkennen. Aufgrund der Inschrift **CONCORDIA** (Eintracht) – klein im Bogen des Steins zu erahnen – wird eine Vertragseinigkeit angenommen. Das Relief ließe sich auch dahingehend deuten, dass dadurch die personifizierte Eintracht dargestellt werden sollte. Falls sich auf dem Relief reale Personen abbilden ließen, dürfte es sich bei ihnen um reiche Persönlichkeiten gehandelt haben. Belegbar wäre dies allein schon dadurch, dass sie Zeit und Geld für die Anfertigung eines Reliefs besaßen.


<!-- Fotogalerie Weihestein & Grabsteine -->
{{< gallery >}}

{{<figure link="images/rottenburg-antike-lapidarium-4.jpg" title="" caption="Weihestein Merkur Visucius und Sancta Visucia.">}}
{{<figure link="images/rottenburg-antike-lapidarium-5.jpg" title="" caption="Bauinschrift des Jupiterheiligtums in Grinario.">}}
{{<figure link="images/rottenburg-antike-lapidarium-6.jpg" title="" caption="Relief zweier Männer beim Handschlag. Vielleicht die personifizierte Concordia (Eintracht).">}}
{{<figure link="images/rottenburg-antike-lapidarium-7.jpg" title="" caption="Front des ersten Grabsteins.">}}
{{<figure link="images/rottenburg-antike-lapidarium-8.jpg" title="" caption="Rechte Seite des ersten Grabsteins.">}}
{{<figure link="images/rottenburg-antike-lapidarium-9.jpg" title="" caption="Front des zweiten Grabsteins.">}}
{{<figure link="images/rottenburg-antike-lapidarium-10.jpg" title="" caption="Rechte Seite des zweiten Grabsteins.">}}
{{<figure link="images/rottenburg-antike-lapidarium-11.jpg" title="" caption="Linke Seite der beiden ersten Grabsteine.">}}
{{<figure link="images/rottenburg-antike-lapidarium-12.jpg" title="" caption="Front des dritten Grabsteins.">}}

<!-- Fotogalerie Weihestein & Grabsteine Ende -->
{{< /gallery >}}

Die nächsten sechs Steine wurden 1851/52 im Bereich des heutigen Siedlungsgebiets »Lindele« und damit etwa einen Kilometer außerhalb des römischen Stadtgebietes gefunden. Dort lagen die Friedhöfe. Es handelt sich um drei Grabsteine sowie drei Weihesteine aus Stubensandstein. Wer das nötige Geld hatte, ließ sich zu Lebzeiten Grabsteine oder aufwendige Grabbauten anfertigen – wem dies aufgrund des eigenen Todes nicht mehr möglich war, erhielt selbige wahrscheinlich von der eigenen Familie. Die Grabsteine enthielten den Namen, die Herkunft, das Alter, die Berufsbezeichnung oder den Werdegang des verstorbenen Menschen. Im Lapidarium sind drei Grabsteine und drei Altar- und Weihesteine ausgestellt. Von diesen wird jeweils die Inschrift eines Steins exemplarisch im Folgenden aufgeführt. Die übrigen sind im Anhang zu finden:

{{< epigraph title="Inschrift des ersten Grabsteins:" >}}

{{< transcription >}}
**DIS MAN**(IBUS) / **MATRONA** / **CARATVLLI** / **F**(ILIA) **CIVES HEL**(VETIA) / **AN**(NORUM) **XL BALB** / **IVS LIBER MARIT**(US) / **F**(ACIENDUM) **C**(URAVIT)
{{< /transcription >}}

{{< translation >}}
Den Totengöttern Matrona des Caratullus Tochter, helvetische Bürgerin, 40 Jahre alt, Balbius Liber ihr Ehemann hat den Grabstein machen lassen.
{{< /translation >}}

{{< /epigraph >}}

Die im Lindele gefundenen Weihesteine waren der keltischen Göttin Herecura geweiht. Herecura war für die Toten, die Fruchtbarkeit und den Wohlstand zuständig. Auf einigen Reliefs findet sich ihre Darstellung in Form einer Muttergottheit mit gegürtetem Unterkleid und Obergewand und einem Früchtekorb in Händen dargestellt.

{{< epigraph title="Inschrift des ersten Weihesteins:" >}}

{{< transcription >}}
**DE**(A)**E** **ER** / **ECUR**(A)**E** / **QVINT** / **VS EX V** / **OTO V**(OTUM) / [**S**OLVIT] **L**(AETUS) **L**(IBENS)
{{< /transcription >}}

{{< translation >}}
Der Göttin Erecura hat Quintus nach einem Gelübde sein Gelübde eingelöst, frei und gerne.
{{< /translation >}}

{{< /epigraph >}}

Es folgt ein Relief des Gottes Merkur aus Stubensandstein, das 1843 beim Bau der JVA gefunden wurde. Der römische Gott Merkur ist mit dem griechischen Götterboten Hermes gleichzusetzen. Merkur war für den Schutz der Finanzen und materiellen Güter der Händler, Gewerbetreibenden, aber auch von Dieben zuständig. Dargestellt wurde er mit seinen Attributen: Seinem geflügelten Hut, dem Geldbeutel und einem Mantel, den er nur um eine Schulter trägt. Hinzu kommt der geflügelten Stab (Caduceus), um den sich zwei Schlangen winden. Dessen Flügel auf dem Relief allerdings nicht (mehr) zu erkennen sind. Hinter ihm liegt ein Ziegenbock auf dem Boden.

Wir beschließen den Rundgang durch das Lapidarium an der Rekonstruktion einer Jupitergigantensäule. Sie ist die größte der aus Sumelocenna überlieferten Säulen und stellt einen in Südwestdeutschland und einigen Teilen Frankreichs häufig vorkommenden Denkmaltypus dar. Aufgrund von Vergleichen mit anderen, besser erhaltenen Säulen wurde die Rekonstruktion mit 11,5 m Höhe angelegt. Das Denkmal besteht aus einem zweistufigen Unterbau mit profilierter Platte, auf der ein hochkant gestellter Quaderstein – ein sog. Viergötterstein, da er Abbilder von Juno, Apollo, Herkules und Merkur zeigt – ruht. Darauf folgt eine Profilplatte, die als Träger für einen viereckigen Zwischensockel dient, auf dem wiederum Genius, Minerva, Silvan und Diana dargestellt sind. Steht man zunächst vor den viereckigen Steinen und bewegt sich dann linksherum einmal um die Jupitergigantensäule, sieht man vorne Juno und Genius, links Apollo und Minerva, hinten Herkules und Silvan und auf der rechten Seite Merkur und Diana.

Es folgt eine weitere Profilplatte mit drei Säulentrommeln, die von einem viereckigen Kapitell beschlossen werden, das an dieser Säule jedoch nicht ausgearbeitet wurde. An anderen Jupitergigantensäulen beginnt das Kapitell (Abschluss einer Säule) am unteren Ende mit einem Kranz aus Akanthusblättern. An den Ecken zeigen sich Schulterbüsten, die aus den Blättern hervorragen. Diese stellen die vier Jahreszeiten dar und stützen mit ihren Köpfen den Abakus (Deckplatte des Kapitells). Darauf sieht man einen Reiter, dessen Pferd sich über einem Mischwesen aufbäumt und dieses wohl demnächst mit den Vorderhufen niederdrücken wird. Jupiter ist bärtig und hält ein Bündel Blitze in der erhobenen Rechten. Er triumphiert über den Giganten ­– ein Mischwesen, halb Mensch, halb Schlange – indem er über ihn hinwegreitet. Damit triumphiert versinnbildlicht die göttliche Ordnung über das Chaos, das die Giganten mit sich bringen.

Ein weiteres Denkmal der römischen Siedlung findet sich im Waldstück auf der rechten Neckarseite zwischen Mühlweg, Weilerstraße und der Altstadtkapelle im »Gewann Altstadt«. Dort befindet sich der Aussichtspunkt »Römersäule«, von dem man aus auf die »Porta Suevica« blickt. Wie der Name des Aussichtspunktes es schon nahelegt, wurde hier um 1883 ein Säulenpaar aufgestellt, das zu einem heute nicht mehr sichtbaren römischen Gutshof – einer Villa rustica – im »Kreuzerfeld« gehörte. In der näheren Umgebung von Rottenburg wurden insgesamt zehn Villae rusticae nachgewiesen. Drei davon recht nahe beieinander auf der rechten Neckarseite im heutigen Stadtteil Ehingen. Ein römischer Gutshof bei der Klausenkirche, einer »Ob den Ziegelhütten« und einer im »Kreuzerfeld«, wobei der letzte am besten untersucht werden konnte. Letzterer lag recht nahe an der anzunehmenden Straße nach Arae Flaviae. An den Säulen der Villa rustica endet die Rundfahrt durch das römische Rottenburg.

Etwa ab dem Jahr 233 n. Chr. häuften sich die Einfälle kriegerischer alamannischer Zusammenschlüsse in die römischen Gebiete hinter dem Limes. Die römischen Siedler zogen sich zunehmend nach Süden und Westen zurück. Um 260/68 n. Chr. gelang es den einfallenden Gruppen – immer wieder als »die Alamannen« bezeichnet – sich in den Gebieten rechts des Rheines festzusetzen. In dieser Zeit wurde auch Sumelocenna von den meisten Bewohnern verlassen. Die neuen Siedler nutzten die Häuser der Römer nicht selbst, sondern errichteten möglicherweise zunächst im Gebiet »Lindeles« Wohnbauten in Holzbauweise.

<!-- Fotogalerie Jupitergigantensäule -->
{{< gallery >}}

{{<figure link="images/rottenburg-antike-jupiter-giganten-saeule-1.jpg" title="Jupitergigantensäule" caption="Viergötterstein und Zwischensockel, ebenfalls mit drei Gottheiten und einem Schutzgeist. Auf dem Viergötterstein ist Juno dargstellt auf dem Zwischensockel Genius.">}}
{{<figure link="images/rottenburg-antike-jupiter-giganten-saeule-2.jpg" title="" caption="Apollon war auf dem Viergötterstein dargestellt über ihm erahnt man Minerva. Im Hintergrund der Turm des Doms.">}}
{{<figure link="images/rottenburg-antike-jupiter-giganten-saeule-3.jpg" title="" caption="Obgleich der Körper des Gottes beinahe komplett verschwunden ist, kann man Herkules auf dem Viergötterstein erkennen. Das Löwenfell in der Linken, die Keule in der rechten. Über ihm steht Silvan, zu erkennen am Hund, der neben seinen Beinen liegt.">}}
{{<figure link="images/rottenburg-antike-jupiter-giganten-saeule-4.jpg" title="" caption="Auf dem Zwischensockel ist Diana mit einem Jagdhund abgebildet. Erkennbar ist sie an ihrem Köcher, aus dem sie mit ihrer rechten einen Pfeil greift. Eine übliche Darstellungsform der Diana. Aufgrund dessen kann man noch den Bogen in ihrer linken Hand erahnen.">}}
{{<figure link="images/rottenburg-antike-jupiter-giganten-saeule-5.jpg" title="" caption="Jupiter auf seinem Pferd mit einem Blitzbündel in der rechten Hand, triumphiert über den Giganten.">}}
{{<figure link="images/rottenburg-antike-jupiter-giganten-saeule-6.jpg" title="" caption="Noch einmal Jupiter sowie das nicht vollendete Kapitell korinthischer Ordnung.">}}
{{<figure link="images/rottenburg-antike-jupiter-giganten-saeule-7.jpg" title="" caption="Jupitergigantensäule komplett von hinten.">}}
{{<figure link="images/rottenburg-antike-roemische-saeulen.jpg" title="Säulenpaar einer Villa rustica" caption="Dieses Säulenpaar einer ehemaligen Villa rusitca im »Kreuzerfeld« wurde 1883 am Aussichtspunkt auf die Talenge »Porta Suevica« aufgestellt.">}}
{{<figure link="images/rottenburg-antike-denkmaeler-stadtplan.jpg" title="Die Stationen der Rundfahrt" caption="Erstellt auf Grundlage von: Service © openrouteservice.org | Map data © OpenStreetMap contributors | Autor: Michael Scheck (Juni 2020).">}}
{{<figure link="images/rottenburg-antike-plan_sumelocenna.jpg" title="" caption="Bisher bekannte Details von Sumelocenna: Neben dem Verlauf der römischen Stadtmauer ist auf der Karte der Tempelbezirk zu sehen. Des Weiteren sieht man u. a. in der Nähe des Lapidariums folgende im Uhrzeigersinn genannte Gebäude: die öffentliche Latrine, ein Peristylhaus, Bad I und Bad II. Erstellt auf Grundlage von: Leaflet | Created with qgis2leaf by Geolicious & contributors (CC BY-SA 2.0) | Map data © OpenStreetMap contributors | Autor: Michael Scheck (Juni 2020).">}}

{{< /gallery >}}
<!-- Fotogalerie Jupitergigantensäule Ende -->

#### Anhang:

Die *Transkription* und *Übersetzung* der Inschrift, welche die rechtliche Stellung Sumelocennas überliefert, stammen vom Bilderbogen der Homepage des Landesamtes für Denkmalpflege im Regierungspräsidium Stuttgart:

{{< epigraph title="Inschrift, die die rechtliche Stellung Summelocennas überliefert:" >}}

{{< transcription >}}

**IN HONOR**(E)**M** / **DOMUS DIVIN**(AE) / **EX DECRETO ORDINIS** / **SALTUS SUMELOCENNEN** / **SIS CURAM AGENTIB**(US) / **IUL**(IO) **DEXTRO ET G**(AIO) **TURANN**(IO) / **MARCIANO** (VICI?) **MAG**(ISTRIS)

{{< /transcription >}}

{{< translation >}}
Zur Ehre des göttlichen Kaiserhauses auf Beschluss des Bezirksrates des kaiserlichen Dominallandes von Sumelocenna, unter Leitung des Julius Dexter und des Gaius Turranius Marcianus, des Vorstehers des Ortes.
{{< /translation >}}

{{< /epigraph >}}

Die dritte Inschrift im Lapidarium am Römermuseum ist eine Weihinschrift auf Schilfsandstein für den Schutzgeist Genius. Dieser Stein wurde in Marbach am Neckar gefunden. Er folgt im Lapidarium auf die Bauinschrift des Jupiterheiligtums in Köngen. Genius wird als vergöttlichte Persönlichkeit – wie sie in den angeborenen Eigenschaften des einzelnen existiert – ausgelegt. Als Schutzgeist wurde Genius von den Angehörigen verschiedener Zusammenschlüsse von Interessengruppen verehrt. So etwa durch militärische Einheiten, Organisationen, Gemeinden oder auch von ganzen Provinzen. In Siedlungen, die an Flüssen lagen, waren es öfter Zusammenschlüsse von Flößern und Seeleuten die ihren eigenen Genius verehrten. Ein solcher Zusammenschluss wurde »collegium nautarum« genannt. In Sumelocenna gab es ebenfalls solche Zusammenschlüsse, wie sich anhand von – heute leider verschollenen – Inschriften nachweisen ließ. Die im Lapidarium aufgestellte Weihinschrift lautet wie folgt:

{{< epigraph title="Weihinschrift für Grenario:" >}}

{{< transcription >}}
**PRO SAL**(UTE) **IMP**(ERATORIS) / **GEN**(IO) **NAVT**(ARUM) / **G**(AIUS) **IVL**(IUS) **VRBICVS** / **D**(EDIT) **D**(EDICAVIT) **V**(OTUM) **S**(OLVIT) **L**(AETUS) **L**(IBENS) **M**(ERITO)
{{< /transcription >}}

{{< translation >}}
Zum Wohle des Kaisers hat dem Genius der Schiffer und Flößer Gaius Julius Urbicus [den Stein] geschenkt und geweiht. Er hat sein Gelübde eingelöst, froh und gerne aufgrund der [göttlichen] Leistung.
{{< /translation >}}

{{< /epigraph >}}

{{< epigraph title="Inschrift des zweiten Grabsteins:" >}}

{{< transcription >}}
**DIS M**(ANIBUS) / **TESSIAE** / **IVENILI HEL**(VETIAE) / **AN**(NORUM) **XXXVII** / **SILIVS VICTOR** / **HEL**(VETIUS) **CONIV** / **GI ET SIBI** / **F**(ACIENDUM) **C**(URAVIT)
{{< /transcription >}}

{{< translation >}}
Den Totengöttern der Tessia Juvenilis, Helvetierin, 37 Jahre alt, Silius Victor, Helvetier hat seiner Ehefrau und sich den Grabstein machen lassen.
{{< /translation >}}

{{< /epigraph >}}

{{< epigraph title="Inschrift des dritten Grabsteins:" >}}

{{< transcription >}}
**D**(IS) **M**(ANIBUS) / **IULIAE** / **SEVERINAE** / **D**(ECIMUS)  **IVLIVS** / **SEVERVS** / **F**(ACIENDUM) **C**(URAVIT)
{{< /transcription >}}

{{< translation >}}
Den Totengöttern der Julia Severina hat Decimus Julius Severus den Grabstein machen lassen.
{{< /translation >}}

{{< /epigraph >}}

{{< epigraph title="Inschrift des zweiten Weihesteins:" >}}

{{< transcription >}}
**H**(E)**R**(E)**Q**(U)**R**(E) / **RITIFVCTRI** / **V**(O)**TVM S**(OLVIT) **L**(IBENS)
{{< /transcription >}}

{{< translation >}}
Der Herequra hat [? Name unleserlich] sein Gelübde gerne eingelöst.
{{< /translation >}}

{{< /epigraph >}}

{{< epigraph title="Die Inschrift des dritten Steins für Herecura:" >}}

{{< transcription >}}
**OTACILIA** / **MATRONA** / **HERECUR**(A)**E** / **V**(OTUM) **S**(OLVIT) **L**(AETA) **L**(IBENS) / **M**(ERITO)
{{< /transcription >}}

{{< translation >}}
Otacilia Matrona der Herecura hat ihr Gelübde eingelöst, frei und gerne aufgrund der [göttlichen] Leistung.
{{< /translation >}}

{{< /epigraph >}}

Auf den dritten Stein für Herecura folgt ein 1881 an der Straße von Rottenburg nach Seebronn gefundener Weihestein, der dem persischen Sonnen- und Lichtgott Mithras geweiht ist. Anhänger des Mithraskultes waren häufig im Militär vertreten, so war auch der Stifter dieses Steines ein Legionär. Mithras versprach wirksame Schlachtenhilfe, galt als Verteidiger der Wahrheit und Gerechtigkeit, ließ Menschen auferstehen, verlieh den Guten die Unsterblichkeit und vernichtete zudem das Böse. Zeremonien für Mithras waren Männern vorbehalten.

{{< epigraph title="Weihestein für Mithras:" >}}

{{< transcription >}}
**INVICTO** / **MITHRAE** / **P**(UBLIUS) **AEL**(IUS) **VOC-** / **CO MIL**(ES) **L**(EGIONIS) **XXII** / **P**(RIMIGENIAE) **P**(IAE) **F**(IDELIS) / **V**(OTUM) **S**(OLVIT) **L**(AETUS) **L**(IBENS) / **M**(ERITO)
{{< /transcription >}}

{{< translation >}}
Dem unbesiegbaren Mithras hat Publius Aelius Vocco, Soldat der XII. Legion der erstgeschaffenen, frommen getreuen, sein Gelübde eingelöst, froh und gerne aufgrund der [göttlichen] Leistung.
{{< /translation >}}

{{< /epigraph >}}

<!-- Fotogalerie Inschriften Anhang -->
{{< gallery >}}

{{<figure link="images/rottenburg-antike-inschrift-5.jpg" title="Weihestein für Genius" caption=" Zum Wohle des Kaisers hat dem Genius der Schiffer  Gaius Julius Urbicus (den Stein) ge-schenkt und geweiht. Er hat sein Gelübde eingelöst, froh und gerne aufgrund der (göttlichen) Leistung.">}}
{{<figure link="images/rottenburg-antike-inschrift-6.jpg" title="Grabstein 2" caption=" Den Totengöttern, der Tessia Juvenilis, Helvetierin 37 Jahre alt Silius Victor, Helvetier hat seiner Ehefrau und sich den Grabstein machen lassen.">}}
{{<figure link="images/rottenburg-antike-inschrift-7.jpg" title="Grabstein 3" caption="Den Totengöttern Matrona des Caratullus Tochter helvetische Bürgerin 40 Jahre alt Balbius Liber ihr Ehemann hat den Grabstein machen lassen.">}}
{{<figure link="images/rottenburg-antike-inschrift-8.jpg" title="Weihestein Herecura 2" caption=" Der Herequra (? - Name unleserlich) hat sein Gelübde gerne eingelöst.">}}
{{<figure link="images/rottenburg-antike-inschrift-9.jpg" title="Weihestein Herecura 3" caption="Otacilia Matrona der Herecura hat ihr Gelübde eingelöst frei und gerne aufgrund der (göttlichen) Leistung.">}}
{{<figure link="images/rottenburg-antike-inschrift-10.jpg" title="Weihestein Mithras" caption=" Dem unbesiegbaren Mithras hat Publius Aelius Vocco, Soldat der 22. Legion der erstge-schaffenen, frommen getreuen, sein Gelübde eingelöst, froh und gerne aufgrund der (göttlichen) Leistung. ">}}

<!-- Fotogalerie Inschriften Anhang Ende -->
{{< /gallery >}}

<!-- Fotogalerie Weihesteine -->
{{< gallery >}}

{{<figure link="images/rottenburg-antike-lapidarium-13.jpg" title="" caption="Erster Weihestein für Herecura.">}}
{{<figure link="images/rottenburg-antike-lapidarium-14.jpg" title="" caption="Zweiter Weihestein für Herecura. ">}}
{{<figure link="images/rottenburg-antike-lapidarium-15.jpg" title="" caption="Dritter Weihestein für Herecura.">}}
{{<figure link="images/rottenburg-antike-lapidarium-16.jpg" title="" caption="Weihestein für den Gott Mithras.">}}
{{<figure link="images/rottenburg-antike-lapidarium-17.jpg" title="" caption="Relief des Gottes Merkur. Dieser entspricht dem Götterboten Hermes.">}}

<!-- Fotogalerie Weihesteine Ende -->
{{< /gallery >}}

<!-- Literaturangaben -->
{{< bibliography title="Lesenswertes" >}}

{{< bib-item
  author="Gairhos, Sebastian"
  title="Stadtmauer und Tempelbezirk von Sumelocenna. Die Ausgrabungen von 1995 bis 99 in Rottenburg am Neckar, Flur »Am Burggraben«, in: Regierungspräsidium Stuttgart – LAD, Esslingen am Neckar (Hrsg.): Forschungen und Berichte zur Vor- und Frühgeschichte in Baden-Württemberg, Bd. 104, Stuttgart 2008">}}
{{< bib-item
  author="Heiligmann, Karin"
  title="Sumelocenna – Römisches Stadtmuseum Rottenburg am Neckar, in: Landesdenkmalamt Baden-Württemberg/Förderkreis für die ur- und frügeschichtliche Forschung in Baden/Gesellschaft für Vor- und Frühgeschichte in Württemberg und Hohenzollern (Hrsg.): Führer zu archäologischen Denkmälern in Baden-Württemberg, Bd. 18, Stuttgart 1992">}}
  {{< bib-item
    author="Heising, Alexander"
    title="Sumelocenna/Rottenburg am Neckar – Neues und (Alt-)Bekanntes zu einer römischen »Stadt«. Ein Gedächtnisaufschrieb des Vortrags Alexander Heisings durch Peter Ehrmann vom Stadtarchiv Rottenburg, in: Sülchgauer Altertumsverein e. V. Rottenburg am Neckar:  Eigenspuren. Archäologie und Regionalgeschichte um Rottenburg, Bd. 60/61, Jg. 2016/2017, Rottenburg am Neckar 2016, S. 7–8">}}

{{< /bibliography >}}
<!-- Literaturangaben Ende -->
