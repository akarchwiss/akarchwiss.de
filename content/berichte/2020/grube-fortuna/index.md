---
title: "Grube Fortuna" # Nicht mehr als 70 Zeichen – Google kürzt!
subtitle: Mai-Exkursion zur Grube Fortuna 2018
aliases:
  - /blog/exkursionsberichte/32-grube-fortuna

license: # Optional.

publishDate: 2020-05-11T09:30:00+03:00
lastmod: 2021-03-18T00:00:00+03:00
draft: false
author:
  - mscheck

# Vorschaubild für Listenübersicht:
thumb_img: images/bergwerk-grube-fortuna-thumb.jpg
# Bild für Header:
header_img: images/grube-fortuna-header.jpg
header_mobile_img: images/grube-fortuna-header-mobile.jpg

# Ein hervorgehobener Post wird auf der Startseite gelistet:
featured: false
weight: 1
featured_headline:
featured_img: /images/ # Absolute Pfadangabe (static/).
featured_img_alt:
featured_summary:

hide_header: false # Setze true für wiss. Artikel, wenn diese sehr textlastig sind (nüchternes Erscheinugsbild).

toc: false # Noch nicht implementiert.
autoCollapseToc: true # Noch nicht implementiert.
reward: false
mathjax: false

description: >-
  Glück auf! Hier stellen wir dir das Besucherbergwerk »Grube Fortuna« vor.
  Neben historischen Infos erwarten dich Technik und zahlreiche Fotos.

tags:
  - Exkursion
  - Hessen
  - Eisenerz
  - Bergbau

layout: post
---

Am 1. Mai 2018 besichtigte der Verein das stillgelegte Eisenerzbergwerk, die [Grube Fortuna](https://grube-fortuna.de/ "Geowelt Fortuna e. V."), die nördlich von Solms-Oberbiel bei Wetzlar im Lahn-Dill-Gebiet bis Anfang der 1980er Jahre betrieben wurde.

<!--more-->

Die Exkursionsteilnehmer reisten mit der Bahn zum nächstgelegenen Bahnhof in Albshausen an. Vor dem Gang ins Bergwerk informierte sie Vereinsmitglied Herr M. Runzheimer über den Eisenerzabbau im Lahn-Dill-Gebiet. Im Bereich der Grube Fortuna wurde schon in der Antike Eisenerz gewonnen, wie 1881 gemachte Funde römischer Fibeln und Haustierknochen im Grubenfeld »Felicitas« nahelegten. 2003 bestätigte sich diese Annahme durch Ausgrabungen in der Gemarkung Dalheim bei Wetzlar, bei denen unter anderem gut erhaltene Reste eines Rennofens aus dem 6. Jahrhundert n. Chr. entdeckt wurden. Schriftliche Nachweise zum Bergbau in der Grube Fortuna finden sich erst aus der Frühen Neuzeit. 1605 gestattete der Graf Johann Albrecht zu Solms-Braunfels dem Grafen Wilhelm zu Solms-Greifenstein »die Gewinnung von Eisenstein im Klosterwalde bei Altenberg«. Dies bezog sich höchstwahrscheinlich auf die heutigen Grubenfelder »Glückshain« und »Bergmann« der Grube Fortuna. Bergamtsakten aus dem Jahr 1847 stellen wiederum die erste schriftliche Erwähnung des Erzvorkommens dar. 1849 wurde das Grubenfeld der Fortuna schließlich an den Fürsten zu Solms Braunfels verliehen, den mit 13 fördernden Erzbergwerken größten Grubenbesitzer im mittleren Lahngebiet. Er verpachtete den Betrieb noch im gleichen Jahr an Buderus. Im Jahr 1901 kam es zum Abteufen des ersten Schachtes, da die oberflächennahen Erze, die bisher im Tagebau oder durch Stollenbetrieb gewonnen werden konnten, erschöpft waren. Der Schacht reichte 44,8 Meter unter die Stollensohle. Um den Abbau lohnenswert zu halten, musste viel Geld in die maschinelle Ausrüstung und die Anlegung von Schachtanlagen investiert werden, was der Fürst zu Solms-Braunfels und auch Buderus wohl nicht leisten konnten oder wollten. Daher verkaufte der Fürst seinen gesamten Grubenbesitz 1906 an die Firma Krupp in Essen. Diese zahlte ihm sechs Millionen Reichsmark.

Nachdem der geschichtliche Rahmen des Bergbaus im Bereich der Grube Fortuna in groben Zügen erläutert war, galt es, den 7,4 Kilometer langen Weg vom Bahnhof bis zum Bergwerk zurückzulegen, den die Exkursionsteilnehmer zu Fuß bewältigten. Der Weg führte an der alten Drahtseilbahn — die 1908 erbaut wurde — vorbei, mit der Grubenmitarbeiter das Erz zum Bahnhof Albshausen und zur Lahntaleisenbahn transportierten. Mit der Lahntaleisenbahn ging der Weg etwa zum Erzhafen in Oberlahnstein weiter. Dort wurde das Erz auf Schiffe verladen und zur »Hütte Rheinshausen« in Duisburg, die sich im Besitz der Firma Krupp befand, verschifft. Da die Seilbahn querfeldein verläuft, beträgt ihre Länge nur 3,28 Kilometer.

Im heutigen Besucherbergwerk angekommen, unternahm die Gruppe eine Tour unter Tage: Sie führte zu Fuß durch den Stollen zum Schacht und von dort mit dem Förderkorb, der einem offenen Aufzug gleicht, in eine Tiefe von 150 Meter hinunter — quasi die Besuchertiefe. Zu Förderzeiten begaben sich die Bergleute sogar bis in eine Tiefe von 250 Metern. Von der 150-Meter-Sohle fuhren die Exkursionsteilnehmer mit der Grubenbahn durch niedrige Gänge in den Abbaubereich, in welchem der Bergwerksführer diverse Originalmaschinen aus dem Zeitraum von 1900 bis 1972/73 vorführte.  

<!-- Fotogalerie Waldbereich -->
{{< gallery tile=one >}}

{{<figure link="images/bergwerk-grube-fortuna-waldbereich.jpg"
  title="Zum oberen Zechengelände"
  caption="Vom unteren Zechengelände ging es am Seilscheibenhaus vorbei hinauf zum oberen Zechengelände. Hier der Blick in den Wald auf der gegenüberliegenden Hangseite des Seilscheibenhauses, das Bodeneingriffe aufweist.">}}

{{< /gallery >}}
<!-- Fotogalerie Waldbereich Ende -->

Über Tage besichtigte die Gruppe das Zechenhaus mit »Kaue« (in der Neuzeit ein Aufenthalts- und/oder Umkleide- und Duschbereich) sowie das alte und das neue Fördermaschinenhaus auf dem oberen Zechengelände. Unterwegs erfuhren die Teilnehmer weitere Details aus der Geschichte des Bergwerks. Das neue Förderhaus, mit dem noch heute der Förderkorb betrieben wird, wurde 1958 eingerichtet. Ein Fördermaschinist ist für den Betrieb und die Sicherheit beim Transport mit dem Korb verantwortlich. Dazu behält er den Ein- und Ausstieg der Gäste mit einem Monitor im Auge. Angetrieben wird der Förderkorb von einer Trommel-Fördermaschine mit Elektromotor. Die zwei Förderseile haben einen Durchmesser von 27 Millimetern. Am oberen Seil hängt der Förderkorb im Schacht, am unteren ist ein Gegengewicht von drei Tonnen angebracht. Statt eines Förderturms besitzt die Grube Fortuna ein Seilscheibenhaus. Die Höhe des Förderturms wird durch die Steigung vom unteren zum oberen Zechengelände ersetzt — eine in Deutschland einmalige Art der Fördertechnik.  
In unmittelbarer Nähe des neuen Fördermaschinenhauses befindet sich das alte Maschinenhaus. Ein Bau, der durch sein verputztes und gelb gestrichenes Mauerwerk sowie die tragenden Teile, Stürze und Eckpartien, die allesamt ziegelgemauert sind, ins Auge sticht. Reichlich ungewöhnlich für einen Bergwerksbau nimmt das ungeübte Auge den, mit seinen Zinnen einem Burgturm gleichenden, Malakow-Turm auf. Seinen Namen trägt der Turm nach Fort Malakow (je nach Schreibweise auch Malakoff oder Malachow). Dieses Fort war Teil der Wehranlagen von Sewastopol auf der Halbinsel Krim und wurde durch den Krimkrieg von 1853 bis 1856 bekannt. Die Kriegsparteien waren Russland auf der einen Seite und das Osmanische Reich mit seinen Verbündeten Frankreich, Großbritannien sowie Sardinien auf der anderen Seite. Der im Zentrum des Forts stehende steinerne Malakow-Turm wurde während des Krieges lange belagert und schließlich 1855 von französischen Truppen eingenommen. Aufgrund der intensiven Kriegsberichterstattung und den Beschreibungen des Erbauers des Forts, Eduard von Todleben (einem preußischen General in russischen Diensten) wurde der Name Malakow zu einem Synonym für Stärke, Größe und Belastbarkeit. Als Ausdruck dieser Eigenschaften wurden Türme im Bergbau bewusst im Stil von Burgtürmen errichtet und Malakow-Türme genannt. Dadurch schien es gegeben, dass sie die zugeschriebenen Eigenschaften erfüllten und in jedem Fall den in Fördermaschinen waltenden Kräften gewachsen waren.  
Der Malakow-Turm im alten Maschinenhaus der Grube Fortuna fand Anwendung als Wasserspeicher für die Kesselanlage, eines nicht mehr existenten Nachbargebäudes. Das alte Maschinenhaus wurde 1908 erbaut. In seinem Inneren stand bis 1943 — als es zum Einsturz des Maschinenschachtes im oberen Drittel des Grubengeländes kam — eine Dampfmaschine zur Schachtförderung. In der Folge des Schachteinsturzes kam es zu einem Rückgang der Förderung, sodass die Belegschaft auf 50 Mann reduziert wurde. Im Winter 1943/44 begannen die Abteufarbeiten eines neuen Blindschachtes im »Neuen Tiefen Stollen«, der auf dem Niveau der äußeren Talsohle angelegt wurde. Aufgrund des eben erst erlebten Einsturzes wurde der neue Schachtpunkt sorgfältig ausgewählt und mit Beton ausgebaut. Die Besucher, die sich heute zur Tour unter Tage aufmachen, fahren durch diesen Schacht ein.

<!-- Fotogalerie Oberes Zechengelände -->
{{< gallery >}}

{{<figure link="images/bergwerk-grube-fortuna-maschinenhaus-1.jpg"
  caption="Oberes Zechengelände|Am oberen Zechengelände angelangt, präsentiert sich dem Besucher das alte Maschinenhaus aus der Krupp-Ära, 1908 erbaut.">}}

{{<figure link="images/bergwerk-grube-fortuna-maschinenhaus-2.jpg"
  caption="Eckpartien, Stürze und tragende Teile sind ziegelgemauert. Links hinten der mit Zinnen geschmückte Malakow-Turm.">}}

{{<figure link="images/bergwerk-grube-fortuna-foerdertrommel.jpg"
  caption="Trommelfördermaschine, die den Förderkorb antreibt, der heute die Besucher auf die 150-Meter-Sohle hinabbefördert.">}}

{{<figure link="images/bergwerk-grube-fortuna-maschinenhaus-3.jpg"
  caption="Vom Tagebaubereich aus konnte ein Blick auf die Rückseite des alten Maschinenhauses geworfen werden.">}}

{{<figure link="images/bergwerk-grube-fortuna-ruine-ziegenstall.jpg"
  caption="Linker Hand des alten Maschinenhauses steht die Ruine eines alten Ziegenstalls.">}}

{{<figure link="images/bergwerk-grube-fortuna-tagebaugebiet.jpg"
  title="Tagebaugbereich"
  caption="Der Tagebaubereich, in dem mindestens von 1849 bis 1901 Eisenerz gefördert wurde.">}}

{{< /gallery >}}
<!-- Fotogalerie Oberes Zechengelände Ende -->

1953 übernahm die Harz-Lahn-Erzbergbau AG die Grube Fortuna. Dies geschah aufgrund des Gesetzes Nr. 27 der Alliierten Hohen Kommission — der drei Westmächte — mit dem durch Neuordnung des Eisenerzbergbaus die Entflechtung der Montan- und Stahlindustrie erreicht werden sollte. Am Aktienkapital der Harz-Lahn-Erzbergbau AG waren die Firma Krupp mit 50 % und die Unternehmen Hoesch und Klöckner mit je 25 % beteiligt. In den 1960ern kamen harte Zeiten auf die Grube Fortuna zu, da Krupp, Hoesch und Klöckner keine Harz-Lahn-Erze mehr bezogen. Stattdessen griffen sie auf Erze aus dem Ausland zurück. Erze, wie sie in Venezuela und Brasilien vorkamen, waren mit einem Eisengehalt von etwa 65 % höherwertiger und billiger zu erhalten. Der Roteisenstein, der im Lahngebiet vorkommt, enthielt nur bis zu 50 % Eisen und seine kalkige Abart, der Flusseisenstein nur um die 34 %. 1961 wurden die Förder- und Transportkosten von einer Tonne Erz aus Schweden an die Ruhr mit 51 Mark, die Transportkosten einer Tonne Erz aus dem Siegerland hingegen mit 100 Mark veranschlagt. Im Dezember 1962 kam es zur offiziellen Schließung der Grube Fortuna. Ein Schicksal, dass sie mit anderen Gruben in Deutschland teilte. Der Schließung zum Trotz wurde ein Notbetrieb mit 20 Bergleuten aufrechterhalten und bereits 1963 nahm das Bergwerk den Normalbetrieb wieder auf. Das an Siliziumdioxid und Kalk reiche Erz diente nun als »Schlackenträger«, d. h. zur notwendigen Trennung von Schlacke und Roheisen im Hochofen.  
1975 ging die Stammbelegschaft der Grube in Kurzarbeit, nachdem zunehmend Abnehmer für die Fortuna-Erze wegfielen. Im März 1983 erfolgte die endgültige Einstellung des Bergwerkbetriebs, nachdem mit der Stilllegung der Sophienhütte bei Wetzlar — Hessens letztem Hochofenwerk — 1981 ein weiterer wichtiger Abnehmer wegfiel. Durch den fortschreitenden Wegfall von Abnehmern war die Erzhalde im Grundbachtal bis 1983 auf 140.000 Tonnen angewachsen. Insgesamt wurden in 134 Jahren 4,6 Millionen Tonnen Eisenerz gefördert.  
Im Mai 1983 gründete sich der »Förderverein Besucherbergwerk Fortuna e. V.«, um die Grube für Besuchszwecke zu erhalten. Der Förderverein konnte die Unterhaltungskosten auf Dauer nicht alleine stemmen. Nach langen Bemühungen gelang es, eine Trägerschaft, bestehend aus dem Lahn-Dill-Kreis, den umliegenden Städten und Gemeinden sowie dem Förderverein zu organisieren. 1987 öffnete die Grube Fortuna ihre Pforten für den Besucherbetrieb. Vorher mussten u. a. der Schacht über dem »Neuen Tiefen Stollen« für den neuen Personenförderkorb erweitert, die 150-Meter-Sohle freigepumpt und eine Hilfsförderanlage auf der Stollensohle installiert werden.

<!-- Fotogalerie Grubenbahnmuseum -->
{{< gallery >}}

{{<figure link="images/bergwerk-grube-fortuna-aelteste-lok-1912.jpg"
  title="Feld-und Grubenbahnmuseum"
  caption="Nach einer Pause ging es noch ins Feld- und Grubenbahnmuseum. Dort konnte unter anderem die älteste Dampflokomotive der Firma Krauss von 1912 besichtigt werden.">}}

{{<figure link="images/bergwerk-grube-fortuna-lok-1913.jpg"
  caption="Dampflokomotive der Firma Orenstein&Koppel, Baujahr 1913.">}}

{{<figure link="images/bergwerk-grube-fortuna-diesellok.jpg"
  caption="Feldbahn-Diesellok der Firma LKM, Typ V10C. Baujahr 1963">}}

{{<figure link="images/bergwerk-grube-fortuna-bohrstaender.jpg"
  caption="Neben den Lokomotiven gab es im Feld- und Grubenbahnmuseum auch noch einen Bohrständer mit manuellem Antrieb zu begutachten.">}}

{{< /gallery >}}
<!-- Fotogalerie Grubenbahnmuseum Ende -->

Sinkende Besucherzahlen, beginnend mit dem Jahr 2004, und eine verschärfte Haushaltslage stellten den Lahn-Dill-Kreis 2010 vor die Wahl: Die Einstellung des Besucherbetriebs oder die Übertragung an einen privaten Träger, der mit einem halbierten jährlichen Zuschuss des Kreises auskommen muss. Der Förderverein Fortuna gründete daraufhin mit Unterstützung des Lahn-Dill-Kreises, der IHK und dem Land Hessen den gemeinnützigen Verein »Geowelt Fortuna e. V.«, der seit 2011 das Besucherbergwerk betreibt und trägt.

Um das Grubengelände herum führt seit 1987 ein bergbaukundlicher Lehrpfad. Er startet am oberen Zechenhaus, verläuft über das obere Zechengelände, vorbei am neuen und alten Maschinenhaus, zum Tagebaubereich der Grube Fortuna und weiter durch den Bergwerkswald. Dort kommt der Wanderer u. a. am Wetterloch vorbei, das zur Frischluftversorgung der Grube diente und zu den sog. Lochsteinen, die einst die Grubenfelder über Tage markierten. Die Exkursionsteilnehmer begaben sich nicht auf den Lehrpfad, sondern erhielten eine ausführliche Erläuterung von Herrn Tim Schönwetter — der die Gruppe über Tage geführt hatte — zum Tagebaubereich am Rande desselben.

Abschließend erfolgte die Besichtigung des Feld- und Grubenbahnmuseums, in dem 68 Lokomotiven mit Baujahren von 1912 bis 1989 und über 100 Wagen ausgestellt sind, sowie letztlich der Fußmarsch zurück zum Bahnhof, um den Zug gen Heimat zu erreichen. So endete eine lehrreiche und bewegungsintensive Exkursion. Zuletzt sei Herrn Schönwetter noch ein Dank für die umfang- und detailreiche Führung ausgesprochen.

<!-- Literaturangaben -->
{{< bibliography title="Lesenswertes" >}}

{{< bib-item
  author="Geowelt Grube Fortuna e. V."
  title="Historie der Grube Fortuna[Online abrufbar](https://grube-fortuna.de/historie/ \"Historie | Grube Fortuna\")">}}

{{< bib-item
  author="Tim Schönwetter"
  title="Die montanhistorische Kulturlandschaft im südwestlichen Lahn-Dill-Gebiet. Erfassung, Bewertung, Erhaltungsempfehlungen (Masterarbeit 2012). [Zusammenfassung online abrufbar](https://www.uni-bamberg.de/bauforschung/studium/masterarbeiten/die-montanhistorische-kulturlandschaft-im-suedwestlichen-lahn-dill-gebiet-erfassung-bewertung-erhaltungsempfehlungen/ \"Zusammenfassung der Masterarbeit von Tim Schönwetter | Uni Bamberg\")">}}

{{< bib-item
  author="Bergbau Hessen"
  title="Gezähekiste, Heft 17, Ausgabe 01/2016. [Online abrufbar](https://www.bergbau-hessen.de/images/Gezaehekiste/Gezaehekiste_16_1.pdf \"Heft 17, Ausgabe 01/2016 | Gezähekiste\")">}}

{{< bib-item
  author="Bergbau Hessen"
  title="Gezähekiste, Heft 19, Ausgabe 01/2017. [Online abrufbar](https://www.bergbau-hessen.de/images/Gezaehekiste/Gezaehekiste_17_1.pdf \"Heft 19, Ausgabe 01/2017 | Gezähekiste\")">}}

{{< bib-item
  author="Der Spiegel"
  title="Erzgruben – Letzte Schicht, in: DER SPIEGEL 50/1961, vom 6.12.1961, S. 41-42. [Online abrufbar](https://www.spiegel.de/spiegel/print/d-43367722.html \"Letzte Schicht | DER SPIEGEL\")">}}

{{< bib-item
  author="Walter Buschmann"
  title="Rheinische Industriekultur. Malakowtürme. [Online abrufbar](http://www.rheinische-industriekultur.de/objekte/Bergbau/Malakows/malakows.html \"Beschreibung Malakowtürme | Rheinische Industriekultur\")">}}

{{< /bibliography >}}
<!-- Literaturangaben Ende -->
