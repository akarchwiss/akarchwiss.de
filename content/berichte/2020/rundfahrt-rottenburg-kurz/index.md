---
title: "Kurzfassung der Rundfahrt durch Rottenburg am Neckar" # Nicht mehr als 70 Zeichen – Google kürzt!
subtitle: # Optional.
aliases:
  - /blog/exkursionsberichte/38-kurzfassung-der-rundfahrt-durch-rottenburg-am-neckar

license: # Optional.

publishDate: 2020-08-21T09:30:00+04:00
lastmod: 2021-03-27T00:00:00+03:00
draft: false
author:
  - mscheck

pdf_version: # Du kannst einen vom Ordnernamen abweichenden PDF-Name hier angeben.

# Vorschaubild für Listenübersicht:
thumb_img: ../rundfahrt-rottenburg-ii/images/rottenburg-mittelalter-altstadtkapelle-2.jpg
# Bild für Header:
header_img: ../rundfahrt-rottenburg-ii/images/rottenburg-mittelalter-schuetteturm.jpg
header_mobile_img: ../rundfahrt-rottenburg-ii/images/rottenburg-mittelalter-priesterseminar.jpg

# Ein hervorgehobener Post wird auf der Startseite gelistet:
featured: false
weight: 1
featured_headline:
featured_img: /images/ # Absolute Pfadangabe (static/).
featured_img_alt:
featured_summary:

hide_header: false # Setze true für wiss. Artikel, wenn diese sehr textlastig sind (nüchternes Erscheinugsbild).

toc: false # Noch nicht implementiert.
autoCollapseToc: true # Noch nicht implementiert.
reward: false
mathjax: false

summary:

description: >-
  Unternimm mit uns einen kurzen Abstecher in die Vergangenheit von Rottenburg am Neckar!

tags:
  - Römer
  - Mittelalter
  - Antike
  - Exkursion
  - Baden-Württemberg

layout: post
---
Da der Bericht über Rottenburg am Neckar sehr umfangreich geworden ist, haben wir aus beiden Teilen eine Kurzfassung erstellt, um den Einstieg in die Langfassungen zu erleichtern.

<!--more-->

Angesichts der diesjährigen Umstände, die Exkursionen in Gruppen erschweren, wurde eine kleine, lokale Rundtour zu einer Auswahl der in Rottenburg am Neckar und naher Umgebung auffindbaren römischen und mittelalterlichen Denkmäler unternommen. Bei schönem Wetter ließ sich die Tour gut per Fahrrad bestreiten. Begonnen wurde die Fahrt an den einzigen Überresten der römischen Stadtmauer, die hinter der Justizvollzugsanstalt – dem ehemaligen Hohenbergischen Schloss – aufgefunden wurden. Sumeloceanna – das römische Rottenburg – wurde wohl um 110/115 n. Chr., ungefähr zeitgleich mit der Anlegung des Odenwald-Neckar-Alb-Limes, errichtet. Im 3. Jh. erhielt die Stadt eine Mauer. Unweit der begutachteten Mauerreste befand sich in der Antike ein Tempelbezirk, der ebenfalls eine Mauer besaß. Der Bezirk wurde komplett überbaut. Er war mind. 53 m breit und mind. 166 m lang. Die kompletten Ausmaße ließen sich bei Ausgrabungen der jüngeren Vergangenheit leider nicht mehr nachvollziehen.

Vom Tempelbezirk ging die Fahrt weiter in das Zentrum des römischen Siedlungsgebietes. Im heutigen Stadtkern wurde eine 32 m lange Toilettenanlage ausgegraben, die heute im Römischen Stadtmuseum – im Erdgeschoss des Parkhauses neben dem Martinshof – besichtigt werden kann. Zur rechten Seite des Museumseingangs befindet sich ein überwölbtes Teilstück des römischen Aquädukts. Dieses erstreckte sich über 7,2 km Länge vom heutigen Obernau – südwestlich von Rottenburg gelegen – nach Sumelocenna. Gespeist wurde es von einer Quelle im Rommelstal, nördlich von Obernau. Von einem Sammelbehälter, der in der Antike im Bereich der JVA stand, wurde das Trinkwasser über Deichelleitungssysteme in die Gebäude der wohlhabenden Bewohner geleitet. Vor dem Eingang des Museums befindet sich ein kreuzförmiger Zierbrunnen aus Stubensandstein mit 9,5 m Durchmesser und einer Tiefe von 0,30 m. Auf den Brunnen folgt ein Lapidarium, in dem steinerne Inschriften, Relief-, Grab- und Weihesteine sowie die Rekonstruktion der Jupiter-Giganten-Säule aus Sumelocenna zu sehen sind. Durch die Inschriften ist belegt, dass die römische Siedlung eine »Civitas« war. Bei einer Civitas handelte es sich um eine zivile Verwaltungseinheit eines regionalen Gebietes. Der Hauptort dieser Verwaltungseinheit war dabei namensgebend für die Civitas. Wie groß das verwaltete Gebiet war, lässt sich nicht rekonstruieren. Aufgrund von Inschriften, die auch im Lapidarium zu sehen sind, ist bekannt, dass das Kastell in Köngen (römisch: Grinario) Teil davon war.

Ein Relief im Lapidarium zeigt wohl die personifizierte Eintracht (Concordia). Zu sehen sind zwei wohlhabende Männer beim Handschlag unter einem Baldachin. Sie haben wahrscheinlich einen Vertrag oder einen Handel abgeschlossen. Auf dem Baldachin steht Concordia. Beeindruckendstes Stück des Lapidariums ist die Rekonstruktion der Jupiter-Giganten-Säule aus Sumelocenna. Hierbei handelt es sich um eine für den südwestdeutschen und französischen Raum typischen Denkmaltypus. Das 11,5 m hohe Denkmal besteht aus einem zweistufigen Unterbau mit profilierter Platte, auf der ein hochkant gestellter Quaderstein ruht. Dieser zeigt die Abbildung der vier Götter Juno, Minerva, Herkules und Merkur. Darauf folgt eine Profilplatte, der Träger eines viereckigen Zwischensockels. Auf diesem sind wiederum Genius, Apollo, Silvan und Diana dargestellt. Es folgt eine Profilplatte mit drei Säulentrommeln, die von einem korinthischen Kapitell beschlossen werden, das hier nicht ausgearbeitet ist. Auf der Deckplatte des Kapitells befindet sich Jupiter, der mit seinem Pferd einen Giganten niederreitet. Er verkörpert so den Triumph der göttlichen Ordnung über das Chaos, das die Giganten mit sich bringen.

<!-- Fotogalerie Antike -->
{{< gallery >}}

{{<figure link="../rundfahrt-rottenburg-i/images/rottenburg-antike-roemische-stadtmauer-1.jpg" title="" caption="Reste der römischen Stadtmauer.">}}
{{<figure link="../rundfahrt-rottenburg-i/images/rottenburg-antike-lapidarium-1.jpg" title="" caption="Teilstück des römischen Aquädukts.">}}
{{<figure link="../rundfahrt-rottenburg-i/images/rottenburg-antike-lapidarium-3.jpg" title="" caption="Kreuzförmiger Zierbrunnen.">}}
{{<figure link="../rundfahrt-rottenburg-i/images/rottenburg-antike-blick-durchs-lapidarium.jpg" title="" caption="Blick durch das Lapidarium.">}}
{{<figure link="../rundfahrt-rottenburg-i/images/rottenburg-antike-lapidarium-6.jpg" title="Vertragsabschluss" caption="Das Relief zeigt zwei wohlhabende Männer möglicherweise beim Abschluss eines Vertrags.">}}
{{<figure link="../rundfahrt-rottenburg-i/images/rottenburg-antike-jupiter-giganten-saeule-7.jpg" title="Jupiter-Giganten-Säule" caption="Jupiter triumphiert über die Giganten und somit die Ordnung über das Chaos.">}}

{{< /gallery >}}
<!-- Fotogalerie Antike Ende -->

Direkt hinter Fundstücken des Lapidariums kann der Sprung in die Geschichte der spätmittelalterlichen Stadt vollzogen werden. Hinter römischen Weihe- und Grabsteinen befindet sich das Rondell des Büchelesturms. Dabei würde man allerdings etwa 1000 Jahre überspringen. Folglich führte der Weg zunächst zur Sülchenkirche auf dem gleichnamigen Friedhof. Sie ist das letzte Überbleibsel der Siedlung Sülchen. Diese wurde spätestens im 5. Jh. errichtet, nachdem die Römer zur Mitte des 3. Jhs. durch sich häufende Einfälle einheimischer (alemannischer) Personengruppen vertrieben worden waren. Ob die Siedlung Sülchen direkt im Nachgang an die Aufgabe Sumelocennas folgte ließ sich noch nicht belegen. Gesicherte Siedlungsspuren gibt es erst für das 5. Jh. Der frühmittelalterliche Siedlungskern der neuen Siedlung könnte im Bereich der Unterführung der Jahnstraße unter der Umgehungsstraße L 361 gelegen haben. In römischer Zeit war die heutige Jahnstraße Teil der Hauptverbindungsstraße Arae Flaviae – Sumelocenna – Grinario (Rottweil - Rottenburg - Köngen). Diese Fernstraße bot sich somit hervorragend als neuer Siedlungsstandort an, da davon auszugehen ist, dass sie auch nach Abzug der Römer noch stark genutzt wurde. Ihre Bedeutung lässt sich sicherlich daran ermessen, dass sie in der Moderne als Verbindungsstraße nach Wurmlingen fungierte. Der Ort Sülchen scheint einen raschen Aufstieg vollzogen zu haben, offenbar wurde der Sülchgau nach ihr benannt. Die Vorgängerbauten der heutigen – 1450 erbauten – spätgotischen Sülchenkirche reichen bis ins 7. Jh. zurück.

Nachdem Graf Albrecht II. von Hohenberg das – von den Herren von Rotenburg – in den Resten der römischen Siedlung errichtete Dorf Rotenburg 1286 zur Stadt ausgebaut hatte und die Herren von Ehingen auf der rechten Neckarseite eine Stadt errichtet hatten, wurde Sülchen um 1300 aufgegeben und verlassen. Dieses Schicksal teilten die Ortschaften Kalkweil auf der linken Neckarseite und Schadenweiler auf der rechten Neckarseite. Die Gründe hierfür sind nicht geklärt, denkbar wären sich bietende Privilegien der neuen Stadt oder vielleicht auch besondere Anreize durch die Hohenberger. Rotenburg - die Schreibweise des Ortsnamens mit zwei t setzte sich erst im 19.Jh. durch - wurde zum Verwaltungszentrum der Hohenberger, weshalb sich Adelige der nahen und fernen Umgebung in der Stadt niederließen. Darunter etwa im Wernauer Hof, an dem wir wieder in die spätmittelalterliche Stadt zurückkehrten. Von dort ging es durch das Lapidarium zu einer mächtigen mittelalterlichen Wehranlage mit Sandlöchern, Hängebrücke und Notfallrutsche, hinter der ein alter Viereckturm geradeso herauslugt. Bei diesem Vierecktturm handelt es sich um den im 13. Jh. errichteten Gaisholzturm. Zu beiden Seiten des Turms sind kurze Abschnitte der überdachten Wehrmauer erkennbar. Um dem Verlauf der Mauer zu folgen, musste die Reiserstraße überquert werden. Auf der gegenüberliegenden Straße ist die Mauer auf einer Länge von über 120 m erhalten. Dieser Abschnitt hat die Zeit besser überdauert und weist neben dem deutlich tieferen Burggraben noch den Zwingerturm auf: ein Rundturm mit Ziegeldach.

{{< gallery >}}
<!-- Fotogalerie Mittelalter 1 Ende -->

{{<figure link="../rundfahrt-rottenburg-ii/images/rottenburg-mittelalter-buechelesturm.jpg" title="" caption="Büchelesturm.">}}
{{<figure link="../rundfahrt-rottenburg-ii/images/rottenburg-mittelalter-suelchenkirche-1.jpg" title="Sülchenkirche" caption="Der einzig verbliebene Bau der Siedlung Sülchen.">}}
{{<figure link="images/rottenburg-mittelalter-gaisholzturm-1.jpg" title="" caption="Gaisholzturm.">}}
{{<figure link="../rundfahrt-rottenburg-ii/images/rottenburg-mittelalter-zwingerturm-1.jpg" title="" caption="Zwingerturm.">}}
{{<figure link="../rundfahrt-rottenburg-ii/images/rottenburg-mittelalter-altstadtkapelle-2.jpg" title="Altstadtkapelle" caption="Der einzig verbliebene Bau der versuchten Stadtgründung durch die Herren von Ehingen.">}}

{{< /gallery >}}
<!-- Fotogalerie Mittelalter 1 Ende -->

Vom Zwingerturm aus führte der Weg durch die Stadt in das hinter dem Kreuzerfeld gelegene Areal Altstadt. Dort war das Ziel die Altstadtkapelle aus dem 13. Jh. Am Weg zur Kapelle ging es zuvor an einem Stück einer spätmittelalterlichen Mauer vorbei. Die Mauer stand laut zugehörigem Schild am Torweg einer merowingischen Befestigungsanlage aus dem frühen 7. Jh. Die jüngere Mauer war Teil der Befestigungsanlage einer versuchten Stadtgründung der Herren von Ehingen, die jedoch ­vermutlich aufgrund des Einfluss und Investments der Hohenberger für ihre Stadtgründung­ scheiterte. Die auf der rechten Neckarseite gelegene Siedlung Ehingen – der ursprüngliche Sitz der Herren von Ehingen, den sie auf Druck der Hohenberger später aufgeben mussten – wurde zum Stadtteil Rottenburgs. Unweit der Altstadtkapelle befindet sich der Aussichtspunkt Römersäule. Dort wurden im 19.Jh. Teile eines im Kreuzerfeld befindlichen römischen Gutshofs (Villa rustica) aufgestellt. Zurück in der Stadt folgten Abstecher zu weiteren Wehranlagen, etwa dem Kapuziner Tor – das einzige im Stadtteil Ehingen erhaltene Tor – und dem Schütteturm, der während der Hexenverfolgung in der Frühen Neuzeit als Gefängnis genutzt wurde. Beendet wurde die Rundfahrt durch das Mittelalter hinter dem Wernauer Hof und unterhalb des römischen Tempelbezirks, wo ein weiterer gut erhaltener spätmittelalterlicher Turm der Wehranlage sowie ein weiterer Abschnitt der Mauer begutachtet werden konnten.

<!-- Fotogalerie Mittelalter 2 Ende -->
{{< gallery >}}

{{<figure link="../rundfahrt-rottenburg-ii/images/rottenburg-mittelalter-spaetma_mauerrest-1.jpg" title="" caption="Mauerrest im Gewann Altstadt.">}}
{{<figure link="../rundfahrt-rottenburg-i/images/rottenburg-antike-roemische-saeulen.jpg" title="" caption="Römische Säulen.">}}
{{<figure link="../rundfahrt-rottenburg-ii/images/rottenburg-mittelalter-kapuzinertor-1.jpg" title="" caption="Kapuzinertor.">}}
{{<figure link="../rundfahrt-rottenburg-ii/images/rottenburg-mittelalter-schuetteturm.jpg" title="" caption="Schütteturm.">}}
{{<figure link="../rundfahrt-rottenburg-ii/images/rottenburg-mittelalter-turm-wernauer-hof.jpg" title="" caption="Turm hinter dem Wernauer Hof.">}}

{{< /gallery >}}
<!-- Fotogalerie Mittelalter 2 Ende -->
