---
title: "Rundfahrt durch Rottenburg am Neckar – Teil Ⅱ" # Nicht mehr als 70 Zeichen – Google kürzt!
subtitle: # Optional.
aliases:
  - /blog/exkursionsberichte/39-rundfahrt-durch-rottenburg-am-neckar-teil-ii

license: # Optional.

publishDate: 2020-08-17T09:30:00+03:00
lastmod: 2021-03-22T00:00:00+03:00
draft: false
author:
  - mscheck

pdf_version: # Du kannst einen vom Ordnernamen abweichenden PDF-Name hier angeben.

# Vorschaubild für Listenübersicht:
thumb_img: images/rottenburg-mittelalter-stadtmauer-1.jpg
# Bild für Header:
header_img: images/rottenburg-mittelalter-kalkweil-2.jpg

# Ein hervorgehobener Post wird auf der Startseite gelistet:
featured: false
weight: 1
featured_headline:
featured_img: /images/ # Absolute Pfadangabe (static/).
featured_img_alt:
featured_summary:

hide_header: false # Setze true für wiss. Artikel, wenn diese sehr textlastig sind (nüchternes Erscheinugsbild).

toc: false # Noch nicht implementiert.
autoCollapseToc: true # Noch nicht implementiert.
reward: false
mathjax: false

summary:

description: >-
  Begib dich mit uns auf Spurensuche in der mittelalterlichen Stadt Rottenburg
  und erfahre mehr über die historischen Gegebenheiten und Baudenkmäler.

tags:
  - Exkursion
  - Mittelalter
  - Baden-Württemberg

layout: post
---
Nachdem wir uns im ersten Teil mit dem römischen Rottenburg (Sumelocenna) beschäftigt haben, befassen wir uns in diesem Teil mit der mittelalterlichen und in knappen Auszügen auch mit der frühneuzeitlichen Geschichte der Stadt und der nahen Umgebung – genauer der früh- und hochmittelalterlichen Siedlung Sülchen, die auf Sumelocenna folgte. Auch dieses Mal soll die Geschichte anhand von Baudenkmälern nachvollzogen werden.

<!--more-->

### Teil Ⅱ: Die früh- und hochmittelalterliche Siedlung Sülchen und die spätmittelalterliche Stadt Rot(t)enburg:

Ab 233 n. Chr. geriet die Herrschaft der Römer über das Gebiet hinter dem Odenwald-Neckar-Alb-Limes stark ins Wanken. Es häuften sich Einfälle kriegerischer Zusammenschlüsse wohl vorrangig aus Personen der Bevölkerungsgruppe der »Alamannen«.[^1] Um 260/68 gelang es diesen Gruppen, sich endgültig im rechtsrheinischen Raum festzusetzen, woraufhin die Römer dieses Gebiet endgültig aufgaben. Auch die römische Stadt Sumelocenna wurde von seiner Bevölkerung verlassen.

Der erste Abstecher der Radtour führte uns zur nordwestlich der römischen Stadt gelegenen Sülchenkirche auf dem gleichnamigen Friedhof. Um diese Kirche herum befand sich ein Teil der früh- und hochmittelalterlichen Siedlung Sülchen.[^2] Die exakte Lage und Ausdehnung der Siedlung Sülchen konnte noch nicht nachvollzogen werden. Dies könnte durch die vollständige Auswertung der seit 1982 sukzessive gewonnenen Grabungserkenntnisse geändert werden. Bislang wurden nur ca. 10 % des kompletten Siedlungsareals ausgegraben, weshalb fraglich ist, ob sich je ein komplettes Bild der Ausdehnung Sülchens ergibt[^3] Der frühmittelalterliche Siedlungskern befand sich möglicherweise an der Unterführung der Jahnstraße unter der Umgehungsstraße L 361. In römischer Zeit war die heutige Jahnstraße Teil der Hauptverbindungsstraße Arae Flaviae – Sumelocenna – Grinario[^4]. Das Dorf Sülchen soll durch die »alamannischen« Siedler in direkter Nachfolge auf die Vertreibung der Römer angelegt worden sein. Nachweisen ließ sich die Siedlung bisher frühestens für das 5. Jh. Streufunde aus dem späten 3. und 4. Jh. fielen zu gering aus und belegen nur die Anwesenheit von Personen in diesem Gebiet.

Die neue Siedlung könnte einen raschen Aufstieg zu überregionaler Bedeutung vollzogen haben. Scheinbar ließen sich Handwerker aus dem Donauraum dort nieder und brachten ihre Techniken und Waren mit.[^5] Bei diesen Waren handelte es sich um feintonige Becher und Schalen, teilweise mit Ritzungen in Form von Gitter- und Zackenmustern, wie sie auch für den mittleren Donauraum nachgewiesen werden konnten. Denkbar ist, dass etwa die Keramik derselben Warenart, wie sie sich innerhalb der befestigten alamannischen Höhensiedlung auf dem Runden Berg bei Urach fand, in Sülchen hergestellt und gen Urach verkauft wurde. In der Frühzeit des Dorfes gelangte der Hauptanteil der Gebrauchskeramik wohl durch Handel nach Sülchen, eine Keramikproduktion vor Ort ließ sich erst ab der zweiten Hälfte des 5. Jhs. nachweisen. Sollte ein derart weiträumiger Handel bestanden haben, dürfte sich ein Teil der Bevölkerung den Lebensunterhalt allein durch diesen finanziert haben. Noch eine weitere Tatsache spricht für einen Bedeutungsanstieg: Die weitere Umgebung um die Siedlung erhielt den Namen Sülchgau und dürfte damit seinen Namen nach der Siedlung erhalten haben. Spätestens in der Zeit der Karolinger erhielt das Dorf Verwaltungsaufgaben für den Sülchgau hinzu. Mit Handwerk, Handel, Marktfunktion und Verwaltungsaufgaben verfügte die Siedlung über frühstädtische Strukturen.

Im späten 6. Jh. war das Gebiet um die Sülchenkirche aufgesiedelt, was sich durch Keramikfunde der sog. Donzdorfer Ware von bemerkenswertem Umfang belegen ließ; die lockere Besiedlung verdichtete sich. Die Siedlung Sülchen übertraf in ihrer Ausdehnungsfläche die spätmittelalterliche Stadt Rottenburg um ein Mehrfaches. Allerdings lag dies etwa darin begründet, dass die Bebauung in Sülchen locker und ohne begrenzende Stadtmauer angelegt worden war. In der spätmittelalterlichen Stadt wurde auf eine mehrgeschossige Bauweise zurückgegriffen, durch die sich mehr Einwohner auf einer kleineren Fläche ansiedeln ließen. Die Ausdehnung Sülchens erstreckte sich über einen langen Zeitraum, den sowohl starker Wachstum, als auch Schwankungen und Siedlungsverlagerungen kennzeichneten. Innerhalb der Siedlung wurden früh genutzte Flächen aufgegeben und erst Jahrhunderte später in Zeiten des größten Siedlungswachstums vom 11. bis zum 13. Jh. wieder besiedelt.

Der Name Sülchen fand seinen Weg – im Vergleich zum Alter des Ortes – erst spät in schriftliche Quellen.[^6] Die Forschung nennt zwei Werke als mögliche Erstnennung, diese sind verschieden alt und führen unterschiedliche Erwähnungen. Bei dem einen Werk handelt es sich um die Aufzeichnung alamannischer Orte in der »Cosmographia« des sog. Geographen von Ravenna. Der Geograph verfasste es zwischen dem Ende des 7. und der Mitte des 9. Jhs. Darin listete er – laut Schmidt – auch eine Ortschaft mit Namen »Solich« auf. Hierbei könnte es sich theoretisch um Sülchen handeln – eine Auslegung, die nicht jeder teilt. Wahrscheinlich begründet sich die Ablehnung dadurch, dass der Name mit seiner Schreibweise mit O zu weit von Sülchen und den angenommenen Ursprungswörtern – alles Begriffe mit u – siehe die These unten zur Herleitung des Ortsnamens – entfernt ist. Quarthal schreibt, dass in der Cosmographia – deren Quellenwert und die zeitliche Stellung ihrer Vorlagen umstritten sind – in der Rubrik *patria Suavorum que et Alamannorum patria* der Name *Solist* aufgelistet ist. Nun ergibt sich bereits das Problem, dass verschiedene Autoren unterschiedliche Wörter angeben, die der Geograph von Ravenna als Name der Siedlung Sülchen aufgeführt haben könnte. Joseph Schnetz, welcher sich mit den rechtsrheinischen Orten der Cosmographia auseinandersetzte, hält diesen Begriff für die Benennung des Zollern bei Hechingen, auf dem heute die Burg Hohenzollern zu finden ist. Er ist der Ansicht, dass das *Silist* des Geographen von Ravenna eigentlich *Solici* heißen müsste und deutete es als verlesene Form von *Solire,* das wiederum mit *Solicinium* identisch sei. *Solire* wiederum war für ihn eine Frühform von Zollern. Lehnt man die Nennung durch die Cosmographia ab, wäre der erste gesicherte schriftliche Hinweis auf Sülchen eine indirekte Erwähnung durch eine Schenkungsurkunde König Arnulfs von Kärnten aus dem Jahr 888. Diese Urkunde nennt den Sülchgau, in dem sich das Dorf Sülchen befand und für den es wohl auch namensgebend war. Eine weitere, jüngere Quelle erwähnt ebenfalls diesen Gau. In der um 900 geschriebenen Vita des 861 ermordeten heiligen Meinrad heißt es, dass selbiger im Sülchgau geboren worden sei, der seit alters her seinen Namen nach dem Dorf Sülchen trage. Auch bezüglich des Siedlungsnamens bestehen mehrere Deutungsansätze. Nach einer jüngeren These könnte sich der Name von germanischen Sprachwurzeln hergeleitet haben. Ausgehend vom altdeutschen Wort »Sulicha« (Nominativ) / »Sulichen« (Dativ) wäre eine Verbindung zum germanischen Wort »Sulika« bzw. »Sullika« denkbar, dass sich mit »das Sumpfige/beim Sumpfigen« übersetzen lässt.[^7] Hieraus entwickelte sich schließlich über die Jahrhunderte der Name Sülchen. Für diese Deutung spricht, dass einerseits das angenommene Siedlungsgebiet Sülchens und die unmittelbare Umgebung noch heute stellenweise sumpfig sind und aufgrund von Grabungsbefunden vermutet wird, dass dort Altarme des Neckars verliefen. Andererseits ist durch eine Schenkungsurkunde aus dem 12. Jh. eine Mühle in der Siedlung belegt. Zwischen 1126 und 1146 schenkte der Speyer Bischof Sigefridus diese Mühle dem Kloster Hirsau. Daher kann man davon ausgehen, dass im Mittelalter ein stärkerer Wasserlauf durch den Ort ging.

Es scheint, als habe Sülchen im Siedlungsgefüge des Neckarraumes eine Sonderstellung eingenommen. Dafür sprechen die Ausdehnung des Ortes, die – aufgrund des Fundspektrums – anzunehmenden weiträumigen Handelsbeziehungen, sowie die ansässigen qualifizierten Handwerker, deren Anwesenheit sich durch die Funde ihrer Produkte ergibt. Bereits zu Beginn des 11. Jhs. setzte allerdings die Auflösung des Sülchgaus und damit wohl auch der Bedeutungsverlust des Ortes Sülchen ein. 1007 wurde der Ort Kirchheim aus dem Sülchgau – das heutige Kirchentellinsfurt – durch eine Schenkung Heinrichs an das Bistum Bamberg übertragen. Im Jahr 1057 schenkte Heinrich IV. der bischöflichen Kirche zu Speyer das Gut Sülchen. Die entsprechende Schenkungsurkunde stellt gleichzeitig den letzten urkundlichen Beleg des Namens Sülchgau dar. Ab dieser Zeit wurde er als Neckargau bezeichnet. Danach finden sich noch einige schriftliche Erwähnungen des Ortes Sülchen, der im Jahr 1286 letztmals in einem Schriftstück Erwähnung fand.

Das Ende der Siedlung Sülchen besiegelten letztlich auf der einen Seite Graf Albrecht II. von Hohenberg durch den Ausbau des Dorfes Rotenburg zur Stadt Rotenburg an der linken Neckarseite um 1280. Das Dorf Rotenburg war durch die edelfreien[^8] Herren von Rotenburg zwischen der Mitte des 11. und des Beginns des 12. Jhs. in den Ruinen des römischen Sumelocenna errichtet worden. Als Kern des Dorfes wird eine Wasserburg am linken Neckarufer – unweit der Südwestecke der römischen Stadtmauer bzw. des ehemaligen Karmeliterklosters und heutigen Priesterseminars – angenommen. Auf der anderen Seite waren die Herren von Ehingen mit ihrer 1292 erstmals erwähnten »neuen Stadt« Ehingen und ihrer versuchten zweiten – letztlich jedoch gescheiterten – Stadtgründung im Areal Altstadt, beide auf der rechten Neckarseite, verantwortlich.

<!-- Fotogalerie Sülchen & Wasserburg -->
{{< gallery >}}

{{<figure link="images/rottenburg-mittelalter-suelchen.jpg" title="Die Lage der ehemaligen Siedlung Sülchen" caption="Nordwestlich des Siedlungsgebietes Sumelocennas und an der alten Hauptverbindungsstraße der Römer von Ariae Flaviae über Sumelocenna nach Köngen befand sich die Siedlung Sülchen. Erstellt auf Grundlage von: Leaflet, Created with qgis2leaf by Geolicious & contributors.">}}
{{<figure link="images/rottenburg-mittelalter-suelchenkirche-1.jpg" title="Die Sülchenkirche" caption="Die spätgotische Friedhofskirche, die 1450 umgebaut wurde, ist das letzte sichtbare Überbleibsel der Siedlung Sülchen. Die Siedlung wurde um 13 allmählich aufgegeben.">}}
{{<figure link="images/rottenburg-mittelalter-suelchenkirche-2.jpg" title="" caption="Die Kirche besitzt drei Vorgängerbauten. Der erste – ein Rechteckbau – wurde im 7. Jh. angelegt.">}}
{{<figure link="images/rottenburg-mittelalter-priesterseminar.jpg" title="Wasserburg des Dorfs Rotenburg" caption="Links des Priesterseminars, bei den dunkleren Bäumen, wird der Standort der Wasserburg des Dorfes Rotenburg angenommen.">}}
{{<figure link="images/rottenburg-mittelalter-wernauer-hof.jpg" title="Die Stadt Rotenburg" caption="Mit dem Aufstieg Rotenburgs zur Stadt zog der Adel der Umgebung in Stadthäuser, wie den Wernauer Hof.">}}

{{< /gallery >}}
<!-- Fotogalerie Sülchen & Wasserburg Ende -->

Die Hohenberger gingen aus der schwäbischen Linie des Hauses Hohenzollern hervor, welches seinerseits seine Ursprünge auf der Burg Hohenzollern bei Hechingen, 26 km südlich von Rottenburg hatte. Entscheidend bei der Entstehung der neuen Linie waren die Söhne Burchard und Friedrich des Grafen Burchard von Zollern, der 1150 zum letzten Mal in Quellen greifbar ist. Sein gleichnamiger Sohn benannte sich ab 1179 sowohl nach der Burg Hohenberg, [^8] als auch nach der Burg Zollern benannte.[^10] Sein Bruder Friedrich seinerseits wird ab 1186 als Graf von Hohenberg bezeichnet. Wohl um 1170 erwarben die Grafen von Hohenberg im Raum der späteren Stadt Rotenburg eine Vielzahl an Gütern und Rechten und lösten so die vormaligen Herren von Rotenburg ab. Burchard trat das Erbe des Grafen Wetzel von Haigerloch an und übertrug an die Zollern verliehene Reichsrechte an die neue Hohenberger Linie. Erkennbar wird dies durch das von Burchard als Graf von Hohenberg weitergeführte zollerische Reitersiegel. Während die Hohenberger Grafen im 13. Jh. alle Urkunden und Dokumente mit dem Reitersiegel besiegelten, verwendeten die Grafen von Zollern zunächst ein Siegel mit einem Löwen und später eines mit dem schwarz-weiß gevierten Schild.[^11] Die Herauslösung der Hohenburger Grafen aus dem Hause der Zollern ging jedoch nicht schlagartig vonstatten. Noch 1207 wurden Mitglieder des Hauses auch als Grafen von Zollern bezeichnet. Mit der Verlagerung des Schwerpunktes der Regierung und Verwaltung von der Burg Hohenberg zunächst nach Haigerloch und später nach Rottenburg vollzog sich die endgültige Abspaltung vom Hause Zollern. Die Grafen von Hohenberg verlegten im Laufe des 13. Jhs. ihren Regierungssitz auf die unweit von Rottenburg gelegenen »Weiler Burg«.[^12] Diese war ursprünglich der Sitz der Herren von Rotenburg. Für rund 100 Jahre verblieb die Weiler Burg als der Hohenbergische Regierungssitz. 1300 oder einige Jahre früher erwarben die Hohenberger das Patronatsrecht für die Pfarrei Ehingen vom Stift in Kreuzlingen und verschmolzen in der Folge im ersten Drittel des 14. Jhs. die beiden Nachbarstädte Rottenburg und Ehingen zu einer Stadt. Symbol der neuen Einheit wurde die Obere Brücke, die schriftlich seit 1317 nachweisbar ist. Vorher nutzten die Bürger beider Städte eine Furt zwischen Morizplatz und Badgasse zur Überquerung des Neckars.

Die Stadt Rotenburg wurde durch Graf Albrecht II. zum Verwaltungsmittelpunkt der Grafschaft Hohenberg erhoben. Dementsprechend befanden sich dort zahlreiche Stadthäuser verschiedener Adelsgeschlechter der näheren und weiteren Umgebung. Darunter etwa der Wernauer Hof,[^13] der älteste Adelssitz der Stadt. Aus diesen Adelsgeschlechtern stammten die führenden Personen der herrschaftlichen Verwaltung. Die zahlreichen Privilegien und Perspektiven, die der Aufstieg zum Verwaltungsmittelpunkt mit sich brachte, machte Rotenburg wohl zum Anziehungspunkt zahlreicher Bewohner der naheliegenden Ortschaften Kalkweil und Sülchen auf der linken Neckarseite. Für die Bewohner des auf der rechten Neckarseite gelegenen Schadenweilers hatten Ehingen und die Stadtgründung im Areal Altstadt wahrscheinlich einen vergleichbaren Effekt.

<!-- Fotogalerie Kalkweil -->
{{< gallery >}}

{{<figure link="images/rottenburg-mittelalter-kalkweil-1.jpg" title="Kalkweil" caption="Die Dorfkapelle Kalkweils. Das letzte Gebäude des im Hochmittelalter abgegangenen Dorfes Kalkweil.">}}
{{<figure link="images/rottenburg-mittelalter-kalkweil-2.jpg" title="" caption="Das Haus Kalkweil befindet sich auf dem ehemaligen Siedlungsgebiet Kalkweils, allerdings wurde es etwa 100 Jahre nach der Aufgabe der Siedlung errichtet.">}}
{{<figure link="images/rottenburg-mittelalter-schadenweiler-1.jpg" title="Schadenweiler " caption="Schadenweiler Hof von 1570. Adelssitz der Herren von Themar.">}}
{{<figure link="images/rottenburg-mittelalter-schadenweiler-2.jpg" title="" caption="Die Herren von Themar verloren ihr Geld im Dreißigjährigen Krieg (1618 bis 1648) und verkauften den Adelssitz schließlich 1675 an die Stadt Rotenburg.">}}

{{< /gallery >}}
<!-- Fotogalerie Kalkweil Ende -->

Denkbar wäre auch, dass die Begründer der neuen Städte, den Bürgern der Ortschaften in unmittelbarer Umgebung gewisse »Anreize« darboten, um den eigenen Städten rasch zu einer angemessenen Anzahl an Einwohnern zu verhelfen. Gerade die Bürger von Kalkweil dürften hierbei von besonderem Interesse gewesen sein, übertraf doch die Steuerkraft des Dorfes die der Ortschaften Dettingen, Obernau, Seebronn und Weiler um ein Drittel – wie aus einer hohenbergischen Steuerliste von 1385 hervorgeht. Potente Bürger in Rotenburg zu haben, erhöhte sicherlich auch das Prestige der Stadt. Eine weitere These sieht die Abwanderung eines Teils der Bürgerschaft von Kalkweil durch die Brandschatzung der Ortschaft im Zuge der Fehde, die Markgraf Bernhard von Baden und Graf Rudolf III. von Hohenberg austrugen, begründet.[^14]

Wie auch immer die tatsächlichen Gründe für die Abwanderung der Einwohner der genannten Ortschaften aussahen – die drei Dörfer wurden im ausgehenden 13. und frühen 14. Jh. endgültig verlassen und fielen wüst. Die Städter nutzten allerdings die landwirtschaftlichen Flächen, die um diese Ortschaften lagen, weiterhin, da die Landwirtschaft noch immer einen gewichtigen Anteil des Erwerbes ausmachte.

Im Zuge mittelalterlicher Stadtgründungen kam es häufig zur Aufgabe umliegender Ortschaften. Oft ist die frühere Existenz eines Dorfes in der Gegenwart nur noch durch Flur- oder Wegbezeichnungen[^15] überliefert. Hierdurch ist eine genaue Verortung der Lage und Ausdehnung der früheren Siedlung kaum möglich. Manchmal bleibt ein einzelnes Bauwerk zurück. Häufig sind es die Dorfkirche bzw. die Dorfkapelle,[^16] die zurückbleiben und eine exaktere Lokalisierung der ehemaligen Siedlung ermöglichen.[^17]  Im vormaligen Siedlungsgebiet des Dorfs Schadenweiler wurde 1570 der heute noch existierende Schadenweiler Hof errichtet. Er war Adelssitz der Herren von Themar. Die Themar verarmten im Verlauf des Dreißigjährigen Krieges. Ab 1631 verkauften sie ihren Besitz nach und nach, 1675 schließlich auch ihren Adelssitz, der in den Besitz der Stadt Rotenburg überging. Seit 1979 ist er Teil der Hochschule für Forstwirtschaft. Bereits um 1500 errichtete eine Familie der Rottenburger Oberschicht an dieser Stelle ein Haus.

Die Sülchenkirche und ihr umliegender Friedhof selbst stellen einen erheblichen Glücksfall dar, da sich dort zahlreiche Funde der früheren Siedler fanden und ein Belegungszeitraum des Friedhofes über etwa 1500 Jahre nachweisen ließ. Die heute noch stehende spätgotische Saalkirche wurde um 1450 auf einem romanischen Vorgängerbau neu errichtet. Bemerkenswert am Neubau ist, dass er lange nach der Aufgabe der Siedlung um 1300 vollzogen wurde. Die Vorgängerbauten der heutigen Kirche reichen bis ins 7. Jh. zurück. In dieser Zeit wurde eine kleine, rechteckige Steinkirche errichtet. Etwa zum Anfang des 8. Jhs. erhielt sie einen eingezogenen Rechteckchor als Anbau. Zwischen dem späten 8. und dem frühen 11. Jh. wurde ein vorromanischer Neubau errichtet. Es handelte sich um eine dreischiffige, querschifflose Pfeilerbasilika mit einem Dreiapsidenchor. Zwischen 1160 und 1180 wurde die vorromanische Kirche durch einen romanischen Neubau – dabei ist noch nicht gesichert, ob es sich um einen vollständigen Neubau handelte oder nur Teile neu errichtet wurden – ersetzt. Die Erbauer der spätgotischen Kirche rissen die jüngere romanische Kirche fast vollständig ab. Daher lässt sich heute kaum noch sagen, ob für den romanischen Bau nur die Fundamente oder auch das aufgehende Mauerwerk seines Vorgängerbaus Verwendung fand. Gesichert ist, dass die vorherige Bauform übernommen wurde. Den spätgotischen Bau errichteten seine Erbauer aus Spolien[^18] in Form von Werksteinen seines romanischen Vorgängers und in Teilen aus römischen Handquadern. Letztere fanden schon im vorromanischen Bau Anwendung.

Fortgesetzt wurde die Rundfahrt im Lapidarium vor dem römischen Museum. Hinter den antiken Weihesteinen für die Göttin Herequra befinden sich Reste der Stadtmauer Rotenburgs. Dort wurde das Rondell des ehemaligen Büchelesturms begutachtet. Die mittelalterliche Stadt erhielt eine Doppelmauer mit – zumindest in Teilen – überdachtem Wehrgang an der inneren Mauer. 22 Türme waren in die Mauer eingebaut, dazu sieben Tore, von denen zwei Doppeltore waren. Heute sind noch vier Türme und zwei Tore – eines mit Torturm – erhalten. Die Errichtung der Mauer wurde im 13. Jh. begonnen und im Laufe des 14. Jhs. abgeschlossen. Verlässt man das Lapidarium des Römermuseums und entfernt sich von der rekonstruierten Jupiter-Giganten-Säule, kommt man bald zu einem Viereckturm. Dabei handelt es sich um den im 13. Jh. errichteten Gaisholzturm. Der Turm fügt sich in die überdachten Wehrgänge der mittelalterlichen Stadtmauer – die zu seinen beiden Seiten verlaufen – ein. Seinen Namen trägt der Turm aufgrund eines früheren Bewohners. Die Stadtmauer und der dazugehörige Graben setzen sich an dieser Stelle – unterbrochen durch die Reiserstraße – noch etwa 120 m weit in Richtung Neckar fort. Auf der gegenüberliegenden Seite der Straße ist der Stadtgraben deutlich tiefer und die Mauer in einem längeren Abschnitt erhalten, als im Bereich des Lapidariums. Von einem Parkplatz an der Reisestraße aus konnte ein Blick durch den Graben geworfen werden. Die Mauer verschwand unter hohen Büschen und ausladenden Bäumen. Am Ende des Burggrabens fiel der mächtige, mit Schießscharten ausgestattete und von einem hohen, kegelförmigen Ziegeldach gekrönte Rundturm ins Auge. Hierbei handelt es sich um den ehemaligen Zwingerturm. Er stammt aus dem 14. Jh.

<!-- Fotogalerie Stadtgraben -->
{{< gallery >}}

{{<figure link="images/rottenburg-mittelalter-buechelesturm.jpg" title="Im Stadtgraben" caption="Das römische Lapidarium befindet sich im ehemaligen mittelalterlichen Stadtgraben. Somit kann man stellenweise römische und mittelalterliche Funde gemeinsam betrachten.">}}
{{<figure link="images/rottenburg-mittelalter-gaisholzturm-2.jpg" title="Gaisholzturm" caption="Viereckturm aus dem 13. Jahrhundert mitsamt überdachter Wehrmauer. Benannt ist der Turm nach einem seiner Bewohner.">}}
{{<figure link="images/rottenburg-mittelalter-stadtmauer-1.jpg" title="Stadtmauer" caption="Blick entlang der überwachsenen Stadtmauer in Richtung Zwinger.">}}
{{<figure link="images/rottenburg-mittelalter-stadtmauer-2.jpg" title="" caption="Die Stadtmauer im Winter, wenn der Bewuchs geringer ausfällt.">}}
{{<figure link="images/rottenburg-mittelalter-zwingerturm-1.jpg" title="Zwingerturm" caption="Zwischen all dem Grün wirkt der Rundturm wie ein Fremdkörper …">}}
{{<figure link="images/rottenburg-mittelalter-zwingerturm-2.jpg" title="" caption="… nähert man sich ihm jedoch, erblickt man einen mächtigen Rundturm mit Schießscharten und Ziegeldach.">}}
{{<figure link="images/rottenburg-mittelalter-spaetma_mauerrest-1.jpg" title="" caption="Spätmittelalterlicher Mauerrest, der laut Infotafel am Torweg einer merowingischen Befestigungsanlage aus dem frühen 7. Jh. n. Chr. stand. 1985 renoviert.">}}
{{<figure link="images/rottenburg-mittelalter-spaetma_mauerrest-2.jpg" title="" caption="Auf der Rückseite des Mauerrestes ist eine Inschrift von 1602 angebracht. Laut Infotafel ist sie jedoch historisch falsch.">}}
{{<figure link="images/rottenburg-mittelalter-spaetma_mauerrest-3.jpg" title="" caption="Die Steintafel auf der Rückseite der Mauer.">}}

{{< /gallery >}}
<!-- Fotogalerie Stadtgraben Ende -->

Vom Zwingerturm ging es über den Neckar nach Ehingen und hinauf zum am Rand des Kreuzerfelds gelegenen Areal Altstadt. Wer sich mit dem Rad dorthin begibt, umfährt das eigentliche Gebiet zunächst in einem weitausholenden Bogen. Dabei führt der Weg zu einem Teilstück einer spätmittelalterlichen Mauer vorbei. Dieses Mauerstück war sicherlich Teil der Ummauerung der hier geplanten Stadtgründung. Errichtet wurde es laut zugehörigem Schild am Torweg einer merowingischen Befestigungsanlage aus dem frühen 7. Jh. Das Mauerstück erhielt bei der Renovierung 1985 einem Rauverputz. Auf der Rückseite der spätmittelalterlichen Mauer wurde 1602 eine Inschrift angebracht, die – laut der Informationstafel von 1985 – historisch fehlerhaft ist.

Die Altstadt dürfte eine Anlage des 12. und 13. Jhs. gewesen sein. Aufgrund belegter Verbindungen zum Hause Ehingen[^19] wird die Altstadt als im Anfangsstadium gescheiterter Versuch einer Stadtgründung als Gegengewicht zu Rotenburg gedeutet. Die Herren von Ehingen waren Ministerialen der Schutzvögte des Kreuzlinger Klosters und wurden durch das Eindringen der mächtigen Hohenberger in der Umgebung um Rotenburg in Bedrängnis gebracht. So verlegten sie auf Druck der Hohenberger ihre Burg, die wahrscheinlich an der Ehinger Remigiuskirche lag,[^20]ins Katzenbachtal hinter Niedernau. Die neue Burg wurde 1407 zerstört. 1279 traten sie in die Dienste des Hauses Hohenberg und gaben die Stadtgründung auf. Heute zeugen nur noch das beschriebene Mauerstück sowie die Altstadtkapelle und ihr Wirtschaftsbau vom beinahe 180 × 300 m messenden Areal und dem Versuch der Stadtgründung. Errichtet wurde das Gotteshaus im 13. Jh. als Marienkapelle im spätromanischen Stil. Ihren Hochaltar weihte 1268 der Hl. Albertus Magnus. 1688 wurde der Bau barock umgestaltet und erweitert. Die Kapelle erhielt einen hölzernen Glockenturm mit Ziegeldach. 1983/85 erfolgten Renovierungsarbeiten. Die Kapelle liegt oberhalb des Aussichtspunkts Römersäule, welcher im ersten Teil den letzten Besichtigungspunkt der römischen Denkmäler darstellte.

Von der Altstadtkapelle begaben wir uns, dem weitläufigen Bogen folgend und an der Kreuzerfeld Realschule vorbei – hinter der man einen Blick über die Stadt werfen kann –, wieder hinunter in das Siedlungsgebiet der spätmittelalterlichen Stadt. Das nächste Ziel war ein weiterer Abschnitt der mittelalterlichen Stadtmauer. Dort ging es zum Pulverturm zwischen dem Bahnübergang an der Niedernauer Straße und dem Ehinger Platz. In diesem Turm aus dem 14. Jh. war im Spätmittelalter bzw. in der Frühen Neuzeit das Pulvermagazin der Stadt untergebracht. Der Turm steht im Stadtteil Ehingen. Nächste Station war das Kapuziner Tor. Zu diesem ging es durch die Kapuzinergasse in Richtung Neckar. Auch dieses Tor wurde im 14. Jh. erbaut und erhielt einige Jahre später einen Fachwerkoberbau. Hierbei handelt es sich um das einzige im Stadtteil Ehingen erhaltene Stadttor. Wer die Stadt im Mittelalter durch dieses Tor verließ und dem Neckar folgte, kam etwa nach Bad Niedernau, einem Ort, der 1127 erstmalig durch Urkunden Erwähnung fand.

<!-- Fotogalerie Altstadtkapelle -->
{{< gallery >}}

{{<figure link="images/rottenburg-mittelalter-altstadtkapelle-1.jpg" title="Altstadtkapelle und ihr Wirtschaftsbau" caption="Romanische Kapelle aus dem 13. Jh. Ihr Hochaltar wurde 1268 geweiht. 1688 wurde sie barock umgestaltet und erweitert.">}}
{{<figure link="images/rottenburg-mittelalter-altstadtkapelle-2.jpg" title="" caption="Die Kapelle befindet sich an der Stelle der versuchten Stadtgründung der Herren von Ehingen.">}}
{{<figure link="images/rottenburg-mittelalter-abgegangene-doerfer.jpg" title="Die Siedlungen um Rottenburg" caption="Lage der Siedlungen, die im Spätmittelalter wüst fielen bzw. die aufgegebene Stadtgründung im Areal Altstadt. Erstellt auf Grundlage von: Service © openrouteservice.org ">}}
{{<figure link="images/rottenburg-mittelalter-blick-ueber-rottenburg.jpg" title="Blick über die Stadt" caption="Blick auf Rottenburg vom Kreuzerfeld aus. Links der Torturm des Kalkweiler Tores, dahinter der Schütteturm und das alte Schloss (heute JVA). Rechts der 1491 fertiggestellte Turm des Doms St. Martin.">}}
{{<figure link="images/rottenburg-mittelalter-pulverturm-1.jpg" title="Pulvermagazin" caption="In diesem Turm aus dem 14. Jahrhundert war im Spätmittelalter bzw. in der Frühen Neuzeit das Pulvermagazin der Stadt untergebracht.">}}
{{<figure link="images/rottenburg-mittelalter-pulverturm-2.jpg" title="" caption="Teil der Stadtmauer neben dem Pulverturm.">}}

{{< /gallery >}}
<!-- Fotogalerie Altstadtkapelle Ende -->

Vom Kapuziner Tor aus begaben wir uns in die Königsstraße, überquerten den Neckar über die Obere Brücke und folgten der Königsstraße ein Stück weit bis zur ersten Abzweigung nach links (hinter dem Kreisverkehr, der auf die Brücke folgt). Hier geht der Staig von der Königsstraße ab. Von der Königsstraße aus fiel der Blick direkt auf den Schütteturm – einem weiteren Wehrbau des 14. Jhs., der an das ehemalige Hohenbergische Schloss – heute die Justizvollzugsanstalt – angrenzt. Während der Hexenverfolgung der Frühen Neuzeit, die in der Grafschaft Hohenberg um 1530 begann, diente der Schütteturm als Gefängnis für beschuldigte Personen. Ende des 16. Jhs. kam es während der sog. Kleinen Eiszeit zu einer klimatischen Abkühlung, durch welche die örtlichen Winzern Einkommenseinbußen hinnehmen mussten. Dementsprechend kam die Mehrzahl der Anklagen durch den Vorwurf der »hexerischen« Einflussnahme auf das Wetter zustande. Der Weinanbau stellte bis ins 17. Jh. eine wichtige Einnahmequelle der Stadt dar. Auf dem Höhepunkt wurde er in 90 Orten, darunter Wien und Innsbruck, verkauft. Folglich war ein großflächiger Ausfall nicht nur für die Winzer verheerend. Die Hexenprozesse erreichten ihren Höhepunkt um die Jahre 1596 bis 1601. 1671 wurde die letzte Exekution einer verurteilten Person in Rotenburg durchgeführt. Der Turm besaß bis ins 19. Jh. ein Obergeschoss, das nach einem Blitzeinschlag im Jahr 1836 abgetragen wurde. Wir folgten dem Staig nach links und erblickten den rötlich-gelben Torturm des Kalkweiler Tors. Dieser wurde – wer hätte es gedacht? – ebenfalls im 14. Jh. erbaut und erhielt 1770/80 im Durchgang ein sog. Kreuztragunsfresko. Heute sieht man allerdings eine Kopie desselben von 1975. Die Rundfahrt wurde rückwärtig zum Wernauer Hof und unterhalb des römischen Tempelbezirks beendet. Dort konnte ein gut erhaltener Turm und ein weiterer erhaltener Abschnitt der teilweise noch immer überdachten Stadtmauer begutachtet werden.

Enden wollen wir mit dem Abstieg des Hauses Hohenberg. Dieses setzte wohl noch zu Lebzeiten Alberts II. von Hohenberg – der als bedeutendste Persönlichkeit dieser Familie gilt – ein. Nachdem eine Linienteilung den Startpunkt der Hohenberger bedeutete, leitete wohl eine solche ihren Niedergang ein. 1281 trennten die Hohenberger ihre an Zollern angrenzenden Besitzungen von den zumeist neu erworbenen links des Neckars. Weitere Erbteilungen sorgten anscheinend für einen enormen Anstieg der Schuldenlast. Sie wurde für den letzten der Hohenberger aus der Rotenburger Hauptlinie zu groß, sodass er die Grafschaft 1381 für 60.000 Goldgulden an Herzog Leopold III. von Österreich, Steiermark, Kärnten und Krain aus dem Hause Habsburg verkaufte.[^21] 1486 erlosch die Linie der Hohenberger Grafen endgültig. Rotenburg blieb bis zum Frieden von Pressburg 1806 in Folge der Schlacht von Austerlitz im Besitz Österreichs. Danach wurden die vorderösterreichischen Gebiete Württemberg und Baden zugeschlagen.

<!-- Fotogalerie Kapuziner Tor -->
{{< gallery >}}

{{<figure link="images/rottenburg-mittelalter-kapuzinertor-1.jpg" title="Kapuziner Tor " caption="Das einzige im Stadtteil Ehingen – bis ins 13. Jh. ein selbstständiger Ort – erhaltenene Stadttor. Durchquert man das Tor und folgt dem Neckar, kommt man etwa nach Bad Niedernau.">}}
{{<figure link="images/rottenburg-mittelalter-kapuzinertor-2.jpg" title="" caption="Rückseite des Kapuzinertores.">}}
{{<figure link="images/rottenburg-mittelalter-schuetteturm.jpg" title="Schütteturm" caption="Der Schütteturm am ehemaligen Hohenberger Schloss, heute die JVA. Nahm während der Hexenverfolgung im 16. Jh. die unrühmliche Rolle als Gefängnis für die der Hexerei beschuldigten Personen ein.">}}
{{<figure link="images/rottenburg-mittelalter-kalkweiler_tor-1.jpg" title="Kalkweiler Tor" caption="Der gelblich/beige bis organgene Torturm des Kalkweiler Tores, welcher aus dem 14. Jh. stammt.">}}
{{<figure link="images/rottenburg-mittelalter-kalkweiler_tor-2.jpg" title="" caption="Die 1975 angefertigte Kopie des Kreuztragungsfreskos von 1770/80.">}}
{{<figure link="images/rottenburg-mittelalter-kalkweiler_tor-3.jpg" title="" caption="Das Tor von der Kalkweiler-Steige aus betrachtet.">}}
{{<figure link="images/rottenburg-mittelalter-turm-wernauer-hof.jpg" title="" caption="Weiterer erhaltener Teil der spätmittelalterlichen Stadtbefestigung.">}}
{{<figure link="images/rottenburg-mittelalter-plan-rotenburg.jpg" title="Plan Rotenburgs" caption="Besichtigte Baudenkmäler der spätmittelalterlichen Stadt Rotenburg. Erstellt auf Grundlage der in der Reiserstraße in Rottenburg am Neckar aufgestellten Schautafel. Bearbeitet durch Michael Scheck (Juli 2020).">}}

{{< /gallery >}}
<!-- Fotogalerie Kapuziner Tor Ende -->

<!-- Literaturangaben -->
{{< bibliography title="Lesenswertes" >}}

{{< bib-item
  author="Manz, Dieter"
  title="Rottenburger Stadtgeschichte. Von den Anfängen bis zum Jahr 2000, Stadt Rottenburg (Hrsg.), Rottenburg am Neckar 2002">}}
{{< bib-item
  author="Schmidt, Beate"
  title="Die archäologische Ausgrabung in der Sülchenkirche. Neue Erkenntnisse – neue Fragen, in: Aderbauer, Herbert/Kiebler, Harald (Hrsg.): Die Sülchenkirche bei Rottenburg. Frühmittelalterliche Kirche – alte Pfarrkirche – Friedhofskirche – bischöfliche Grablege, Lindenberg i. A. 2018, S. 14–53">}}
  {{< bib-item
    author="Schmidt, Erhard"
    title="Das Dorf Sülchen – Zentrum des Sülchgau. Bemerkungen zu den archäologischen Untersuchungen, in: Aderbauer/Kiebler (Hrsg.): Die Sülchenkirche bei Rottenburg, Lindenberg i. A. 2018, S. 184–213">}}
    {{< bib-item
      author="Quarthal, Franz"
      title="Politische und kirchliche Verhältnisse in Sülchen bis zum Ende des Mittelalters in historischer Perspektive, in: Arderbauer/Kiebler (Hrsg.): Die Sülchenkirche bei Rottenburg, Lindenberg i. A. 2018, S. 214–232">}}

{{< /bibliography >}}
<!-- Literaturangaben Ende -->

<!-- Fußnoten -->
[^1]: Die einfallenden werden gerne verallgemeinernd als »Alamannen« bezeichnet. Man kann davon ausgehen, dass nicht ausschließlich alamannische Gruppen an den Plünderungen teilnahmen bzw. diese Gruppen nicht nur aus Alamannen bestanden. Denkbar sind Einfälle aller möglicher Gruppen, denen ein solches Vorgehen lukrativ erschien und auch gemischte Gruppen unter alamannischer Führung. Da die aktuelle Forschung bereits für eine Schlacht vor etwa 3250 Jahren davon ausgeht, dass Kämpfer aus dem Nordosten Deutschlands sowie gar aus Böhmen und den östlichen Mittelgebirgen zu einer Schlacht in Norddeutschland zusammenfanden, ist auch hier nicht ausschließlich von Kämpfern von einer einzigen Bevölkerungsgruppe auszugehen.
[^2]: Die Darstellung der Geschichte Sülchens erfolgt nach den Aufsätzen: Schmid, Beate: Die archäologische Ausgrabung in der Sülchenkirche. Neue Erkenntnisse – neue Fragen und Schmidt, Erhard: Das Dorf Sülchen – Zentrum des Sülchgau. Bemerkungen zu den archäologischen Untersuchungen, beide in: Aderbauer, Herbert /Harald Kiebler (Hrsg.): Die Sülchenkirche bei Rottenburg. Frühmittelalterliche Kirche – alte Pfarrkirche – Friedhofskirche – bischöfliche Grablege, Lindenberg i. A. 2018, S. 14 – 53 und S. 184-213.
[^3]: Teile lassen sich wahrscheinlich aufgrund moderner Überbauung auch zukünftig nicht mehr untersuchen. Es ist zudem anzunehmen, dass potentielle Funde durch die modernen Bauten zerstört wurden.
[^4]: Rottweil - Rottenburg - Köngen.
<<<<<<< HEAD
[^5]: Erhard Schmidt schreibt in seinem hier zitierten Aufsatz auf Seite 205, dass inzwischen unzweifelhaft belegt sei, dass nicht die Ware, sondern die Handwerker »importiert« worden seien. Leider benennt er nicht, wodurch genau dieser unzweifelhafte Beleg erfolgte.
[^6]: Dieser Umstand könnte jedoch einer spärlichen Quellenüberlieferung geschuldet sein.
[^7]: These von Lutz Reichart, nachvollzogen über: Quarthal, Franz: Politische und kirchliche Verhältnisse in Sülchen bis zum Ende des Mittelalters in historischer Perspektive, in: Arderbauer / Kiebler (Hrsg.): Die Sülchenkirche, Lindenberg i. Allgäu 2018, S. 214-232, S. 219.
[^8]: Adelige aus einem landrechtlichen Stand, die ihren Adel keinem Dienst- oder Lehensverhältnis zu verdanken hatten. Sie unterstanden nur dem König bzw. dem Kaiser. Ihre Territorien galten ab dem 11. Jh. als reichsunmittelbar, was ebenfalls bedeutete, dass diese nur dem König oder Kaiser unterstanden.
[^9]: Eine abgegangene Burg bei Schörzingen, nahe Schönbergs.
[^10]: Erst ungefähr ab dem 14. Jh. nannten die Grafen von Zollern sich und ihren Stammsitz Hohenzollern. Der Zusatz »Hohen« wird verschiedentlich als Kenntlichmachung der eigenen Standeserhöhung bewertet.
[^11]: Welchen sowohl die schwäbische, als auch die fränkisch-brandenburgische und heute preußische Linie der Hohenzollern im Wappen führt.
[^12]: Die Bezeichnung als Weiler Burg setzte sich im Volksmund durch. Historisch handelt es sich dabei um die Rotenburg.
[^13]: ursprüngl. der Sitz des Amann v. Wendelsheim, seit dem 14. Jh. der Herren v. Ehingen, 16./17. Jh. der Herren v. Wernau und bis nach 1735 Sitz des Freiherren Schenk v. Stauffenberg.
[^14]: Manz, Dieter: Rottenburger Stadtgeschichte. Von den Anfängen bis zum Jahr 2000, Stadt Rottenburg (Hrsg.), Rottenburg am Neckar 2002, S. 31.
[^15]: Im Falle Kalkweils findet sich gleich beides.
[^16]: Gotische Kapelle des 14. Jhs., die dem Hl. Georg geweiht war. Der Bau wurde nach einem Brand 1644 erneuert.
[^17]: Unweit der Georgs-Kapelle von Kalkweil steht bis heute auch ein Schafhaus, das heutige Haus Kalkweil, welches wohl im Laufe des 16. Jhs. errichtet wurde. Durch seine späte Errichtungszeit kann es nicht als Beleg der Existenz des Dorfes gewertet werden, da dieses bereits um 1410 aufgegeben wurde.
[^18]: Wiederverwendete Teile älterer Bauten, Inschriften, Skulpturen u. ä.
[^19]: Wie etwa der Taufe Ritter Rudolf I. von Ehingen 1378 in der Altstadtkapelle.
[^20]: Die Remigiuskirche befand sich an der Stelle, an der sich heute die Klausenkapelle des gleichnamigen Friedhofs befindet.
[^21]: Zwischen den Hohenbergern und Habsburgern bestand eine gewisse Verbindung: Gertrud von Hohenberg, die Schwester Alberts II. wurde um 1245/47 in Rottenburg mit Rudolf von Habsburg verheiratet. 1273 wurde Rudolf zum deutschen König gekürt, seine Frau Getrud nannte sich von nun an Anna von Habsburg. Sie gilt als Stammmutter der Habsburger Dynastie.
<!-- Fußnoten Ende -->
