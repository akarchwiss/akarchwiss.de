---
title: "Öffentliche Sicherheit im klassischen Athen"
aliases:
  - /texte/wissenschaftliche-artikel/41-oeffentl-sicherheit-im-klass-athen

publishDate: 2020-12-28T00:00:00+03:00
lastmod: 2021-03-01T00:00:00+03:00
author:
  - mneckenich

pdf_version: neckenich_oeffentl-sicherheit-im-klass-athen.pdf

hide_header: true

toc: false # Noch nicht implementiert.
autoCollapseToc: true # Noch nicht implementiert.
reward: false
mathjax: false

summary: >-
  Dieser Text befasst sich mit der Frage nach der Existenz der Institution Polizei im antiken Athen.
  Da es dort keine Polizei im heutigen Sinne gab, behandelt der Text die Einrichtungen der Gesetzgebung
  und der Strafverfolgung im klassischen Athen, anhand der gegenwärtigen Erkenntnisse der Forschungsliteratur.

description: >-
  Erfahre mehr über staatliche Institutionen der inneren und öffentlichen Sicherheit im klassischen Athen in diesem wissenschaftlichen Artikel.

tags:
  - Antike
  - Klassische Zeit
  - Griechenland
  - Staatswesen

layout: post 
---

Dieser Text befasst sich mit der Frage nach der Existenz der Institution Polizei im antiken Athen. Da es dort keine Polizei im heutigen Sinne gab, behandelt der Text die Einrichtungen der Gesetzgebung und der Strafverfolgung im klassischen Athen, anhand der gegenwärtigen Erkenntnisse der Forschungsliteratur. Zwar fehlten feste Institutionen und langjährige Amtsinhaber, aber es gab mehrere Beamte und Magistrate, die für verschiedene Bereiche des öffentlichen Lebens zuständig waren, jedoch übernahmen sie keinerlei Strafverfolgung.[^1]

## Staatliche Institutionen innerer Sicherheit

»In der Frühen Nz. bezeichnete Polizei bzw. ›gute Policey (g.Pol.)‹ — wohl abgeleitet von griech. politeia, der Ordnung des antiken Stadtstaates — die ›gute Ordnung‹ eines Gemeinwesens […]«[^2]

Im klassischen Athen gab es keine Polizei[^3] und keine Staatsanwaltschaft.[^4] Dennoch existierten staatliche Instanzen, die mit Aufgaben der öffentlichen Aufsicht und Kontrolle betraut waren: »On the other hand, ancient city-states recognized the need for publicly appointed officials to carry out functions of social regulation.«[^5] Die Astynomen und Agoranomen werden auch bei Blanton und Fargher genannt.[^6] Diese Beamten bildeten jeweils ein Gremium von zehn Bürgern, die ihre Zuständigkeitsbereiche,  den Piräus und die Stadt Athen, immer zu fünft betreuten.[^7] Die Astynomen sorgten, neben der Instandhaltung und Sauberkeit der Straßen, für die, wie Bleicken es formuliert, »Einhaltung der baupolizeilichen Vorschriften«.[^8] Pflicht der Agoranomen war es, die Ordnung auf dem Markt aufrechtzuerhalten, Gewichte und Maße zu überwachen und die Marktabgaben einzuziehen.[^9] Ergänzt werden kann diese Aufzählung um die Magistrate, die sich um alle (Organisations-) Probleme zu kümmern hatten, die mit der Getreideversorgung der Stadt im Zusammenhang standen. Gegen Ende des 4. Jh. v. Chr. wuchs  die Anzahl der sitophýlakes (Getreideaufseher) auf 35 an.[^10] Außerdem ist der Aufseher der Wasserversorgung zu nennen; einer der wenigen Magistrate, die gewählt wurden.[^11]

Evident ist jedoch, dass diese Magistrate nur im allerweitesten Sinne etwas mit den Aufgaben zu tun hatten, die wir heute mit den Funktionen einer professionellen repressiven Behörde verbinden. Auch wenn die Autoritätsbereiche der genannten Magistrate zurecht an die öffentliche Ordnung und an Funktionen sozialer Regulation geknüpft werden, erscheint es sinnvoller, diese Ämter im Hinblick auf das staatliche Bemühen um die Herstellung und Aufrechterhaltung einer Form von Versorgungssicherheit zu betrachten. Arten von Sozialversicherungen — auf Bürger beschränkt — wie staatliche Zuschüsse für den Lebensunterhalt behinderter Personen oder die Unterstützung minderjähriger Söhne von im Krieg gefallenen Bürgern sind ebenfalls Schutzanstrengungen des Staates, die der Kategorie einer Versorgungssicherheit zuzurechnen sind.[^12] Sicherlich ist es legitim, das Verlangen nach Nahrung und Wasser ebenso wie das allgemeine Streben nach sozialer Absicherung als menschliche Grundbedürfnisse zu konstatieren. Aber mit der Wahrung innerer Sicherheit, im zuvor definierten Sinne, haben diese Beamten und Einrichtungen schlicht nichts zu tun.

Auf der Suche nach einer staatlich organisierten Exekutive wird man dennoch am ehesten beim »Beamtenapparat« der Magistrate fündig. Magistrate wurden entweder gelost oder gewählt und ein Bürger musste mindestens 30 Jahre alt sein, bevor er um eine Magistratur kandidieren konnte.[^13] Kennzeichen der athenischen Demokratie war es u.a., dass der Zugang zu einem Amt nicht an das Vermögen des Bürgers gebunden war,[^14] die allermeisten Dienste nur einmalig und nur für die Dauer eines Jahres ausgeübt werden konnten[^15] und das Magistrate, bis auf sehr wenige Ausnahmen,  in Gremien organisiert waren, die dem Prinzip der Kollegialität folgten.[^16]

Vorrangige Aufgabe der Magistrate war es, gefällte Regierungsentscheidungen auszuführen und den Bürgern Anordnungen zu erteilen.[^17] Nippel prononciert die Relevanz der Magistrate, wenn er schreibt, dass »grundsätzlich […] die öffentliche Ordnung darauf beruhte, daß die Bürger die Autorität der Magistrate anerkannten.«[^18] Der Staat stattete daher die Magistrate auch mit besonderen Rechten und Befugnissen aus. Sie genossen besonderen gesetzlichen Schutz und durften kleinere inappellabele Bußgelder verhängen.[^19] Als Zeichen ihres Status trugen Magistrate einen Myrtenkranz bei der Ausübung ihrer Pflichten und Gewalt gegen Beamte während ihres Dienstes oder deren Verleumdung wurden strenger geahndet, als dies bei gewöhnlichen Bürgern der Fall war.[^20] Weiterhin gab es eine moralische Verpflichtung, den Anweisungen von Magistraten Folge zu leisten, denn »der Ephebeneid schloß das Versprechen ein, den Magistraten zu gehorchen.«[^21] Magistrate vermittelten Bürgeranträge, riefen Versammlungen ein, bereiteten Geschäfte vor und brachten Anträge zur Abstimmung.[^22] Überdies hatten alle Magistrate prinzipiell das Recht, Klagen anzunehmen und den Prozessen vorzusitzen, auch wenn sich in der Praxis eine spezielle Aufteilung der Zuständigkeiten durchgesetzt hatte.[^23] Sie hatten, betrachtet man die staatliche Regierungsebene, keine Entscheidungsgewalt. Diese war der Volksversammlung, dem Kollegium der Nomotheten und dem Volksgericht vorbehalten, aber die Funktionen einer Exekutive und Verwaltung verrichtend bereiteten sie Entscheidungen vor und führten sie aus.[^24]  
Übernahmen die Magistrate aber auch die Strafverfolgung von Kriminellen, leisteten sie Ermittlungsarbeit und führten Verbrecher vor Gericht? Die nach Pflichten sortierte Einteilung der Magistrate des Aristoteles nennt u.a. zwar die bereits zuvor diskutierte Gruppe von Aufsehern über Märkte, Straßen, Wasser etc.; zudem wird die den Magistraten zukommende Aufgabe der Urteilsvollstreckung genannt, aber ein Appell zur Kriminalitätsbekämpfung, zur Herstellung innerer Sicherheit, findet sich keiner.[^25]  
Trotzdem gab es ein Gremium von Magistraten, das in einer engeren Beziehung zur Kriminalitätsbekämpfung stand. Auch wenn es keinen hoch angesehenen Status mit sich brachte, einer der Elf zu sein, und man nahezu jeden Tag des Jahres zum Dienst verpflichtet war, wurde diese Magistratur verhältnismäßig gut vergütet.[^26] Den »Beamten in der Rechtspflege«[^27] oblag zum einen die Leitung des Gefängnisses,[^28] wenngleich man in Athen keine Gefängnisstrafen als Strafhaft nach heutigem Verständnis kannte.[^29] Allerdings bestand eine Form der Untersuchungshaft. Sofern diese nicht ausgesprochen worden war, blieb ein Beschuldigter auf freiem Fuß. Ein Verurteilter blieb wiederum nur bis zur Vollstreckung des Urteils im Gefängnis.[^30] Die »Beamten in der Rechtspflege« hatten für diejenigen Sorge zu tragen, die in Untersuchungshaft saßen oder auf ihre Hinrichtung warteten.[^31] Ferner mussten sie sich um die Verfolgung entflohener Häftlinge kümmern.[^32] Des Weiteren gehörte es zu ihren Aufgaben, Konfiskationen durchzuführen und eingezogenes Vermögen öffentlich zu versteigern.[^33] Außerdem, und dies ist im Hinblick auf die Fragestellung der Diskussion von besonderer Bedeutung, waren sie für die Vollstreckung öffentlicher Gerichtsurteile und somit für die Hinrichtung verurteilter Verbrecher verantwortlich.[^34] Bemerkenswert ist, dass die Elf zudem das Recht besaßen, eine besondere Art Krimineller, die kakúrgoi,[^35] ohne Gerichtsverfahren hinzurichten, wenn diese in Ausübung der Tat ergriffen wurden und geständig waren.[^36] Selbst Bürger konnten auf diese Art und Weise exekutiert werden.[^37] Allerdings hatten diese das Recht, Einspruch gegen das Votum der Elf einzulegen. Dann musste das Gremium den Fall vor ein Gericht bringen, das die Schuld des Angeklagten erneut prüfte.[^38]  
Magistrate im Allgemeinen und insbesondere die »Elfmänner« besaßen also spezielle Autorität und Befugnisse, aber ihre Dienstpflichten können nicht mit der Wahrung der inneren Sicherheit in Verbindung gebracht werden. Hunter resümiert in Bezug auf die Elf: »It is difficult to believe that they were responsible for public order in Athens.«[^39] Immerhin erfüllten die elf Beamten die Funktionen einer urteilsvollstreckenden Exekutive. Dennoch gibt es keine Hinweise darauf, dass es zum Aufgabenbereich der Elf gehörte, Festnahmen in der Folge, oder gar zur Verhütung, von Straftaten durchzuführen.[^40] Wie hätte auch eine Mannschaft von elf Männern diese Aufträge nur im Entferntesten in einer Stadt von der Größe Athens bewältigen können?  
In diesem Zusammenhang lohnt eine Betrachtung der Staatssklaven, im Besonderen der im 5 Jh. v. Chr. »angeschafften« 300 skythischen Sklaven, die mehreren Magistraten als Hilfskräfte zur Verfügung standen.[^41] Bleicken nennt sie eine »besondere Polizeitruppe«,[^42] deren Aufgabe es war, für Ruhe und Ordnung zu sorgen und die zur Herstellung der Ordnung in der Volksversammlung sogar die Peitsche gebrauchte.[^43] Hierbei agierten sie nicht frei, sondern strikt auf Befehl autorisierter Herren.[^44] Ob diese Truppe auch den Elf assistierte, ist nicht ausdrücklich belegt, auch wenn bekannt ist, dass öffentliche Sklaven diese Magistrate unterstützten.[^45] Die toxotai wurden außerdem als Ordnungskräfte bei den Gerichten und als Wachposten aufgestellt.[^46] Dass sie regelmäßig in den Straßen patrouillierten, ist anderseits höchst unwahrscheinlich.[^47] Gegen die Bezeichnung als Polizeitruppe spricht ebenfalls ihr niederer gesellschaftlicher Rang, der keinerlei Autorität oder Rechte mit sich brachte. Die Legitimität ihres Tuns wurde einzig und allein durch den Status ihrer Herren verbürgt. Zusätzlich gibt es keinerlei Anzeichen dafür, dass sie in irgendeiner Form eine Rolle bei der Ermittlung oder Anklage von Kriminellen spielten.[^48] Folgender Einschätzung Hansens ist deshalb beizupflichten: »Es sieht jedoch nicht so aus, als seien sie irgendeine Art von Polizeitruppe im allgemein heutigen Sinn gewesen.«[^49]  
Zusammenfassend ist daher festzuhalten, dass der athenische Staat keine Institution kannte, deren Aufgabe es war, durch organisierte repressive Kriminalitätsbekämpfung die innere Sicherheit zu befördern. Einige Magistrate sorgten sich vornehmlich um die Versorgungssicherheit der Stadt, andere (Die Elf) waren primär mit der Vollstreckung von Gerichtsurteilen und der Leitung des Gefängnisses beschäftigt. Die Gruppe der skythischen Sklaven erfüllte allenfalls bloß sehr rudimentär die Aufgaben einer »Polizeitruppe« und das auch nur für den beschränkten Zeitraum von etwa 450 bis 350 v. Chr.[^50] Die Frage bleibt bis dato also noch offen, wer für Ermittlung, Festnahme und die Anklage von Straftätern vor Gericht verantwortlich war und wie die Sicherheit der Menschen »auf den Straßen« der Stadt gewährleistet werden konnte.

<!-- Literaturangaben -->
{{< bibliography title="Lesenswertes" >}}

{{< bib-item
  author="Blanton, Richard E./Fargher, Lane F."
  title="Collective Action in the Formation of Pre-Modern States, New York 2008">}}
{{< bib-item
  author="Bleicken, Jochen"
  title="Die athenische Demokratie, Paderborn 1995">}}
{{< bib-item
  author="Cornell, Tim J."
  title="police, in: Eidinow, Esther/Hornblower, Simon/Spawforth, Anthony (Eds.): The Oxford	Classical Dictionary, Oxford a. New York 2012, S. 1169-1170">}}
{{< bib-item
  author="Eder, Walter"
  title="Gefängnisstrafe, in: DNP, Abruf der Online-Ausgabe unter: [http://dx.doi.org/10.1163/1574-9347_dnp_e420370](http://dx.doi.org/10.1163/1574-9347_dnp_e420370)">}}
{{< bib-item
  author="Hansen, Mogens Hermann"
  title="Die Athenische Demokratie im Zeitalter des Demosthenes – Struktur, Prinzipien und Selbstverständnis, Berlin 1995">}}
{{< bib-item
  author="Härter, Karl"
  title="Polizei, in: Jaeger, Friedrich (Hrsg.): Enzykloädie der Neuzeit, Bd. 10, Stuttgart 2009, Sp. 170-180">}}
{{< bib-item
  author="Hunter, Virginia J."
  title="Policing Athens. Social control in the Attic lawsuits, 420-320 B.C., Princeton 1994. Nippel, Wilfried: Polizei, in: Cancik/Schneider/Landfester (Hrsg.): Der Neue Pauly, Bd. 10, Stuttgart 2001,	Sp. 34-35">}}
{{< bib-item
  author="Thür, Gerhard"
  title="Kakurgoi, in: DNP, Abruf der Online-Ausgabe unter: [http://dx.doi.org/10.1163/1574-9347_dnp_e605400](http://dx.doi.org/10.1163/1574-9347_dnp_e605400)">}}

{{< list title="Abkürzungen" header-element="4" >}}

{{< list-item
  title="DNP"
  description="Der Neue Pauly. Enzyklopädie der Antike, Cancik, Hubert/Schneider, Helmuth/Landfester, Manfred (Hrsg.)" >}}

{{< /list >}}

{{< /bibliography >}}
<!-- Literaturangaben Ende -->

<!-- Fußnoten -->
[^1]: Ich danke dem AkArchWiss e. V. für die kritische Durchsicht und hilfreiche Anmerkungen.
[^2]: Das ganze Zitat stammt aus: Härter 2009, Sp. 170f.
[^3]: Vgl. Cornell 2012, 1169.
[^4]: Vgl. Hansen 1995, 198.
[^5]: Das ganze Zitat stammt aus: Cornell 2012, 1169.
[^6]: Vgl. Blanton/Fargher 2008, 154.
[^7]: Vgl. Bleicken 1995, 288.
[^8]: Ebd.
[^9]: Vgl. ebd.
[^10]: Vgl. ebd.
[^11]: Vgl. Hansen 1995, 342.
[^12]: Vgl. ebd., 100.
[^13]: Vgl. ebd., 235.
[^14]: Vgl. ebd., 233.
[^15]: Vgl. ebd., 320.
[^16]: Vgl. ebd., 234; 245ff.
[^17]: Vgl. ebd., 237.
[^18]: Das ganze Zitat aus: Nippel 2001, Sp. 34.
[^19]: Vgl. Hansen 1995, 238.
[^20]: Vgl. ebd.
[^21]: Ebd., 237. *Epheboi* nannte man die Athener, die ihren zweijährigen Wehrdienst leisteten.
[^22]: Vgl. ebd.
[^23]: Vgl. ebd., 197.
[^24]: Vgl. ebd., 233; 237.
[^25]: Vgl. ebd., 252.
[^26]: Vgl. ebd., 242; 325.
[^27]: Bleicken 1995, 287.
[^28]: Vgl. ebd. Jene Einrichtungen, die man in der Antike als Gefängnis nutzte, können natürlich nicht mit den heutigen Gefängnissen verglichen werden.
[^29]: Vgl. ebd. Sowie: Eder, Walter: Gefängnisstrafe, in: Cancik, Hubert/Schneider, Helmuth/Landfester Manfred (Hrsg.): Der Neue Pauly. Abruf der Online-Ausgabe]
[^30]: Vgl. ebd.
[^31]: Vgl. Hunter 1994, 144. Vgl. Hansen 1995, 197.
[^32]: Vgl. Hunter 1994, 144.
[^33]: Vgl. Hansen 1995, 185f; 271.
[^34]: Vgl. ebd., 364. Vgl. Hunter 1994, 145.
[^35]: Vgl. Hansen 1995, 365. Dieser Begriff kann allgemein mit »Übeltäter« übersetzt werden. In einem speziellen athenischen Gesetz wurden mit dieser Bezeichnung »nächtliche Diebe, Kleiderräuber, Menschenräuber, Einbrecher und Beutelschneider« benannt. Zitat aus: Thür, Gerhard: Kakurgoi, in: Cancik/Schneider/Landfester (Hrsg.): Der Neue Pauly. Abruf der Online-Ausgabe.
[^36]: Vgl. ebd., 196. Vgl. Bleicken 1995, 244.
[^37]: Vgl. Hansen 1995, 196.
[^38]: Vgl. Hunter 1994, 145.
[^39]: Ebd.
[^40]: Vgl. ebd.
[^41]: Vgl. Hansen 1995, 127.
[^42]: Bleicken 1995, 110.
[^43]: Vgl. ebd.
[^44]: Vgl. ebd. 111.
[^45]: Vgl. Hunter 1994, 147.
[^46]: Vgl. Hansen 1995, 127. Vgl. Hunter 1994, 146.
[^47]: Vgl. Hunter 1994, 147.
[^48]: Vgl. ebd.
[^49]: Hansen 1995, 127.
[^50]: Vgl. ebd. Hansen gibt diesen Zeitraum an; Hunter hingegen bemerkt, dass es in den Quellen keine Hinweise auf Aktivitäten der Skythen nach ca. 390 v. Chr. gibt. Vgl. Hunter 1994, 147.
<!-- Fußnoten Ende -->
