---
title: "Kontakt"
aliases:
  - /about-us

header_img: /images/contact.jpg # Source: https://github.com/stackbit/stackbit-theme-fjord/blob/2c265913f38d23c21322f4ae5893a6653097895f/static/images/contact.jpg | License: https://github.com/stackbit/stackbit-theme-fjord/blob/2c265913f38d23c21322f4ae5893a6653097895f/LICENSE

layout: page
---

**Arbeitskreis Archäologische Wissenschaften e. V.**  
Röntgenstraße 12  
86415 Mering  
GERMANY  

E-Mail: {{< mail recipient="info@akarchwiss.de" text="info@akarchwiss.de" >}}  
Website: https://www.akarchwiss.de

Neumitgliederbetreuung: {{< mail recipient="neumitglied@akarchwiss.de" text="E-Mail" >}}  
Betreuung der Website: {{< mail recipient="webmaster@akarchwiss.de" text="E-Mail" >}}

[Infos zum Publizieren beim AkArchWiss e. V.](/leitfaeden)

<!-- Unsichtbarer Link, um unser Profil auf der Mastodon-Instanz troet.cafe zu verifizieren.
     Siehe auch: https://docs.joinmastodon.org/user/profile/ -->
<div style="display: none;">
  <a rel="me" href="https://troet.cafe/@akarchwiss">Mastodon</a>
</div>
