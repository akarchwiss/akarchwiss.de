---
title: "Veranstaltungen"
aliases:
  - /veranstaltungen
  - /veranstaltungen/ankuendigungen
  - /veranstaltungen/veranstaltungen-kommend
has_more_link: true
header_img: /images/temple-2.jpg
header_mobile_img: /images/temple-2-mobile.jpg

description: >-
  Hier findest du eine Übersicht über anstehende und vergangene Veranstaltungen des Arbeitskreis Archäologische Wissenschaften e. V.

layout: events_upcoming
---
