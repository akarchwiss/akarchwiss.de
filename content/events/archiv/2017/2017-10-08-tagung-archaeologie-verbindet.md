---
title: "Tagung: Archäologie verbindet" # Nicht mehr als 70 Zeichen – Google kürzt!
slug: tagung-archaeologie-verbindet-2017 # Benutzerdefinierter Pfad nach Domainname. Jahr, ggf. Monat mit aufnehmen! 
aliases:
  - /veranstaltungen/vergangene-veranstaltungen/Eventdetail/7/-/archäologie-verbindet

startDate: 2017-10-08T10:00:00+05:00
endDate: 2017-10-08T21:00:00+05:00
startDate_hhmm: true
endDate_hhmm: true

location:
  - Fulda

publishDate: 2017-08-30T10:30:00+00:00

summary: 

layout: event
---

Der AkArchWiss lädt seine Mitglieder und alle Interessenten herzlich zur 1. Tagung des Arbeitskreis Archäologische Wissenschaften e. V. am 8. Oktober 2017 ein.

<!--more-->

Die Tagung »Archäologie verbindet – ein Diskurs zwischen Wissenschaft, Beruf und Ehrenamt« findet in der Kapelle des Vonderau Museums, Fulda, statt.
