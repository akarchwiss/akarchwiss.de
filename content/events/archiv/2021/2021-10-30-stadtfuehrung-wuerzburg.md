---
title: "Stadtführung Würzburg" # Nicht mehr als 70 Zeichen – Google kürzt!
subtitle: Ein Spaziergang durchs alte Würzburg # Optional.
slug: stadtfuehrung-wuerzburg-2021 # Benutzerdefinierter Pfad nach Domainname. Jahr, ggf. Monat mit aufnehmen! 

startDate: 2021-10-30T16:30:00+10:00
endDate: 2021-10-30T18:30:00+10:00
startDate_hhmm: true # Wenn false oder Parameter nicht gesetzt, dann nur Datum in html.
endDate_hhmm: true # Wenn false oder Parameter nicht gesetzt, dann nur Datum in html.
archive: # Manuell archivieren, wenn Event vor seinem Enddatum in Sektion "Vergangene Veranstaltungen" erscheinen soll. 

location: # Liste möglich. 
  - Würzburg

publishDate: 2021-10-18T10:00:00+02:00
lastmod: 2021-10-05T00:00:00+10:00
draft: false
author:

header_img: /images/events/wuerzburg-marktkapelle.jpg
header_mobile_img: /images/events/wuerzburg-marktkapelle.jpg

summary: Folgt uns in der goldenen Jahreszeit auf einem Spaziergang durch das mittelalterliche und (früh-)neuzeitliche Würzburg.

layout: event
---

Warum wurde ein Würzburger Handelsreisender beinahe Opfer eines japanischen Selbstmordrituals?
Was ist eine unbefleckte Empfängnis und welches Verbrechen ereignete sich im »Mordhof« in der Augustinerstraße?
Würzburg am Main besitzt zahlreiche skurrile, spannende, amüsante und unheimliche Geschichten, Sagen und Anekdoten.
Ziel unserer Stadtführung ist es, diesen auf den Grund zu gehen und etwas Licht in die geheimnisvolle Geschichte der alten Bischofsstadt zu bringen.
Möchtet ihr das Geheimnis des Hexenturms lüften, über die Rotlichtmeile des alten Würzburg flanieren und die Machenschaften einer Hochstaplerin am Königshof aufdecken?
Dann seid ihr herzlich eingeladen, uns auf einer Stadtführung ins alte Würzburg zu folgen.

### Vorläufiger Tagesablauf:

* Beginn um 16.30 Uhr am Frankonia Brunnen vor der Würzburger Residenz.
* Ende der Stadtführung um ca. 18.30 Uhr.
* Nach dem Ende der Stadtführung ist eine gemütliche Kneipentour möglich.

### Termin & Anmeldung: {#registration}

<!-- registration: true # Setze die header-id "#registration" im Content! Erstellt link unter Artikel-Überschrift. -->

<!-- Ggf. nähere Informationen zum Termin; ansonsten reicht Überschrift "Anmeldung" -->

<!-- Ansprechpartner, E-Mail-Adresse (shortcode!) oder Telefonnummer -->

Eine Anmeldung im Voraus ist nicht notwendig.
Es gelten die Coronaverordnungen des Landes Bayern.

Für nähere Informationen wendet euch per {{< mail recipient="neumitglied@akarchwiss.de" subject="Stadtführung Würzburg 2021" text="E-Mail" >}} oder {{< phone number="+4917621478666" text="Telefon (+49 176 21 47 86 66)" >}} an Philipp Kuhn (Neumitgliederbetreuung).
