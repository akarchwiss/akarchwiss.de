---
title: "Virtueller Stammtisch"
slug: stammtisch-2021-02-07 # Benutzerdefinierter Pfad nach Domainname. Füge Datum hinzu!

startDate: 2021-02-07T19:00:00+01:00
endDate: 2021-02-07T22:00:00+01:00
startDate_hhmm: true # Wenn false (auch wenn Parameter fehlt), dann nur Datum in html.
archive: # Manuell archivieren, wenn Event vor seinem Enddatum als abgesagt unter Vergangene Veranstaltungen erscheinen soll.

location:
  - cloud.akarchwiss.de

publishDate: 2021-02-05T16:38:14+01:00
lastmod: 2021-02-05T00:00:00+01:00
author:
  - akarchwiss

summary:

layout: event
---

Der AkArchWiss lädt alle seine Mitglieder herzlich zu seinem Virtuellen Stammtisch am 7.2.2021 ein.

<!--more-->

Fragen zum Stammtisch bitte im Mitglieder-Chat der Cloud stellen oder an {{< mail recipient="info@akarchwiss.de" subject="Virtueller Stammtisch 7.2.2021" text="info@akarchwiss.de" >}} richten.
