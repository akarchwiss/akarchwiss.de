---
title: "Exkursion: Römerkastell Saalburg"
slug: exkursion-roemerkastell-saalburg-2016
aliases:
  - /veranstaltungen/vergangene-veranstaltungen/Eventdetail/6/-/römerkastell-saalburg

startDate: 2016-04-30
endDate: 2016-04-30

location:
  - Bad Homburg

publishDate: 2016-03-23T10:00:00+00:00

layout: event
---

Der AkArchWiss lädt seine Mitglieder herzlich zur Exkursion am 30. April 2016 ein.

<!-- more -->

Das Ziel ist das Römerkastell Saalburg im Archäologischen Park bei Bad Homburg.
