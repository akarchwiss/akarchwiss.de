---
title: "hessenARCHÄOLOGIE-Tag 2016" # Nicht mehr als 70 Zeichen – Google kürzt!
slug: hessenarchaeologie-2016 # Benutzerdefinierter Pfad nach Domainname. Jahr, ggf. Monat mit aufnehmen! 
aliases:
  - /veranstaltungen/vergangene-veranstaltungen/Eventdetail/5/-/hessenarch%C3%A4ologie-tag

startDate: 2016-11-05
endDate: 2016-11-05

location:
  - Hünfeld

publishDate: 2016-10-31T12:15:00+00:00

summary: 

layout: event
---

Der AkArchWiss lädt seine Mitglieder herzlich zur Exkursion am 5. November 2016 ein. Das Ziel ist der *hessenARCHÄOLOGIE-Tag 2016* in Hünfeld.
