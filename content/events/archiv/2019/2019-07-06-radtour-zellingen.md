---
title: "Archäologische Radltour 2019"
aliases:
  - /veranstaltungen/vergangene-veranstaltungen/Eventdetail/24/-/archäologische-radltour-2019

startDate: 2019-07-06T08:30:00+01:00
endDate: 2019-07-06T20:00:00+01:00
startDate_hhmm: true
endDate_hhmm: true

location:
  - Mainfranken

publishDate: 2019-06-10T10:30:00+00:00

author:
  - akarchwiss

summary: >-
  Der AkArchWiss lädt Rad- und Geschichtsbegeisterte zu einer Route durch die mainfränkische Geschichte ein.

header_img: /images/events/radtour-1.jpg # Source: https://unsplash.com/photos/LRu-_oeAE8c | Photo by https://unsplash.com/@nattyflo

layout: event
---

Der AkArchWiss lädt Rad- und Geschichtsbegeisterte zu einer Route durch die mainfränkische Geschichte ein. Auf einer [Strecke von knapp 65 km](https://maps.openrouteservice.org/directions?n1=49.847171&n2=9.881687&n3=12&a=49.792964,9.937681,49.899053,9.817848,49.909671,9.762211,49.96087,9.756281,49.961256,9.764357,49.97655,9.771219,49.999298,9.742985,50.02691,9.796901,49.898311,9.829524&b=1a&c=0&k1=en-US&k2=km&fbclid=IwAR3-F5s2KOeEyIl7O0IdRa8DAFif4d21UGGT6q6KYSzqa_aDma8FM8_niP8 "Routenplanung | openrouteservice.org") besuchen wir sowohl archäologische, als auch historische Stätten; daneben erwartet uns die sommerlich schöne Landschaft des westlichen Mainfrankens.

Zeitlich erstreckt sich das Spektrum vom Neolithikum über das gesamte Mittelalter bis hin zur jüngeren Neuzeit; der Zeitplan ist lediglich als grober Richtwert zu verstehen; die Tour richtet sich nach den Bedürfnissen und Wünschen der Teilnehmenden.

### Vorläufiger Tagesablauf:

| Uhrzeit   | Programmpunkt |
|-----------|---------------|
| 08:30 |	Start *Frankoniabrunnen am Residenzplatz, Würzburg* |
| 10:00	| historischer Altort, Schlossmauer, barocke Kirche, aktuelle Ausgrabungsstelle der Firma BfAD Heyse GmbH & Co. KG, *Zellingen* |
| 10:50 | Brotzeit; neolithische Fundstelle, *Theresienkapelle auf dem Kirchberg* |
| 11:10 | Topografie + Archäologie nahe ICE-Trasse; Besichtigung eines spätmittelalterlichen Wachturms, *Zellingen + Leinacher Umgebung* |
| 12:45 | Suche nach einem potentiellen Grabhügel, *Waldstück nahe Zellingen* |
| 13:30 | Verpflegung im Biergarten »Down Town«, *Himmelstadt* |
| 14:30 | Synagoge, ca. 400 J. alte Bausubstanz (wird durch den Förderkreis Ehemalige Synagoge Laudenbach e. V. instand gehalten), *Laudenbach* |
| 15:00 | Burgruine Karlsburg (8. Jh.–16. Jh.), *bei Karlstadt* |
| 15:40 | Besichtigung Altstadt, *Karlstadt* |
| 16:20 | optional: Burgruine Homburg (11.–17. Jh.), *bei Gössenheim* |
| 18:00 | Schützengräben des Zweiten Weltkriegs, *Retzbach* |

*Abreise per Rad (ca. 60 min nach Würzburg-Innenstadt) oder Zug (10–20 min nach Würzburg HBf, Fahrradmitnahme möglich).*

{{< notice title="Hinweis" >}}
Der Arbeitskreis Archäologische Wissenschaften e. V. und seine Mitglieder übernehmen keine sicherheitsrelevante Verantwortung, die Teilnehmenden handeln eigenverantwortlich. Der AkArchWiss organisiert lediglich die Route und bietet Informationen zu ausgewählten historischen und archäologischen Stätten. Ferner empfehlen wir festes Schuhwerk und ggf. Vorkehrungen gegen Zecken, da unsere Route eine Waldpassage enthält. Die Radtour wird auch bei Dauerregen veranstaltet. Eine Anmeldung ist nicht zwingend erforderlich.
{{< /notice >}}

<!-- *Header photo by [Flo P](https://unsplash.com/@nattyflo "unsplash.com | @nattyflo").* -->
