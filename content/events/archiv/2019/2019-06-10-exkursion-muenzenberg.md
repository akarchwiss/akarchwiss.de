---
title: "Exkursion: Münzenberg"
slug: exkursion-muenzenberg-2019
aliases:
  - /veranstaltungen/vergangene-veranstaltungen/Eventdetail/22/-/exkursion-münzenberg

startDate: 2019-06-10
endDate: 2019-06-10

location:
  - Münzenberg

publishDate: 2019-05-31T12:30:00+00:00

layout: event
---

Der Verein lädt zu einer Exkursion nach Münzenberg im hessischen Wetteraukreis.

<!--more-->

Neben dem alljährlich an Pfingsten veranstalteten Münzenberger Mittelaltermarkt soll die im Hochmittelalter errichtete Burg Münzenberg besichtigt werden. Nach einer Führung durch die von Sascha Piffko M. A. geleitete Grabungsfirma SPAU kann der Tag bei einem Grillfest ausgeklungen lassen werden.

### Termin & Anmeldung: {#registration}

Für die organisatorische Planung und zur Übermittlung näherer Informationen wird um Anmeldung bis einschließlich 5. Juni 2019 per E-Mail gebeten.
