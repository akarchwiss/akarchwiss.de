---
title: "Sommerfest"
slug: sommerfest-2019
aliases:
  - /veranstaltungen/vergangene-veranstaltungen/Eventdetail/21/-/sommerfest

startDate: 2019-06-22
endDate: 2019-06-22

location:
  - Fronhausen-Hassenhausen

publishDate: 2019-05-28T11:30:00+00:00

layout: event
---

Der Verein lädt zum diesjährigen Sommerfest ins Westhessische Berg- und Senkenland.

<!--more-->

Das gemütliche Beisammensein soll neben dem Kennenlernen der Mitglieder untereinander auch dem Austausch von Ideen, Vorstellungen oder Veranstaltungsvorschlägen dienen und so letztlich zur Weiterentwicklung des Vereins beitragen.

Herzlich eingeladen sind auch Bekannte des Vereins sowie am Verein Interessierte.

### Termin & Anmeldung:

Um Wünsche nach Grillgut und Getränken zu erfüllen, sowie um die Unterstützung bei der Verköstigung (etwa durch die Gabe eines Salats) koordinieren zu können, wird um eine entsprechende und verbindliche Rückmeldung bis einschließlich 31. Mai 2019 per E-Mail gebeten.
