---
title: "Jahreshauptversammlung 2019" # Nicht mehr als 70 Zeichen – Google kürzt!
slug: jahreshauptversammlung-2019 # Benutzerdefinierter Pfad nach Domainname. Jahr, ggf. Monat mit aufnehmen! 
aliases:
  - /veranstaltungen/vergangene-veranstaltungen/Eventdetail/20/-/jahreshauptversammlung

startDate: 2019-03-23T13:00:00+05:00
endDate: 2019-03-23T17:00:00+05:00
startDate_hhmm: true
endDate_hhmm: true

location:
  - Würzburg

publishDate: 2019-03-03T09:30:00+00:00

summary: 

layout: event
---

Der AkArchWiss lädt seine Mitglieder herzlich zur Jahreshauptversammlung 2019 nach Würzburg ein.
