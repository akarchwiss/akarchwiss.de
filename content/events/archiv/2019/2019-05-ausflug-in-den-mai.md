---
title: "Ausflug in den Mai"
slug: ausflug-in-den-mai-2019
aliases:
  - /veranstaltungen/vergangene-veranstaltungen/Eventdetail/14/-/ausflug-in-den-mai

startDate: 2019-05-04T11:00:00+01:00
endDate: 2019-05-05
startDate_hhmm: true
endDate_hhmm: false

location:
  - Fulda
  - Rhön

publishDate: 2019-04-01T12:30:00+00:00

layout: event
---

Der Verein lädt zu einer zweitägigen archäologischen, historischen und naturkundlichen Wanderung in die Hessische und Bayerische Rhön.

<!--more-->

### Vorläufiger Tagesablauf:

#### 4. Mai 2019

* Treffpunkt: 11.00 Uhr am Vorplatz des Bahnhofs Fulda
* [Vonderau Museum in Fulda](https://www.fulda.de/kultur-freizeit/vonderau-museum/das-museum/ "Stadt Fulda | Vonderau Museum")
* Vorgeschichtliche Höhensiedlung Milseburg
* Wanderung durch einen Teil des UNESCO Biosphärenreservats Rhön mit Einkehren in der Gaststätte »Fuldaer Haus«

#### 5. Mai 2019

* Wanderung auf Naturlehrpfad »Schwarzes Moor«
* Fränkisches Freilandmuseum Fladungen mit Einkehren in der Gaststätte »Zum Schwarzen Adler«

### Termin & Anmeldung:

Die An- und Abreise ist selbstständig zu organisieren. Treffpunkt ist am 4. Mai 2019 der Vorplatz des Bahnhofs Fulda (11.00 Uhr). Während des Ausflugs stehen Fahrzeuge und kostenfreies Mineralwasser zur Verfügung.
Für die Vollpension in der Jugendherberge Oberbernhards ist ein Entgelt in Höhe von 26 € zu entrichten.

Bei Interesse ergeht Bitte um eine verbindliche Anmeldung per {{< mail
  recipient="info@akarchwiss.de"
  subject="Exkursion Rhön 2019"
  body="Ich melde mich verbindlich an."
  text="E-Mail"
>}}.
