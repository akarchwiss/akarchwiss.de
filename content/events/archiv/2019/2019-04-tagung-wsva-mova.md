---
title: "Tagung: Verbandstagung WSVA + MOVA"
slug: tagung-wsva-mova-2019
aliases:
  - /veranstaltungen/vergangene-veranstaltungen/Eventdetail/15/-/verbandstagung-wsva-mova

startDate: 2019-04-01
endDate: 2019-04-05

location:
  - Würzburg

publishDate: 2019-03-14T13:00:00+00:00

summary: >-
  Der AkArchWiss ruft seine Mitglieder zur Teilnahme an der Verbandstagung von WSVA und MOVA auf.

layout: event
---

Der AkArchWiss ruft seine Mitglieder herzlich zur Teilnahme an der *Gemeinsamen Verbandstagung des West- und Süddeutschen Verbandes für Altertumskunde und des Mittel- und Ostdeutschen Verbandes für Altertumskunde* auf.
Sie findet vom 01. April 2019 bis zum 05. April 2019 in Würzburg statt.

* [Tagungen des WSVA](https://wsva.net/tagungen/ "WSVA | Tagungen" )
* [Homepage des MOVA](https://mova-online.de/ "MOVA")
