---
title: "Virtueller Stammtisch"
slug: stammtisch-2020-06-06 # Benutzerdefinierter Pfad nach Domainname. Füge Datum hinzu!
aliases:
  - /veranstaltungen/vergangene-veranstaltungen/Eventdetail/27/-/virtueller-stammtisch

startDate: 2020-06-06T18:00:00+05:00
endDate: 2020-06-06T00:00:00+05:00
startDate_hhmm: true
endDate_hhmm: false

location: 
  - Skype

publishDate: 2020-06-03T10:30:00+00:00

summary: 

layout: event
---

Am Samstag den 6. Juni 2020 wird ab 18 Uhr ein Stammtisch für unsere Mitglieder stattfinden. Einmal mehr wird er auf [Skype](https://www.skype.com/ "Microsoft | Skype") ausgerichtet.

Fragen zum Stammtisch bitte an {{< mail recipient="info@akarchwiss.de" subject="Virtueller Stammtisch 6.6.2020" text="info@akarchwiss.de" >}} richten.
