---
title: "Virtueller Stammtisch"
slug: stammtisch-2020-05-01 # Benutzerdefinierter Pfad nach Domainname. Füge Datum hinzu!
aliases:
  - /veranstaltungen/vergangene-veranstaltungen/Eventdetail/26/-/virtueller-stammtisch

startDate: 2020-05-01T18:00:00+05:00
endDate: 2020-05-01T00:00:00+05:00
startDate_hhmm: true
endDate_hhmm: false

location: 
  - Skype

publishDate: 2020-04-27T10:30:00+00:00

summary: 

layout: event
---

Da die diesjährige [Mai-Exkursion](/tags/exkursion/) entfallen muss, lädt der AkArchWiss am Freitag, den 1. Mai 2020, seine Mitglieder zu einem virtuellen Stammtisch ein.

<!--more-->

Der Stammtisch ist für die Mitglieder über [Skype](https://www.skype.com/ "Microsoft | Skype") zugänglich; ein Einladungslink wurde per E-Mail versandt.

Fragen zum Stammtisch bitte an {{< mail recipient="info@akarchwiss.de" subject="Virtueller Stammtisch 6.6.2020" text="info@akarchwiss.de" >}} richten.
