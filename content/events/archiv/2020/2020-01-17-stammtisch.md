---
title: "Stammtisch" # Nicht mehr als 70 Zeichen – Google kürzt!
slug: stammtisch-2020-01-17 # Benutzerdefinierter Pfad nach Domainname. Jahr, ggf. Monat mit aufnehmen! 

startDate: 2020-01-17T20:30:00+05:00
endDate: 2020-01-17T00:00:00+05:00
startDate_hhmm: true
endDate_hhmm: false

location:
  - Würzburg

publishDate: 2020-01-12T10:45:00+00:00

summary: 

layout: event
---

Am 17. Januar 2020 lädt der AkArchWiss ab 20.30 Uhr zu seinem Stammtisch.

<!--more-->

Dieses Mal wird uns das Lokal Hohlstange (Semmelstraße 60, 97070 Würzburg) bewirten.
Insbesondere möchten wir unsere Neuzugänge aus dem Landkreis Würzburg begrüßen, aber auch alle Interessierte über den Arbeitskreis Archäologische Wissenschaften e. V. informieren.
