---
title: "Vortrag: Archäologie verbindet"
subtitle: >-
  Tobias Mühlenbruch: Mykener und Hethiter – einander (räumlich) so nah und doch (in vielerlei Hinsicht) so fern
slug: vortrag-archaeologie-verbindet-mkyener-hethiter
aliases:
  - /veranstaltungen/vergangene-veranstaltungen/Eventdetail/11/-/archäologie-verbindet

startDate: 2018-07-03T18:00:00+01:00
endDate: 2018-07-03T21:00:00+01:00
startDate_hhmm: true
endDate_hhmm: true

location:
  - Marburg

publishDate: 2018-03-04T10:20:00+00:00

author:
  - akarchwiss

layout: event
---

Im Jahr 2018 setzt der AkArchWiss seine öffentliche Vortragsreihe »Archäologie verbindet« mit zwei Vorträgen im Juni und Juli fort.

<!--more-->

Im diesem zweiten Vortrag widmet sich PD Dr. Tobias Mühlenbruch dem Thema »Mykener und Hethiter – einander (räumlich) so nah und doch (in vielerlei Hinsicht) so fern«.
Der Vortag findet im *Hörsaal 00013* der Philipps-Universität Marburg (Biegenstraße 11, 35037 Marburg) statt.
