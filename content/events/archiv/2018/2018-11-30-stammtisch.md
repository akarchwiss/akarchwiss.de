---
title: "Stammtisch" # Nicht mehr als 70 Zeichen – Google kürzt!
slug: stammtisch-2018-11-30 # Benutzerdefinierter Pfad nach Domainname. Jahr, ggf. Monat mit aufnehmen! 
aliases:
  - /veranstaltungen/vergangene-veranstaltungen/Eventdetail/13/-/stammtisch

startDate: 2018-11-30T19:00:00+05:00
endDate: 2018-11-30T00:00:00+05:00
startDate_hhmm: true # Wenn false oder Parameter nicht gesetzt, dann nur Datum in html.
endDate_hhmm: false

location:
  - Würzburg

publishDate: 2018-11-25T12:15:00+00:00

summary: 

layout: event
---

Am 30. November 2018 lädt der AkArchWiss alle seine Mitglieder sowie Interssierte zum ersten Stammtisch des Vereins ein.

<!--more-->

Die Veranstaltung wird um 19.00 Uhr in der Gaststätte Hohlstange (Semmelstraße 60, 97070 Würzburg) eröffnet.
