---
title: "Geschichte(n) erleben!" # Nicht mehr als 70 Zeichen – Google kürzt!
slug: zeiteninsel-geschichten-erleben-2018 # Benutzerdefinierter Pfad nach Domainname. Jahr, ggf. Monat mit aufnehmen! 
aliases:
  - /veranstaltungen/vergangene-veranstaltungen/Eventdetail/3/-/geschichte-n-erleben

startDate: 2018-05-13T10:00:00+05:00
endDate: 2018-05-13T17:00:00+05:00
startDate_hhmm: true
endDate_hhmm: true

location:
  - Argenstein (Weimar/Lahn)

publishDate: 2018-05-07T10:45:00+00:00

summary: 

layout: event
---

Am 13. Mai 2018 wird der AkArchWiss mit einem Informationsstand auf der [Zeiteninsel](https://www.zeiteninsel.de/ "Zeiteninsel | Archäologisches Freilichtmuseum Marburger Land eG") des Archäologischen Freilichtmuseums Marbuger Land eG vertreten sein. Der Anlass ist die dortige Veranstaltung: »Geschichte(n) erleben«.

<!--more-->

Hierbei wird es die Möglichkeit geben sich, neben den durch die Zeiteninsel angebotenen Mitmachaktionen und Vorführungen, näher über den Verein und das nächste kommende Projekt zu informieren.
