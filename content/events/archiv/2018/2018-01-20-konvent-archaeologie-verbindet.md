---
title: "Nachtreffen: Archäologie Verbindet" # Nicht mehr als 70 Zeichen – Google kürzt!
slug: nachtreffen-archaeologie-verbindet-2017 # Benutzerdefinierter Pfad nach Domainname. Jahr, ggf. Monat mit aufnehmen! 
aliases:
  - /vergangene-veranstaltungen/Eventdetail/9/-/nachtreffen-archäologie-verbindet

startDate: 2018-01-20T10:00:00+05:00
endDate: 2018-01-20T14:00:00+05:00
startDate_hhmm: true
endDate_hhmm: true

location:
  - Fulda

publishDate: 2018-01-02T10:30:00+00:00

summary: 

layout: event
---

Am 20. Januar 2018 lädt der AkArchWiss die Referenten der Tagung [»Archäologie verbindet – ein Diskurs zwischen Wissenschaft, Beruf und Ehrenamt«](/berichte/2017/tagung-archaeologie-verbindet) zu einem Nachtreffen in der Bibliothek des Vonderau Museums, Fulda, ein.
