---
title: "Vortrag: Archäologie verbindet"
subtitle: "Ulrike Söder: Die Milseburg in der Rhön 2003–2018 – Archäologische Geschichte(n)"
slug: vortrag-archaeologie-verbindet-milseburg
aliases:
  - /veranstaltungen/vergangene-veranstaltungen/Eventdetail/12/-/archäologie-verbindet

startDate: 2018-06-26T18:00:00+01:00
endDate: 2018-06-26T21:00:00+01:00
startDate_hhmm: true
endDate_hhmm: true

location:
  - Marburg

publishDate: 2018-03-04T10:20:00+00:00

author:
  - akarchwiss

layout: event
---

Im Jahr 2018 setzt der AkArchWiss seine öffentliche Vortragsreihe »Archäologie verbindet« mit zwei Vorträgen im Juni und Juli fort.

<!--more-->

In diesem ersten Vortrag widmet sich Dr. Ulrike Söder dem Thema »Die Milseburg in der Rhön 2003–2018 – Archäologische Geschichte(n)«.
Der Vortag findet im *Hörsaal 00013* der Philipps-Universität Marburg (Biegenstraße 11, 35037 Marburg) statt.
