---
title: "Exkursion: Grube Fortuna" # Nicht mehr als 70 Zeichen – Google kürzt!
aliases:
  - /veranstaltungen/vergangene-veranstaltungen/Eventdetail/1/-/grube-fortuna
slug: exkursion-grube-fortuna-2018 # Benutzerdefinierter Pfad nach Domainname. Jahr, ggf. Monat mit aufnehmen! 

startDate: 2018-05-01T11:00:00+05:00
endDate: 2018-05-01T00:00:00+05:00
startDate_hhmm: true
endDate_hhmm: false

location:
  - Solms-Oberbiel (Wetzlar)

publishDate: 2018-04-13T11:00:00+00:00

summary: 

layout: event
---

Am 1. Mai 2018 möchte der AkArchWiss e. V. einen Ausflug in die [Grube Fortuna](https://grube-fortuna.de/ "Geowelt Fortuna e. V."), westlich von Wetzlar (Lahn), unternehmen.

<!--more-->

Es wird eine Führung durch das Besucherbergwerk geben. Hierzu laden wir Dich und Begleitung recht herzlich ein. Die Kosten belaufen sich pro Person auf etwa 10 bis 15 Euro, je nach Gruppengröße. Die An- und Abreise ist individuell zu gestalten, jedoch können gern Fahrgemeinschaften in Eigenregie erstellt werden.

### Termin & Anmeldung: {#registration}
<!-- registration: true # Please set header-id with #registration in content! Erstellt link unter Artikel-Überschrift -->

Genauere Termininformation und die endgültigen Kosten werden unmittelbar nach dem 22.04.2018, per E-Mail, an alle teilnehmenden Vereinsmitglieder versandt. Es wird gebeten am 1. Mai den entsprechenden Betrag  als Bargeld bereit zu halten. Ein Fahrtkostenzuschuss durch den Verein ist, je nach Teilnehmerzahl und Anreiseaufwand, möglich. Hierzu füge deiner Teilnahme-E-Mail bitte eine entsprechende kurze Anfrage und Begründung bei. Die Bewilligung und Höhe der Bezuschussung liegt im Ermessen der Vorstandschaft und wird Dir ggf. per E-Mail mitgeteilt.
