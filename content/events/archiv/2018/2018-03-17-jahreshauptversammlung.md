---
title: "Jahreshauptversammlung 2018" # Nicht mehr als 70 Zeichen – Google kürzt!
slug: jahreshauptversammlung-2018 # Benutzerdefinierter Pfad nach Domainname. Jahr, ggf. Monat mit aufnehmen! 
aliases:
  - /veranstaltungen/vergangene-veranstaltungen/Eventdetail/8/-/jahreshauptversammlung

startDate: 2018-03-17T13:00:00+05:00
endDate: 2018-03-17T17:00:00+05:00
startDate_hhmm: true
endDate_hhmm: true

location:
  - Marburg (Lahn)

publishDate: 2018-03-02T10:35:00+00:00

summary: 

layout: event
---

Der AkArchWiss lädt seine Mitglieder herzlich zur Jahreshauptversammlung 2018 nach Marburg a. d. Lahn ein.
