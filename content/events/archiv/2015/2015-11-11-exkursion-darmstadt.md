---
title: "Exkursion: Darmstadt HLD"
slug: exkursion-darmstadt-2015
aliases:
  - /veranstaltungen/vergangene-veranstaltungen/Eventdetail/10/-/darmstadt

startDate: 2015-11-11
endDate: 2015-11-11

location:
  - Darmstadt

publishDate: 2015-10-01T13:00:00+00:00

layout: event
---

Der AkArchWiss lädt seine Mitglieder herzlich zur Exkursion am 11. November 2015 nach Darmstadt ein.

<!--more-->

Das Ziel ist die Sonderausstellung *»Homo – Expanding Worlds«* im Hessischen Landesmuseum Darmstadt. Anschließend kann der Vortrag »Ursprünge, Umbrüche, Umwege – 6 Millionen Jahre Mensch« von Prof. Dr. Friedmann Schrenk besucht werden.
