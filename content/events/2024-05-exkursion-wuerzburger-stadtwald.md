---
title: "Exkursion in den Würzburger Stadtwald" # Nicht mehr als 70 Zeichen – Google kürzt!
subtitle: Vogelstimmen & archäologische Bodendenkmäler # Optional
slug: exkursion-wuerzburger-stadtwald-2024 # Benutzerdefinierter Pfad nach Domainname. Jahr, ggf. Monat mit aufnehmen! 

startDate: 2024-05-25T17:30:00+05:00
endDate: 2024-05-25T19:30:00+05:00
startDate_hhmm: true
endDate_hhmm: true
archive: # Manuell archivieren, wenn Event vor seinem Enddatum in Sektion "Vergangene Veranstaltungen" erscheinen soll. 

location:
  - Würzburg

publishDate: 2024-05-20T19:00:00+05:00
draft: false
author:
  - akarchwiss

header_img: /images/events/forest-1.jpg
header_mobile_img: /images/events/forest-1.jpg

summary: >-
  Begleitet uns auf einer Wanderung durch den Würzburger Stadtwald – der Schwerpunkt liegt auf Vogelstimmen und archäologischen Bodendenkmälern.

layout: event
---

Am 25. Mai 2024 sind die Mitglieder des AkArchWiss e. V. sowie alle Interessierte herzlich eingeladen, uns auf einer Exkursion in den *Würzburger Stadtwald* zu begleiten.
Der Schwerpunkt liegt an diesem Abend bei der Erkundung von Vogelstimmen und archäologischen Bodendenkmälern (Dauer: ca. 2 h).

Wir treffen uns um 17.30 Uhr vor dem Nordeingang des Würzburger Waldfriedhofs.
Wer möchte, kann mit uns nach der Wanderung gerne in das gemütliche Wirtshaus *Zum Postkutscherl* einkehren.

Eine Anmeldung ist nicht notwendig.
Die Anreise erfolgt individuell.
