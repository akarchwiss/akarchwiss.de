---
title: "Tagung: Burgensymposium des Spessartprojekts" # Nicht mehr als 70 Zeichen – Google kürzt!
subtitle: 15. Symposium zur Burgenforschung im Spessart und Odenwald # Optional
slug: burgensymposium-spessart-odenwald-2024 # Benutzerdefinierter Pfad nach Domainname. Jahr, ggf. Monat mit aufnehmen! 

startDate: 2024-05-17T14:00:00+05:00
endDate: 2024-05-18T18:00:00+05:00
startDate_hhmm: true
endDate_hhmm: true
archive: # Manuell archivieren, wenn Event vor seinem Enddatum in Sektion "Vergangene Veranstaltungen" erscheinen soll. 

location:
  - Zellingen

publishDate: 2024-05-12T16:30:00+05:00
lastmod: 2024-05-12T00:00:00+05:00
draft: false
author:
  - akarchwiss

summary: >-
  Der AkArchWiss ruft seine Mitglieder und Freunde herzlich zur Teilnahme am 15. Symposium zur Burgenforschung im Spessart und Odenwald auf.

layout: event
---

Am 17. und 18. Mai 2024 findet in Zellingen das diesjährige Symposium zur Burgenforschung im Spessart und angrenzenden Regionen statt.
Die Veranstaltung mit dem Titel *»Die Karolingerzeit in Unterfranken und darüber hinaus – Eine archäologische Spurensuche«* wird vom Archäologischen Spessartprojekt e. V., dem Historischen Verein Karlstadt e. V., der Arge Seehausen und dem Markt Zellingen ausgerichtet.

Die Tagung kann mit vielen interessanten Vorträgen zur Karolingerzeit aufwarten;
das offizielle Tagungsprogramm sowie Informationen zum Tagungsort könnt ihr [hier](https://www.spessartprojekt.de/forschung/burgensymposien/burgensymposien-2024-zellingen/) einsehen. Der Eintritt ist kostenlos.

* [Archäologisches Spessartprojekt e. V.](https://www.spessartprojekt.de/)
* [Burgensymposium 2024](https://www.spessartprojekt.de/forschung/burgensymposien/burgensymposien-2024-zellingen/)
