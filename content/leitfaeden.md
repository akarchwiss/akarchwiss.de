---
title: "Leitfäden & Richtlinien"
aliases:
  - /dokumente/nutzerinformationen/42-guidelines

publishDate: 2021-01-01
lastMod: 2021-01-01

header_img: /images/temple-1.jpg

description: >-
  Wenn du dich aktiv in einen interdisziplinär arbeitenden archäologisch-historischen Verein einbringen möchtest, kannst du dich hier genauer informieren!

layout: page
---

### Publizieren beim AkArchWiss e. V.

Wir freuen uns auf deinen Beitrag!
Bevor du ihn bei uns einreichst,
damit wir ihn auf unserer Website veröffentlichen
und u. a. auf Facebook bewerben können,
beachte bitte die folgenden Dokumente:
  * [Leitfaden für Autor:innen zur Einreichung von Textbeiträgen](/docs/leitfaden-textbeitraege-akarchwiss.pdf "Leitfaden Textbeiträge AkArchWiss e. V.")
  * [Einverständniserklärung der Autorenschaft zu Veröffentlichung eines Textbeitrags](/docs/einverstaendniserklaerung-autorenschaft-akarchwiss.pdf "Einverständniserklärung Autorenschaft AkArchWiss e. V.")
