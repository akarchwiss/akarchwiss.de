#!/usr/bin/env bash

# Create thumbnail images for jpg/jpeg.
# Only create thumbnail if "-thumb.jpg" do not exist.
# Do not create thumbnails for featured images (they are only served in full resolution).
# Recursively process a directory.

# ImageMagick parameters were taken from:
# https://www.smashingmagazine.com/2015/06/efficient-image-resizing-with-imagemagick/

# Usage:
#   create-thumb <Input-Directory/>

# For Windows users:
# Define path to ImageMagick binaries, e.g.:
#   export IMAGEMAGICK_BINARIES='/c/Program Files/ImageMagick-6.9.12-Q16-HDRI/'

DIR_IN=$1
IMG_EXT="jpg|jpeg" # Regex for allowed image file extensions.

unameOut="$(uname -s)"
case "${unameOut}" in
	Linux*)     machine=Linux;;
	Darwin*)    machine=Mac;;
	CYGWIN*)    machine=Cygwin;;
	MINGW*)     machine=MinGw;;
	*)          machine="UNKNOWN:${unameOut}"
esac

if [ ${machine} == "MinGw" ] || [ ${machine} == "Cygwin"]
then
    export PATH="${IMAGEMAGICK_BINARIES}:$PATH"
fi

create_thumbnail () {
    local dir_in=${1}
    for file_in in $(find $(pwd)/${dir_in} -type f -regextype posix-extended ! -regex "^.*-thumb\.(${IMG_EXT})" ! -regex "^.*-featured\.(${IMG_EXT})" -regex "^.*\.(${IMG_EXT})"); do
        file_name=$(basename -- "${file_in}")
        file_ext="${file_name##*.}"
        file_name="${file_in%.*}" # File path without extension.
        file_out="${file_name}-thumb.${file_ext}"
        convert ${file_in} -filter Triangle -define filter:support=2 -thumbnail 250x -unsharp 0.25x0.08+8.3+0.045 -dither None -posterize 136 -quality 82 -define jpeg:fancy-upsampling=off -define png:compression-filter=5 -define png:compression-level=9 -define png:compression-strategy=1 -define png:exclude-chunk=all -interlace none -colorspace sRGB -strip ${file_out}; \
        echo "Thumbnail: ${file_in} → ${file_out}";
    done
}

create_thumbnail ${DIR_IN}
