@echo off

title AkArchWiss e. V. - Post erstellen

@echo.
@echo Hier kannst Du einen neuen Post auf Basis eines "archetype" erstellen.
@echo Du kannst auf Wunsch einen neuen Branch anlegen und den Post ins Repo commiten.
@echo.
@echo Bitte gib den Typ und den Speicherpfad des zu erstellenden Posts an.
@echo.
@echo B e i s p i e l e
@echo --------------------------------------------
@echo Regulaerer Artikel: 
@echo Typ: post 
@echo Pfad: berichte/2021/exkursionsbericht-weimar
@echo Branch: exkursionsbericht-weimar
@echo.
@echo Veranstaltung: 
@echo Typ: event
@echo Pfad: events/2021-05-01-exkursion-weimar
@echo Branch: exkursion-weimar
@echo --------------------------------------------
@echo.
@echo Mit Ctrl-c kannst Du das Skript verlassen.
@echo.
@echo.

goto :check-git

:check-git
for /f %%i in ('git diff HEAD') do set git_status=%%i
if "%git_status%" EQU "" goto :create-branch
if "%git_status%" NEQ "" goto :git-dirty

:git-dirty
@echo Dein lokales Repositorium enthaelt nicht committete Dateien.
@echo Pruefe den Tree und committe oder annuliere die modifzierten Dateien,
@echo bevor Du mit der Erstellung eines Posts fortfährst.
set /p retry_git="--> Erneut versuchen [j/n]?"
set "git_status="
if /i "%retry_git%" EQU "j" goto :check-git
if /i "%retry_git%" EQU "n" goto :exit

:new-post
set /p tmpl="--> Typ: "

if "%tmpl%"=="post" (
	goto :create-post
)

if "%tmpl%"=="event" (
	goto :create-event
)

@echo.
@echo Dieser archetype ist nicht vorhanden. Versuche es erneut.
@echo Legitime archetypes: post oder event
@echo.	
goto :new-post

:create-post
set /p post_path="--> Pfad: "
pushd ..
hugo.exe new --kind post-bundle content/"%post_path%"
popd
goto :commit

:create-event
set /p post_path="--> Pfad: "
pushd ..
hugo.exe new --kind event content/"%post_path%".md
popd
goto :commit

:commit
set /p commit="--> Moechtest Du den Post bereits in das aktuelle Repositorium übergeben? [j/n]?"
if /i "%commit%" EQU "j" goto :commit-post
if /i "%commit%" EQU "n" goto :choice

goto :commit

:commit-post
pushd ..
set /p commit_msg="--> Kurze Commit-Message: "
git add content/%post_path%*
git commit -m "%commit_msg%"
popd
goto :choice

:create-branch
@echo Hole neueste Aenderungen von gitlab.com:akarchwiss/akarchwiss.de ...
@echo.
git fetch origin staging --quiet
git pull origin staging --quiet
@echo.
set /p branch_name="--> Gib ein Alias fuer den neuen Branch oder gibt einen bestehenden Branch an: "
set branch_path="posts/%branch_name%"
@echo.

for /f %%i in ('git rev-parse --verify --quiet %branch_path%') do set branch_exists=%%i >nul
if /i "%branch_exists%" EQU "" git switch -c %branch_path% staging --quiet
if /i "%branch_exists%" NEQ "" git switch %branch_path% --quiet

@echo Du befindest dich nun auf dem Branch %branch_path%.

goto :new-post

:choice
set /p continue="--> Einen weiteren Post anlegen [j/n]?"
if /i "%continue%" EQU "j" goto :check-git
if /i "%continue%" EQU "n" goto :exit
