@echo off

title AkArchWiss e. V. - Hugo Development-Server

@echo.
@echo Hier kannst Du auf deinem PC einen lokalen Hugo Development-Server starten.
@echo.
@echo Achte beim lokalen Arbeiten auf die Log-Ausgabe in diesem Fenster.
@echo Sie weist dich u. a. auf Syntaxfehler hin.
@echo.
@echo Wenn Du lokale Änderungen speicherst, wird die Website automatisch neu gebaut
@echo und im Browser aktualisiert.
@echo Beachte, dass der Development-Server auch Entwürfe und geplante Posts rendert.
@echo.
@echo Um die Website lokal anzuschauen, rufe http://localhost:1313 (default) in deinem Browser auf.
@echo Mit Ctrl-c stoppst Du den lokalen Server und schließt das Fenster mit der Log-Ausgabe.
@echo.

:choice
set /p continue="Möchtest Du den Develompent-Server jetzt starten [j/n]?"
if /i "%continue%" EQU "j" goto :start-server
if /i "%continue%" EQU "n" goto :exit

:start-server
start hugo.exe server --disableFastRender --buildDrafts --buildFuture
