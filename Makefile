# This Makefile assists you with local development.
# Extended version of hugo is required.

ENV := env
CLEAR := clear
GIT := git

HUGO := hugo
HUGO_MOD := $(HUGO) mod
HUGO_BUILD_FLAGS := --minify
HUGO_POST_FLAGS := --kind article-bundle
HUGO_EVENT_FLAGS := --kind event
HUGO_VR_STAMMTISCH_FLAGS := --kind vr-stammtisch
HUGO_SERVE_FLAGS := \
	--disableFastRender \
	--buildDrafts \
	--buildFuture 
MKDIR := mkdir
RM := rm
SSH_PRIVATE_KEY := "" # Path to private key.
RSYNC := rsync
RSYNC_FLAGS := \
	-at \
	--verbose \
	--delete \
	--delete-delay \
	--delay-updates \
	--exclude=_ \
	--include=.well-known \
	-e 'ssh -i $(SSH_PRIVATE_KEY)'
PUBLIC_HTML := public
RESOURCES := resources
RSYNC_SRC := $(PUBLIC_HTML)
RSYNC_DST := ""

SITE_MODULE := gitlab.com/akarchwiss/akarchwiss.de

MOD_FILE := go.mod
THEME_GIT := "git@gitlab.com:akarchwiss/hugo-fjord-akarchwiss.git"
THEME_MODULE := "gitlab.com/akarchwiss/hugo-fjord-akarchwiss"
THEMES_PATH := ../
THEME := "hugo-fjord-akarchwiss"

.PHONY: build clean dev deploy event futurelist help module post test theme vr-stammtisch update-modules

all: clean build deploy

# Show help.
help:
	$(CLEAR)
	@echo '    ___    __   ___              __  _       ___ '
	@echo '   /   |  / /__/   |  __________/ /_| |     / (_)_________ '
	@echo '  / /| | / //_/ /| | / ___/ ___/ __ \ | /| / / / ___/ ___/ '
	@echo ' / ___ |/ ,< / ___ |/ /  / /__/ / / / |/ |/ / (__  |__  ) '
	@echo '/_/  |_/_/|_/_/  |_/_/   \___/_/ /_/|__/|__/_/____/____/ '
	@echo ''
	@echo '      https://gitlab.com/akarchwiss/akarchwiss.de/'
	@echo ''
	@echo '            +++ Commonly Used Commands +++'
	@echo ''
	@echo 'Start local web server for testing purposes:'
	@echo '  make test'
	@echo 'You can access the site at localhost:1313.'
	@echo ''
	@echo 'Create new regular article post:'
	@echo '  make article post=section/year/post-title'
	@echo ''
	@echo 'Create new event post:'
	@echo '  make event post=events/event-title'
	@echo ''
	@echo 'Create new VR Stammtisch post:'
	@echo '  make vr-stammtisch post=events/event-title'
	@echo ''
	@echo 'For more tips and tricks have a look at our cheatsheet:'
	@echo 'https://gitlab.com/akarchwiss/akarchwiss.de/-/wikis/Cheatsheet'
	@echo ''

# Build html files and assets.
build:
	$(HUGO) \
		$(HUGO_BUILD_FLAGS)

# Start web server with live changes.
# Use theme from local replacement module.
dev:
	$(ENV) \
		HUGO_MODULE_REPLACEMENTS="$(THEME_MODULE) -> $(THEME)" \
			$(HUGO) \
				server \
				$(HUGO_SERVE_FLAGS) \
				--themesDir $(THEMES_PATH)

# Deploy to web server.
deploy: build theme
	$(RSYNC) \
		$(RSYNC_FLAGS) \
		$(RSYNC_SRC) $(RSYNC_DST)

# Scaffold event post.
event:
	$(HUGO) \
		new $(HUGO_EVENT_FLAGS) \
		$(post).md

# Show a list of future posts.
futurelist:
	 $(HUGO) \
	 	list future \
		| sed -E 's/^.+\/([^\/]+)\/index.md,(.+)/\2 - \1/'

# Initialize site as module.
module: $(MOD_FILE)
$(MOD_FILE):
	$(HUGO_MOD) init $(SITE_MODULE)

# Scaffold regular post.
article:
	$(HUGO) \
		new $(HUGO_POST_FLAGS) \
		$(post)

# Start web server with live changes.
# Use theme from remote module.
test: update-modules
	$(HUGO_MOD) get -u $(THEME_MODULE)
	$(HUGO) \
		server \
		$(HUGO_SERVE_FLAGS)

# Clone theme in local git repo.
theme:
	$(MKDIR) -p $(THEMES_PATH)
	$(GIT) clone $(THEME_GIT)

# Scaffold vr-stammtisch (event) post.
vr-stammtisch:
	$(HUGO) \
		new $(HUGO_VR_STAMMTISCH_FLAGS) \
		$(post).md

# Update modules.
update-modules: module
	$(HUGO_MOD) get -u $(THEME_MODULE)

# Delete local build files.
clean:
	$(RM) \
		-rf \
		$(PUBLIC_HTML) \
		$(RESOURCES)
